import 'package:camera/camera.dart';
import 'package:clinic/face_full_camera/services/face_detection/face_detection_service.dart';
import 'package:clinic/face_full_camera/services/face_mesh/face_mesh_service.dart';
import 'package:clinic/face_full_camera/services/model_inference_service.dart';

import 'package:clinic/widgets/face_mesh_painter.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class CameraApp extends StatefulWidget {
  final bool draw;
  const CameraApp({
    this.draw = true,
    Key? key,
  }) : super(key: key);

  @override
  State<CameraApp> createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> with WidgetsBindingObserver {
  CameraController? controller;
  List<CameraDescription>? _cameras;
  late CameraDescription _cameraDescription;
  final BehaviorSubject<List<Offset>?> inferenceResults =
      BehaviorSubject.seeded(null);

  bool _predicting = true;

  late double _ratio;

  late ModelInferenceService<FaceMesh> _mechService;
  late ModelInferenceService<FaceDetection> _detectionService;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _initStateAsync();
  }

  void _initStateAsync() async {
    _mechService = ModelInferenceService<FaceMesh>();
    _detectionService = ModelInferenceService<FaceDetection>();
    await _initCamera();
    _predicting = false;
  }

  Future<void> _initCamera() async {
    _cameras ??= await availableCameras();
    _cameraDescription = _cameras![1];

    _onNewCameraSelected(_cameraDescription);
  }

  Widget get _drawLandmarks => _ModelPainter(
        customPainter: FaceMeshPainter(
          points: inferenceResults.value ?? [],
          ratio: _ratio,
        ),
      );

  @override
  Widget build(BuildContext context) {
    if (!(controller?.value.isInitialized ?? false)) {
      return Container();
    }

    final screenSize = MediaQuery.of(context).size;
    _ratio = screenSize.width / controller!.value.previewSize!.height;

    final scale = 1 /
        (controller!.value.aspectRatio *
            MediaQuery.of(context).size.aspectRatio);

    return MaterialApp(
      home: Scaffold(
        body: Stack(
          children: [
            Center(
              child: Transform.scale(
                scale: scale,
                child: CameraPreview(
                  controller!,
                  child: StreamBuilder(
                    builder: (context, snapshot) => _drawLandmarks,
                    stream: inferenceResults,
                  ),
                ),
              ),
            ),
            CustomPaint(
              size: const Size(double.infinity, double.infinity),
              painter: OvalPainter(),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();

    _mechService.inferenceResults = null;
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = controller;

    // App state changed before we got the chance to initialize.

    if (state == AppLifecycleState.inactive) {
      cameraController?.dispose();
      controller = null;
    } else if (state == AppLifecycleState.resumed) {
      _initCamera();
      // _onNewCameraSelected(cameraController.description);
    }
  }

  void _onNewCameraSelected(CameraDescription cameraDescription) async {
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: false,
    );

    controller!.addListener(() {
      if (mounted) setState(() {});
    });

    try {
      await controller!.initialize().then((value) {
        if (!mounted) return;
      });
    } on CameraException catch (e) {}

    if (mounted) {
      setState(() {});
    }
    controller!.startImageStream(
        (image) async => await _inference(cameraImage: image));
  }

  Future<void> _inference({required CameraImage cameraImage}) async {
    if (!mounted) return;

    if (_mechService.model.interpreter != null) {
      if (_predicting || !widget.draw) {
        return;
      }
      _predicting = true;
      if (widget.draw) {
        _detectionService.inferenceResults = null;
        _mechService.inferenceResults = null;
        await _detectionService.inference(cameraImage: cameraImage);

        if (_detectionService.inferenceResults != null &&
            _detectionService.inferenceResults!['score'] > 0.90) {
          final rrect = _detectionService.inferenceResults!['bbox'] as Rect;

          await _mechService.inference(
            cameraImage: cameraImage,
            p1: rrect.topLeft - const Offset(50, 50),
            p2: rrect.bottomRight + const Offset(50, 50),
          );
        }
        inferenceResults.value = _mechService.inferenceResults?['point'] ?? [];
      }
      _predicting = false;
    }
  }
}

class _ModelPainter extends StatelessWidget {
  _ModelPainter({
    required this.customPainter,
    Key? key,
  }) : super(key: key);

  final CustomPainter customPainter;

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: customPainter,
    );
  }
}

class OvalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawOval(
      Rect.fromCenter(
        center: Offset(size.width / 2, size.height / 2),
        width: size.width / 1,
        height: size.height / 1.5,
      ),
      Paint()
        ..style = PaintingStyle.stroke
        ..color = Colors.red
        ..strokeWidth = 4,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
