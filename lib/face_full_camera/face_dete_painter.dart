import 'dart:developer';

import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';

class FaceDetectionPainter extends CustomPainter {
  final Rect? bbox;
  final double ratio;

  FaceDetectionPainter({
    this.bbox,
    required this.ratio,
  });

  @override
  void paint(Canvas canvas, Size size) {
    if (bbox != null && bbox != Rect.zero) {
      var paint = Paint()
        ..color = Colors.blue
        ..style = PaintingStyle.stroke
        ..strokeWidth = 3;
      final rect = Rect.fromLTRB(
        bbox!.left * ratio,
        bbox!.top * ratio,
        bbox!.right * ratio,
        bbox!.bottom * ratio,
      );

      canvas.drawRect(rect, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class OvalPainter extends CustomPainter {
  final Color color;
  final Rect bbox;
  final bool right;

  const OvalPainter({
    required this.bbox,
    this.right = true,
    this.color = ColorData.colorElementsActive,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipPath(
      Path()
        ..addRect(
          Rect.fromLTRB(
            right ? size.width / 2 : 0,
            0,
            right ? size.width : size.width / 2,
            size.height,
          ),
        ),
    );
    canvas.drawPath(
      Path()
        ..addOval(
          Rect.fromLTRB(
            size.width / 2 - bbox.left,
            bbox.top,
            size.width / 2 + bbox.right,
            bbox.bottom,
          ),
        ),
      Paint()
        ..color = color
        ..style = PaintingStyle.stroke
        ..strokeWidth = 6,
    );
  }

  @override
  bool shouldRepaint(OvalPainter oldDelegate) =>
      oldDelegate.color != color ||
      oldDelegate.bbox != bbox ||
      oldDelegate.right != right;
}
