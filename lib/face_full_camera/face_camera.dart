import 'dart:developer';

import 'package:camera/camera.dart';
import 'package:clinic/components/common/draw_circle.dart';
import 'package:clinic/face_full_camera/face_dete_painter.dart';
import 'package:clinic/face_full_camera/face_model_painter.dart';
import 'package:clinic/face_full_camera/services/face_detection/face_detection_service.dart';
import 'package:clinic/face_full_camera/services/face_mesh/face_mesh_service.dart';
import 'package:clinic/face_full_camera/services/model_inference_service.dart';

import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:rxdart/subjects.dart';

double topRect = 0.2;
double topLRect = 0.44;
double bottomLRect = 0.54;
double bottomRect = 0.85;

class FaceCameraController {
  late BehaviorSubject<List<Offset>?> pointsBehavior;
  late BehaviorSubject<Rect?> rectBehavior;
  late BehaviorSubject<List<double>?> rotation;
  late BehaviorSubject<bool> leftFace;
  late BehaviorSubject<bool> rightFace;
  late BehaviorSubject<CameraController?> cameraController;
  late BehaviorSubject<img.Image?> image;
  late BehaviorSubject<CameraImage?> imageController;

  FaceCameraController() {
    pointsBehavior = BehaviorSubject.seeded(null);
    rectBehavior = BehaviorSubject.seeded(null);
    rotation = BehaviorSubject.seeded(null);
    leftFace = BehaviorSubject.seeded(false);
    rightFace = BehaviorSubject.seeded(false);
    cameraController = BehaviorSubject.seeded(null);
    imageController = BehaviorSubject.seeded(null);
    image = BehaviorSubject.seeded(null);
  }

  void close() {
    pointsBehavior.close();
    rectBehavior.close();
    rotation.close();
    leftFace.close();
    rightFace.close();
    imageController.close();
    image.close();
  }
}

class FaceCamera extends StatefulWidget {
  final bool draw;
  final FaceCameraController controller;

  const FaceCamera({
    this.draw = true,
    required this.controller,
    super.key,
  });

  @override
  State<FaceCamera> createState() => _FaceCameraState();
}

class _FaceCameraState extends State<FaceCamera> with WidgetsBindingObserver {
  CameraController? controller;
  late ModelInferenceService<FaceMesh> _mechService;
  late ModelInferenceService<FaceDetection> _detectionService;
  bool _predicting = false;
  late CameraDescription _cameraDescription;

  List<CameraDescription>? _cameras;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    _initStateAsync();
  }

  void _initStateAsync() async {
    _mechService = ModelInferenceService<FaceMesh>();
    _detectionService = ModelInferenceService<FaceDetection>();
    _predicting = true;
    await _initCamera();
    _predicting = false;
  }

  Future<void> _initCamera() async {
    _cameras ??= await availableCameras();
    _cameraDescription = _cameras!.firstWhere(
      (el) => el.lensDirection == CameraLensDirection.front,
    );

    _onNewCameraSelected(_cameraDescription);
  }

  Future<void> _inference({required CameraImage cameraImage}) async {
    if (controller?.value.isStreamingImages == false) return;
    log("IMAGE STREAM ");
    if (!mounted || _predicting) return;
    if (_mechService.model.interpreter == null ||
        _detectionService.model.interpreter == null) return;
    _predicting = true;
    try {
      _detectionService.inferenceResults = null;
      _mechService.inferenceResults = null;

      await _detectionService.inference(cameraImage: cameraImage);
      if (_detectionService.inferenceResults != null &&
          _detectionService.inferenceResults!['score'] > 0.92) {
        final rrect = _detectionService.inferenceResults!['bbox'] as Rect;

        await _mechService.inference(
          cameraImage: cameraImage,
          p1: rrect.topLeft - const Offset(50, 50),
          p2: rrect.bottomRight + const Offset(50, 50),
        );
      }

      if (widget.controller.pointsBehavior.isClosed) return;
      if (widget.controller.rectBehavior.isClosed) return;

      final Rect? rrect = _detectionService.inferenceResults?['bbox'];
      if (rrect != null) {
        final topAlign = rrect.top / cameraImage.height;
        final bottomAlign = rrect.bottom / cameraImage.height;

        if (topAlign > topLRect || topAlign < topRect) {
          widget.controller.pointsBehavior.value = null;
          widget.controller.rectBehavior.value = null;
          widget.controller.imageController.value = null;
          _predicting = false;
          return;
        }
        if (bottomAlign < bottomLRect || bottomAlign > bottomRect) {
          widget.controller.pointsBehavior.value = null;
          widget.controller.rectBehavior.value = null;
          widget.controller.imageController.value = null;
          _predicting = false;
          return;
        }
      }

      widget.controller.pointsBehavior.value =
          _mechService.inferenceResults?['point'];
      widget.controller.rectBehavior.value = rrect == null
          ? null
          : Rect.fromLTRB(
              rrect.left / cameraImage.width,
              rrect.top / cameraImage.height,
              rrect.right / cameraImage.width,
              rrect.bottom / cameraImage.height,
            );
      widget.controller.rotation.value =
          _mechService.inferenceResults?['rotate'];
      widget.controller.imageController.value = cameraImage;
      widget.controller.image.value = _mechService.inferenceResults?['image'];

      // widget.controller.rotation = _mechService.inferenceResults?['rotate'];
    } catch (e) {
      log(e.toString());
    }
    _predicting = false;
  }

  @override
  Widget build(BuildContext context) {
    if (!(controller?.value.isInitialized ?? false)) {
      return const SizedBox();
    }
    final screenSize = MediaQuery.of(context).size;
    final ratio = screenSize.width / controller!.value.previewSize!.height;
    final size = controller!.value.previewSize!;
    return Center(
      child: CameraPreview(
        controller!,
        child: Stack(
          children: [
            if (widget.draw)
              StreamBuilder(
                builder: (context, snapshot) => Stack(
                  children: [
                    RepaintBoundary(
                      child: CustomPaint(
                        painter: FaceDetectionPainter(
                          bbox: widget.controller.rectBehavior.value,
                          ratio: ratio,
                        ),
                        isComplex: true,
                      ),
                    ),
                    FaceModelPainter(
                      points: widget.controller.pointsBehavior.value,
                      ratio: ratio,
                    ),
                  ],
                ),
                stream: widget.controller.pointsBehavior,
              ),
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: widget.controller.leftFace,
                builder: (context, snapshot) => StreamBuilder(
                  stream: widget.controller.rightFace,
                  builder: (context, snapshot) => DrawCircle(
                    size: size,
                    left: widget.controller.leftFace.value,
                    right: widget.controller.rightFace.value,
                  ),
                ),
                // child: ,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onNewCameraSelected(CameraDescription cameraDescription) async {
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: false,
    );
    widget.controller.cameraController.value = controller;
    controller!.addListener(() {
      if (mounted) setState(() {});
    });

    try {
      await controller!.initialize();
    } on CameraException catch (e) {}

    if (mounted) {
      setState(() {});
    }
    controller!.startImageStream(listen);
  }

  void listen(image) async => await _inference(cameraImage: image);

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if (controller?.value.isStreamingImages == true) {
      controller?.stopImageStream();
    }

    controller?.dispose();

    _mechService.inferenceResults = null;
    _detectionService.inferenceResults = null;

    _mechService.close();
    _detectionService.close();
    super.dispose();
  }
}
