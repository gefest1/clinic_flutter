import 'dart:async';
import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:clinic/face_full_camera/services/face_detection/face_detection_service.dart';
import 'package:flutter/material.dart';

import '../../utils/isolate_utils.dart';
import 'ai_model.dart';
import 'face_mesh/face_mesh_service.dart';
// import 'hands/hands_service.dart';
// import 'pose/pose_service.dart';
// import 'service_locator.dart';

// enum Models {
//   FaceDetection,
//   faceMesh,
// Hands,
// Pose,
// }

class ModelInferenceService<T extends AiModel> {
  // final FaceDetection faceDetection;
  // final FaceMesh faceMesh;
  late T model;
  late Function handler;
  late Completer<IsolateUtils> _isolateUtilsCompl;
  // late IsolateUtils _isolateUtils;
  Map<String, dynamic>? inferenceResults;
  ModelInferenceService() {
    setModelConfig();
    _init();
  }

  void _init() async {
    _isolateUtilsCompl = Completer<IsolateUtils>();
    final isol = IsolateUtils();
    await isol.initIsolate();
    _isolateUtilsCompl.complete(isol);
  }

  void close() async {
    final isolate = await _isolateUtilsCompl.future;
    model.interpreter?.close();
    isolate.dispose();
  }

  Future<Map<String, dynamic>?> inference({
    required CameraImage cameraImage,
    Offset? p1,
    Offset? p2,
  }) async {
    final responsePort = ReceivePort();
    final isolate = await _isolateUtilsCompl.future;
    isolate.sendMessage(
      handler: handler,
      params: {
        'cameraImage': cameraImage,
        'detectorAddress': model.getAddress,
        'bbox': {
          'p1': p1,
          'p2': p2,
        },
      },
      sendPort: isolate.sendPort,
      responsePort: responsePort,
    );

    inferenceResults = await responsePort.first;
    responsePort.close();
  }

  void setModelConfig() {
    if (T == FaceDetection) {
      model = FaceDetection() as T;
      handler = runFaceDetector;
      return;
    }
    if (T == FaceMesh) {
      model = FaceMesh() as T;
      handler = runFaceMesh;
      return;
    }
    throw 'Need Template';
  }
}
