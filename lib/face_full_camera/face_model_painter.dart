import 'package:clinic/widgets/face_mesh_painter.dart';
import 'package:flutter/material.dart';

class FaceModelPainter extends StatelessWidget {
  final List<Offset>? points;
  final double ratio;

  const FaceModelPainter({
    required this.points,
    required this.ratio,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: FaceMeshPainter(
        points: points ?? [],
        ratio: ratio,
      ),
    );
  }
}
