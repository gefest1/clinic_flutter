import 'dart:developer';
import 'dart:math' as math;

import 'package:flutter/material.dart';

double euclideanDist(x1, y1, x2, y2) {
  return math.sqrt(math.pow((x1 - x2), 2) + math.pow((y1 - y2), 2));
}

bool detectBlink(final List<Offset> points) {
  const leftEyeLeft = 263;
  const leftEyeRight = 362;
  const leftEyeTop = 386;
  const leftEyeBottom = 374;

  const rightEyeLeft = 133;
  const rightEyeRight = 33;
  const rightEyeTop = 159;
  const rightEyeBottom = 145;

  final aL = euclideanDist(points[leftEyeTop].dx, points[leftEyeTop].dy,
      points[leftEyeBottom].dx, points[leftEyeBottom].dy);
  final bL = euclideanDist(
    points[leftEyeLeft].dx,
    points[leftEyeLeft].dy,
    points[leftEyeRight].dx,
    points[leftEyeRight].dy,
  );
  final earLeft = aL / (2 * bL);

  final aR = euclideanDist(
    points[rightEyeTop].dx,
    points[rightEyeTop].dy,
    points[rightEyeBottom].dx,
    points[rightEyeBottom].dy,
  );
  final bR = euclideanDist(
    points[rightEyeLeft].dx,
    points[rightEyeLeft].dy,
    points[rightEyeRight].dx,
    points[rightEyeRight].dy,
  );
  final earRight = aR / (2 * bR);
  if ((earLeft < 0.1) || (earRight < 0.1)) {
    return true;
  } else {
    return false;
  }
}

double l2Norm(List<double> vec) {
  double norm = 0;
  for (final v in vec) {
    norm += v * v;
  }
  return math.sqrt(norm);
}

List<double> rotateDetect(final List<List<dynamic>> keypoints) {
  // V: 10, 152; H: 226, 446
  final faceVerticalCentralPoint = [
    0,
    (keypoints[10][1] + keypoints[152][1]) * 0.5,
    (keypoints[10][2] + keypoints[152][2]) * 0.5,
  ];
  final verticalAdjacent = keypoints[10][2] - faceVerticalCentralPoint[2];
  final verticalOpposite = keypoints[10][1] - faceVerticalCentralPoint[1];
  final verticalHypotenuse = l2Norm([
    verticalAdjacent.toDouble(),
    verticalOpposite.toDouble(),
  ]);
  final verticalCos = verticalAdjacent / verticalHypotenuse;
  final faceHorizontalCentralPoint = [
    (keypoints[226][0] + keypoints[446][0]) * 0.5,
    0,
    (keypoints[226][2] + keypoints[446][2]) * 0.5,
  ];
  final horizontalAdjacent = keypoints[226][2] - faceHorizontalCentralPoint[2];
  final horizontalOpposite = keypoints[226][0] - faceHorizontalCentralPoint[0];
  final horizontalHypotenuse = l2Norm([
    horizontalAdjacent.toDouble(),
    horizontalOpposite.toDouble(),
  ]);
  final horizontalCos = horizontalAdjacent / horizontalHypotenuse;

  return [
    verticalCos,
    horizontalCos,
  ];
}

bool detectMouth(final List<Offset> points) {
  final off = points[14] - points[78];

  return (math.atan(off.dy / off.dx) * (180.0 / math.pi)) > 20;
}
