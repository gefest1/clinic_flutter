DateTime? dateTimeParse(dynamic data) {
  if (data == null) return null;
  if (data is int) return DateTime.fromMillisecondsSinceEpoch(data).toLocal();
  return DateTime.parse(data).toLocal();
}

String? toMapDateTimeParse(DateTime? date) {
  return date?.toUtc().toIso8601String();
}

DateTime dateTimeParseG(dynamic data) {
  if (data is int) return DateTime.fromMillisecondsSinceEpoch(data).toLocal();
  return DateTime.parse(data).toLocal();
}

String toMapDateTimeParseG(DateTime date) {
  return date.toUtc().toIso8601String();
}
