import 'dart:async';

import 'package:clinic/main.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/text.dart';
import 'package:flutter/material.dart';

class ErrorOverlay extends OverlayEntry {
  final String title;

  ErrorOverlay({
    required this.title,
  }) : super(
          builder: (BuildContext context) {
            return const SizedBox();
          },
        );

  @override
  WidgetBuilder get builder => (BuildContext context) {
        return _ErrorWidget(
          errorOverlay: this,
          title: title,
        );
      };
}

void showError(String errorText) {
  navgitorKey.currentState?.overlay?.insert(
    ErrorOverlay(title: errorText),
  );
}

class _ErrorWidget extends StatefulWidget {
  final String title;
  final ErrorOverlay errorOverlay;

  const _ErrorWidget({
    required this.title,
    required this.errorOverlay,
  });

  @override
  State<_ErrorWidget> createState() => _ErrorWidgetState();
}

class _ErrorWidgetState extends State<_ErrorWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animationCurve;
  late Animation<Offset> animationOffset;

  late final Timer removeTimer;
  late final Timer hideTimer;

  void removeOverlay() {
    widget.errorOverlay.remove();
  }

  void hideTerminal() {
    if (!mounted) return;
    animationController.animateTo(
      0,
      duration: const Duration(milliseconds: 600),
      curve: Curves.easeIn,
    );
  }

  @override
  void initState() {
    removeTimer = Timer(
      const Duration(seconds: 4),
      removeOverlay,
    );
    hideTimer =
        Timer(const Duration(seconds: 3, milliseconds: 300), hideTerminal);
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 700),
      lowerBound: 0,
      upperBound: 1,
      value: 0,
    );
    animationOffset = Tween<Offset>(
      begin: const Offset(0, -1.4),
      end: const Offset(0, 0),
    ).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationCurve =
        CurveTween(curve: Curves.easeIn).animate(animationController);

    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top + 40,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: FadeTransition(
              opacity: animationCurve,
              child: SlideTransition(
                position: animationOffset,
                child: Container(
                  constraints: const BoxConstraints(maxHeight: 140),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(
                      color: const Color(0xffFF2F2F),
                      width: 0.5,
                    ),
                  ),
                  padding: const EdgeInsets.all(15),
                  child: Row(
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.warning_amber,
                          size: 24,
                          color: Color(0xffFF2F2F),
                        ),
                      ),
                      Expanded(
                        child: P2Text(
                          widget.title,
                          color: ColorData.colorTextMain,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
