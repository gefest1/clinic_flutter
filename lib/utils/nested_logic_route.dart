import 'dart:developer';
import 'dart:io';

import 'package:clinic/utils/routes.observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NestedLogicRoute extends StatefulWidget {
  final Widget child;

  const NestedLogicRoute({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<NestedLogicRoute> createState() => _NestedLogicRouteState();
}

class _NestedLogicRouteState extends State<NestedLogicRoute> {
  MyNavigatorObserver observer = MyNavigatorObserver();
  GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    Widget child = Navigator(
      key: navKey,
      onPopPage: (route, result) {
        if (observer.routeStack.length != 1) return true;
        Navigator.pop(context, result);
        return false;
      },
      observers: [observer],
      pages: [
        Platform.isIOS || Platform.isMacOS
            ? CupertinoPage(
                child: widget.child,
              )
            : MaterialPage(
                child: widget.child,
              ),
      ],
    );
    if (Platform.isIOS || Platform.isMacOS) return child;

    return WillPopScope(
      onWillPop: () async {
        log("DWADWA");
        if (Platform.isIOS || Platform.isMacOS) return true;
        if (observer.routeStack.length == 1) return true;
        navKey.currentState?.pop();
        return false;
      },
      child: child,
    );
  }
}
