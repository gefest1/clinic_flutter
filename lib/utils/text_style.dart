import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class H6TextStyle {
  static const double fontSize = 12;
  static const FontWeight fontWeight = FontWeight.w700;
  static const double height = 14 / 12;
}

class H5TextStyle {
  static const double fontSize = 14;
  static const FontWeight fontWeight = FontWeight.w700;
  static const double height = 17 / 14;
}

class H4TextStyle {
  static const double fontSize = 18;
  static const FontWeight fontWeight = FontWeight.w700;
  static const double height = 21 / 18;
}

class H3TextStyle {
  static const double fontSize = 24;
  static const FontWeight fontWeight = FontWeight.bold;
  static const double height = 28 / 24;
}

class H2TextStyle {
  static const double fontSize = 32;
  static const FontWeight fontWeight = FontWeight.bold;
  static const double height = 36 / 32;
}

class H1TextStyle {
  static const double fontSize = 48;
  static const FontWeight fontWeight = FontWeight.w700;
  static const double height = 57 / 48;
}

class P0TextStyle {
  static const double fontSize = 18;
  static const FontWeight fontWeight = FontWeight.w400;
  static const double height = 21 / 18;
}

class P1TextStyle {
  static const double fontSize = 16;
  static const FontWeight fontWeight = FontWeight.normal;
  static const double height = 19 / 16;
}

class P2TextStyle {
  static const double fontSize = 14;
  static const FontWeight fontWeight = FontWeight.normal;
  static const double height = 17 / 14;
}

const List<BoxShadow> blocksMain = [
  BoxShadow(
    offset: Offset(0, -3),
    blurRadius: 2,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(-3, 0),
    blurRadius: 2,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(3, 0),
    blurRadius: 2,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(3, 0),
    blurRadius: 20,
    color: Color(0x03000000),
  ),
  BoxShadow(
    offset: Offset(0, 5),
    blurRadius: 20,
    color: Color(0x0d000000),
  ),
];

// const httpUrlPath =
//     kDebugMode ? _debugHttpUrlPath : "https://api.checkallnow.net";
// const _debugHttpUrlPath = "https://api.checkallnow.net"; //ngrok
