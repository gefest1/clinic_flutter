import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as image_lib;

/// ImageUtils
class ImageUtils {
  /// Converts a [CameraImage] in YUV420 format to [Image] in RGB format
  static image_lib.Image? convertCameraImage(
    CameraImage cameraImage, {
    Offset? p1,
    Offset? p2,
  }) {
    image_lib.Image? img;
    //  img =image_lib.copyCrop(img, 0 ?? p1., 0, w, h);
    if (cameraImage.format.group == ImageFormatGroup.yuv420) {
      img = convertYUV420ToImage(cameraImage);
    } else if (cameraImage.format.group == ImageFormatGroup.bgra8888) {
      img = convertBGRA8888ToImage(cameraImage);
    } else {
      return null;
    }
    if (p1 == null && p2 == null) return img;
    final startX = p1?.dx.toInt() ?? 0;
    final startY = p1?.dy.toInt() ?? 0;

    final endX = p2?.dx.toInt() ?? img.width;
    final endY = p2?.dy.toInt() ?? img.height;
    img = image_lib.copyCrop(
      img,
      startX,
      startY,
      endX - startX,
      endY - startY,
    );
    return img;
  }

  /// Converts a [CameraImage] in BGRA888 format to [Image] in RGB format
  static image_lib.Image convertBGRA8888ToImage(CameraImage cameraImage) {
    return image_lib.Image.fromBytes(
      cameraImage.planes[0].width!,
      cameraImage.planes[0].height!,
      cameraImage.planes[0].bytes,
      format: image_lib.Format.bgra,
    );
  }

  /// Converts a [CameraImage] in YUV420 format to [Image] in RGB format
  static image_lib.Image convertYUV420ToImage(CameraImage cameraImage) {
    final width = cameraImage.width;
    final height = cameraImage.height;

    final uvRowStride = cameraImage.planes[1].bytesPerRow;
    final uvPixelStride = cameraImage.planes[1].bytesPerPixel!;

    final image = image_lib.Image(width, height);

    for (var w = 0; w < width; w++) {
      for (var h = 0; h < height; h++) {
        final uvIndex =
            uvPixelStride * (w / 2).floor() + uvRowStride * (h / 2).floor();
        final index = h * width + w;

        final y = cameraImage.planes[0].bytes[index];
        final u = cameraImage.planes[1].bytes[uvIndex];
        final v = cameraImage.planes[2].bytes[uvIndex];

        image.data[index] = ImageUtils.yuv2rgb(y, u, v);
      }
    }
    return image;
  }

  /// Convert a single YUV pixel to RGB
  static int yuv2rgb(int y, int u, int v) {
    // Convert yuv pixel to rgb
    var r = (y + v * 1436 / 1024 - 179).round();
    var g = (y - u * 46549 / 131072 + 44 - v * 93604 / 131072 + 91).round();
    var b = (y + u * 1814 / 1024 - 227).round();

    // Clipping RGB values to be inside boundaries [ 0 , 255 ]
    r = r.clamp(0, 255);
    g = g.clamp(0, 255);
    b = b.clamp(0, 255);

    return 0xff000000 |
        ((b << 16) & 0xff0000) |
        ((g << 8) & 0xff00) |
        (r & 0xff);
  }
}
