import 'dart:developer';

import 'package:clinic/logic/register_logic.dart';
import 'package:clinic/logic/tunduk/organization/organization_bloc.dart';
import 'package:clinic/logic/user_bloc.dart';
import 'package:clinic/pages/auth/init_auth_page.dart';
import 'package:clinic/pages/main_template.dart';
import 'package:clinic/pages/restart_widget.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/hive_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'dart:io' as io;
import 'package:intl/locale.dart' as intlLocale;
import 'package:collection/collection.dart';

late Box sharedBox;
late GlobalKey<NavigatorState> navgitorKey;
late final HiveSICache persistentCache;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  sharedBox = await Hive.openBox('box');
  persistentCache = HiveSICache(await Hive.openBox<Object>('svgCache'));

  runApp(
    RestartWidget(
      child: MultiProvider(
        providers: [
          Provider<RegisterLogic>(
            create: (context) => RegisterLogic(),
          ),
          Provider<UserBloc>(
            create: (context) => UserBloc(),
          ),
          BlocProvider<OrganizationBloc>(
            create: (context) => OrganizationBloc(
              pin: "20504199900686",
              // pin: "21206199600951",
            ),
          ),
        ],
        child: const MyApp(),
      ),
    ),
  );
}

abstract class LocalState extends State<MyApp> {
  void setLocale(Locale value);
  late Locale locale;
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  static LocalState? of(BuildContext context) =>
      context.findAncestorStateOfType<_MyAppState>();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> implements LocalState {
  final String defaultLocale = io.Platform.localeName;
  @override
  late Locale locale;

  @override
  void setLocale(Locale value) {
    setState(() {
      locale = value;
      sharedBox.put('languageCode', locale.languageCode);
    });
  }

  @override
  void initState() {
    if (sharedBox.containsKey('languageCode')) {
      locale = Locale(sharedBox.get('languageCode'));
    } else {
      locale = AppLocalizations.supportedLocales.firstWhereOrNull((element) =>
              element.languageCode ==
              intlLocale.Locale.parse(io.Platform.localeName).languageCode) ??
          const Locale('ky');
    }

    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
    );
    navgitorKey = GlobalKey();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // log(sharedBox.get('accessToken').toString());

    return MaterialApp(
      locale: locale,
      debugShowCheckedModeBanner: false,
      navigatorKey: navgitorKey,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      title: 'Flutter Demo',
      theme: ThemeData(
          appBarTheme: const AppBarTheme(
        backgroundColor: ColorData.colorMainElements,
      )),
      home:
          // const MainTemplate(),
          sharedBox.containsKey('accessToken')
              ? const MainTemplate()
              : const InitAuthPage(),
    );
  }
}
