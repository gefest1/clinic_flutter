import 'package:bloc/bloc.dart';
import 'package:clinic/api/client.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';

abstract class UserEvent {
  const UserEvent();
}

class GetUserEvent extends UserEvent {
  const GetUserEvent();
}

abstract class UserState extends Equatable {
  const UserState();
}

class LoadUserState extends UserState {
  const LoadUserState();
  @override
  List<Object?> get props => [];
}

class DataUserState extends UserState {
  final DateTime dateBirth;
  final String firstName;
  final String id;
  final String lastName;
  final String patronymic;
  final String uniqueNumber;

  const DataUserState({
    required this.dateBirth,
    required this.firstName,
    required this.id,
    required this.lastName,
    required this.patronymic,
    required this.uniqueNumber,
  });

  @override
  List<Object?> get props => [
        dateBirth,
        firstName,
        id,
        lastName,
        patronymic,
        uniqueNumber,
      ];
}

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const LoadUserState()) {
    on<GetUserEvent>(getUserDataEvent);
    add(const GetUserEvent());
  }

  Future<void> getUserDataEvent(
      GetUserEvent event, Emitter<UserState> emit) async {
    final res = await graphqlClient.query$getUser(
      Options$Query$getUser(
        fetchPolicy: FetchPolicy.networkOnly,
      ),
    );
    if (res.hasException) throw res.exception!;
    final userData = res.parsedData!.getUserData;

    emit(
      DataUserState(
        dateBirth: userData.dateBirth,
        firstName: userData.firstName,
        id: userData.id,
        lastName: userData.lastName,
        patronymic: userData.patronymic,
        uniqueNumber: userData.uniqueNumber,
      ),
    );
  }
}
