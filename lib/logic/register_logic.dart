class RegisterLogic {
  String? phoneNumber;
  String? uniqueNumber;
  DateTime? dateBirn;

  String? firstName;
  String? lastName;
  String? patronymic;

  RegisterLogic({
    this.phoneNumber,
    this.uniqueNumber,
    this.dateBirn,
    this.firstName,
    this.lastName,
    this.patronymic,
  });
  void setPhoneNumber(String? phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  void setUniqueNumber(String? uniqueNumber) {
    this.uniqueNumber = uniqueNumber;
  }

  void setFirstName(String? firstName) {
    this.firstName = firstName;
  }

  void setLastName(String? lastName) {
    this.lastName = lastName;
  }

  void setPatronymic(String? patronymic) {
    this.patronymic = patronymic;
  }

  void setDateBirn(DateTime? dateBirn) {
    this.dateBirn = dateBirn;
  }
}
