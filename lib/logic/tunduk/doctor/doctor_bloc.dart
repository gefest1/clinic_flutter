import 'package:clinic/api/client1.dart';
import 'package:clinic/logic/tunduk/doctor/doctor_event.dart';
import 'package:clinic/logic/tunduk/doctor/doctor_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DoctorBloc extends Bloc<DoctorEvent, DoctorState> {
  static final Map<String, DoctorBloc> _blocsMap = {};
  static DoctorBloc getDoctorBloc(String organisationId) =>
      _blocsMap[organisationId] ??= DoctorBloc(organisationId: organisationId);

  final String organisationId;
  DoctorBloc({required this.organisationId})
      : super(const LoadingDoctorState()) {
    on<GetDoctorEvent>(getEvent);
    add(const GetDoctorEvent());
  }

  void getEvent(
    GetDoctorEvent event,
    Emitter<DoctorState> emit,
  ) async {
    final result = await tundukGraphqlClient.query$getDoctorListByOrgId(
      Options$Query$getDoctorListByOrgId(
        variables: Variables$Query$getDoctorListByOrgId(
          organisationId: organisationId,
        ),
      ),
    );
    if (result.hasException) throw result.exception!;
    final prasedArray = result.parsedData!.getDoctorListByOrgId!.result!
        .whereType<Fragment$DoctorResult>()
        .toList();
    emit(DataDoctorState(data: prasedArray));
  }
}
