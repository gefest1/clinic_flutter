abstract class DoctorEvent {
  const DoctorEvent();
}

class GetDoctorEvent extends DoctorEvent {
  const GetDoctorEvent();
}
