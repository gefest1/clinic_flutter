import 'package:clinic/api/client1.dart';
import 'package:equatable/equatable.dart';

abstract class DoctorState extends Equatable {
  const DoctorState();
}

class LoadingDoctorState extends DoctorState {
  const LoadingDoctorState();

  @override
  List<Object?> get props => [];
}

class DataDoctorState extends DoctorState {
  final List<Fragment$DoctorResult> data;

  const DataDoctorState({required this.data});

  @override
  List<Object?> get props => [data];
}
