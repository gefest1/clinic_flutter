abstract class OrganizationEvent {
  const OrganizationEvent();
}

class GetOrganizationEvent extends OrganizationEvent {
  const GetOrganizationEvent();
}
