import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:clinic/api/client1.dart';
import 'package:clinic/logic/tunduk/organization/organization_event.dart';
import 'package:clinic/logic/tunduk/organization/organization_state.dart';

class OrganizationBloc extends Bloc<OrganizationEvent, OrganizationState> {
  final String pin;

  OrganizationBloc({
    required this.pin,
  }) : super(const LoadingOrganizationState()) {
    on<GetOrganizationEvent>(getData);

    add(const GetOrganizationEvent());
  }

  void getData(
    GetOrganizationEvent event,
    Emitter<OrganizationState> emit,
  ) async {
    try {
      final result = await tundukGraphqlClient.query$searchPatientByINN(
        Options$Query$searchPatientByINN(
          variables: Variables$Query$searchPatientByINN(
            pin: pin,
          ),
        ),
      );
      if (result.hasException) throw result.exception!;
      final fragment = result.parsedData!.searchPatientByINN!;
      emit(
        DataOrganizationState(data: fragment),
      );
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
  }
}
