import 'package:clinic/api/client1.dart';
import 'package:equatable/equatable.dart';

abstract class OrganizationState extends Equatable {
  const OrganizationState();
}

class LoadingOrganizationState extends OrganizationState {
  const LoadingOrganizationState();

  @override
  List<Object?> get props => [];
}

class DataOrganizationState extends OrganizationState {
  final Fragment$SearchPatientByINNOutputFragment data;

  const DataOrganizationState({
    required this.data,
  });

  @override
  List<Object?> get props => [data];
}
