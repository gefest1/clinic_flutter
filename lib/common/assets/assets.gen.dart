/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/hand_green.png
  AssetGenImage get handGreen =>
      const AssetGenImage('assets/images/hand_green.png');

  /// File path: assets/images/mediapipe.png
  AssetGenImage get mediapipe =>
      const AssetGenImage('assets/images/mediapipe.png');

  /// File path: assets/images/photo_2023-04-23_16-29-43.jpg
  AssetGenImage get photo20230423162943 =>
      const AssetGenImage('assets/images/photo_2023-04-23_16-29-43.jpg');

  /// File path: assets/images/photo_2023-05-13_15-56-15.jpg
  AssetGenImage get photo20230513155615 =>
      const AssetGenImage('assets/images/photo_2023-05-13_15-56-15.jpg');

  /// File path: assets/images/photo_2023-05-13_15-58-16.jpg
  AssetGenImage get photo20230513155816 =>
      const AssetGenImage('assets/images/photo_2023-05-13_15-58-16.jpg');

  /// File path: assets/images/pose_red.png
  AssetGenImage get poseRed =>
      const AssetGenImage('assets/images/pose_red.png');

  /// File path: assets/images/smile_blue.png
  AssetGenImage get smileBlue =>
      const AssetGenImage('assets/images/smile_blue.png');

  /// File path: assets/images/smile_yellow.png
  AssetGenImage get smileYellow =>
      const AssetGenImage('assets/images/smile_yellow.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        handGreen,
        mediapipe,
        photo20230423162943,
        photo20230513155615,
        photo20230513155816,
        poseRed,
        smileBlue,
        smileYellow
      ];
}

class $AssetsModelsGen {
  const $AssetsModelsGen();

  /// File path: assets/models/face_detection_short_range.tflite
  String get faceDetectionShortRange =>
      'assets/models/face_detection_short_range.tflite';

  /// File path: assets/models/face_landmark.tflite
  String get faceLandmark => 'assets/models/face_landmark.tflite';

  /// File path: assets/models/hand_landmark.tflite
  String get handLandmark => 'assets/models/hand_landmark.tflite';

  /// File path: assets/models/pose_landmark_full.tflite
  String get poseLandmarkFull => 'assets/models/pose_landmark_full.tflite';

  /// List of all assets
  List<String> get values =>
      [faceDetectionShortRange, faceLandmark, handLandmark, poseLandmarkFull];
}

class $AssetsSvgsGen {
  const $AssetsSvgsGen();

  /// File path: assets/svgs/app_background.svg
  String get appBackground => 'assets/svgs/app_background.svg';

  /// File path: assets/svgs/apteka.svg
  String get apteka => 'assets/svgs/apteka.svg';

  /// File path: assets/svgs/building.svg
  String get building => 'assets/svgs/building.svg';

  $AssetsSvgsCategoryGen get category => const $AssetsSvgsCategoryGen();

  /// File path: assets/svgs/doctors.svg
  String get doctors => 'assets/svgs/doctors.svg';

  /// File path: assets/svgs/illustration.svg
  String get illustration => 'assets/svgs/illustration.svg';

  $AssetsSvgsLogoGen get logo => const $AssetsSvgsLogoGen();

  /// File path: assets/svgs/medicine.svg
  String get medicine => 'assets/svgs/medicine.svg';

  /// File path: assets/svgs/pharmacist.svg
  String get pharmacist => 'assets/svgs/pharmacist.svg';

  /// File path: assets/svgs/profile.svg
  String get profile => 'assets/svgs/profile.svg';

  /// List of all assets
  List<String> get values => [
        appBackground,
        apteka,
        building,
        doctors,
        illustration,
        medicine,
        pharmacist,
        profile
      ];
}

class $AssetsSvgsCategoryGen {
  const $AssetsSvgsCategoryGen();

  /// File path: assets/svgs/category/booking.svg
  String get booking => 'assets/svgs/category/booking.svg';

  /// File path: assets/svgs/category/cert.svg
  String get cert => 'assets/svgs/category/cert.svg';

  /// File path: assets/svgs/category/hosp.svg
  String get hosp => 'assets/svgs/category/hosp.svg';

  /// File path: assets/svgs/category/lab.svg
  String get lab => 'assets/svgs/category/lab.svg';

  /// File path: assets/svgs/category/online_cons.svg
  String get onlineCons => 'assets/svgs/category/online_cons.svg';

  /// File path: assets/svgs/category/pharm.svg
  String get pharm => 'assets/svgs/category/pharm.svg';

  /// File path: assets/svgs/category/reciept.svg
  String get reciept => 'assets/svgs/category/reciept.svg';

  /// File path: assets/svgs/category/stat.svg
  String get stat => 'assets/svgs/category/stat.svg';

  /// List of all assets
  List<String> get values =>
      [booking, cert, hosp, lab, onlineCons, pharm, reciept, stat];
}

class $AssetsSvgsLogoGen {
  const $AssetsSvgsLogoGen();

  /// File path: assets/svgs/logo/pulse.svg
  String get pulse => 'assets/svgs/logo/pulse.svg';

  /// File path: assets/svgs/logo/pulse_ugly.svg
  String get pulseUgly => 'assets/svgs/logo/pulse_ugly.svg';

  /// List of all assets
  List<String> get values => [pulse, pulseUgly];
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsModelsGen models = $AssetsModelsGen();
  static const $AssetsSvgsGen svgs = $AssetsSvgsGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
