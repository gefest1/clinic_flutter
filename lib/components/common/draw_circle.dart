import 'package:clinic/face_full_camera/face_camera.dart';
import 'package:clinic/face_full_camera/face_dete_painter.dart';
import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';

class DrawCircle extends StatelessWidget {
  final bool right;
  final bool left;
  final Size size;

  const DrawCircle({
    super.key,
    required this.size,
    this.right = false,
    this.left = false,
  });

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            child: CustomPaint(
              painter: OvalPainter(
                color:
                    right ? ColorData.abonement : ColorData.colorElementsActive,
                right: true,
                bbox: Rect.fromLTRB(
                  (topRect - bottomRect) * size.height * 0.33,
                  topRect * size.height,
                  (topRect - bottomRect) * size.height * 0.33,
                  0.95 * bottomRect * size.height,
                ),
                // ratio: ratio,
              ),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            child: CustomPaint(
              painter: OvalPainter(
                color:
                    left ? ColorData.abonement : ColorData.colorElementsActive,
                right: false,
                bbox: Rect.fromLTRB(
                  (topRect - bottomRect) * size.height * 0.33,
                  topRect * size.height,
                  (topRect - bottomRect) * size.height * 0.33,
                  0.95 * bottomRect * size.height,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
