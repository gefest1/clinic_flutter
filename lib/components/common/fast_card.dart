import 'package:flutter/material.dart';

class FastCard extends StatefulWidget {
  final String? photo;
  const FastCard({this.photo, super.key});

  @override
  State<FastCard> createState() => _FastCardState();
}

class _FastCardState extends State<FastCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(10),
      ),
      child: widget.photo == null
          ? null
          : ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                widget.photo!,
                fit: BoxFit.cover,
              ),
            ),
    );
  }
}
