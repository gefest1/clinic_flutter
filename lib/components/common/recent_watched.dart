import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/components/common/text.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:clinic/components/common/fast_card.dart';

class RecentWatchedCard extends StatefulWidget {
  final String? title;
  final List<String> photos;
  final void Function(int)? onTap;

  const RecentWatchedCard({
    this.title,
    this.photos = const [],
    this.onTap,
    super.key,
  });

  @override
  State<RecentWatchedCard> createState() => _RecentWatchedCardState();
}

class _RecentWatchedCardState extends State<RecentWatchedCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: CustomText(
            widget.title ??
                AppLocalizations.of(context)?.recently_watched ??
                "Recently Watched",
            type: CustomTextType.h22,
            color: ColorData.basicColorDarkBlue,
          ),
        ),
        const SizedBox(height: 20),
        SizedBox(
          height: 150,
          child: ListView.separated(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => SizeTapAnimation(
              onTap:
                  widget.onTap == null ? null : () => widget.onTap?.call(index),
              child: SizedBox(
                width: 144,
                child: FastCard(
                  photo: widget.photos[index],
                ),
              ),
            ),
            separatorBuilder: (context, index) => const SizedBox(width: 20),
            itemCount: widget.photos.length,
          ),
        ),
        const SizedBox(height: 20),
        Align(
          alignment: Alignment.center,
          child: SizeTapAnimation(
            onTap: () {},
            child: const CustomText(
              'Все предложения',
              type: CustomTextType.tab,
              color: ColorData.link,
            ),
          ),
        ),
      ],
    );
  }
}
