import 'package:clinic/common/assets/assets.gen.dart';
import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/components/common/text.dart';
import 'package:clinic/pages/category_card.dart';
import 'package:clinic/pages/hospital/hospital_page.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';

class CategoryWrap extends StatefulWidget {
  const CategoryWrap({super.key});

  @override
  State<CategoryWrap> createState() => _CategoryWrapState();
}

class _CategoryWrapState extends State<CategoryWrap> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          child: CustomText(
            AppLocalizations.of(context)?.category ?? "category",
            type: CustomTextType.h22,
            color: ColorData.basicColorDarkBlue,
          ),
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CategoryCard(
              title: AppLocalizations.of(context)?.online_cons ??
                  "Онлайн\nКонсультации",
              svgLocal: Assets.svgs.category.onlineCons,
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.labs ?? 'Лаборатории',
              svgLocal: Assets.svgs.category.lab,
              onTap: () async {},
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.hosp ?? 'Больницы',
              svgLocal: Assets.svgs.category.hosp,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HospitalPage(),
                  ),
                );
              },
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.state ?? 'Справки',
              svgLocal: Assets.svgs.category.cert,
            ),
          ].map((e) => Expanded(child: e)).toList(),
        ),
        const SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CategoryCard(
              title: AppLocalizations.of(context)?.record ?? 'Записи',
              svgLocal: Assets.svgs.category.booking,
              onTap: () {},
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.visit ?? 'Приемы',
              onTap: () {},
              svgLocal: Assets.svgs.category.reciept,
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.pharm ?? 'Аптеки',
              svgLocal: Assets.svgs.category.pharm,
              onTap: () {},
            ),
            CategoryCard(
              title: AppLocalizations.of(context)?.hosp_point ?? 'Стационары',
              svgLocal: Assets.svgs.category.stat,
            ),
          ].map((e) => Expanded(child: e)).toList(),
        ),
        const SizedBox(height: 16),
        Align(
          alignment: Alignment.center,
          child: SizeTapAnimation(
            onTap: () {},
            child: CustomText(
              AppLocalizations.of(context)?.all_cat ?? 'Все категории',
              type: CustomTextType.tab,
              color: ColorData.link,
            ),
          ),
        ),
      ],
    );
  }
}
