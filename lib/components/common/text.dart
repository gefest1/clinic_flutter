import 'package:flutter/material.dart';

import 'dart:ui' as ui show TextHeightBehavior;

abstract class AppTextStyle {
  static const String? fontFamily = null;

  static const TextStyle h26 = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    fontSize: 26,
    height: 31 / 26,
  );

  static const TextStyle h22 = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    fontSize: 22,
    height: 27 / 22,
  );

  static const TextStyle h20 = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontSize: 20,
    height: 24 / 20,
  );
  static const TextStyle subTitles = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontSize: 18,
    height: 22 / 18,
  );
  static const TextStyle h16 = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w600,
    fontSize: 16,
    height: 19 / 16,
  );
  static const TextStyle medium16 = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontSize: 16,
    height: 19 / 16,
  );
  static const TextStyle regular = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    height: 19 / 16,
  );
  static const TextStyle medium = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontSize: 15,
    height: 16.6 / 15,
  );
  static const TextStyle tips = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontSize: 15,
    height: 16.6 / 15,
  );

  static const TextStyle tab = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w500,
    fontSize: 13,
    height: 15.5 / 13,
  );

  static const TextStyle smallTips = TextStyle(
    fontFamily: fontFamily,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    height: 15 / 12,
  );
}

enum CustomTextType {
  h26(AppTextStyle.h26), // 26
  h22(AppTextStyle.h22), // 22
  h20(AppTextStyle.h20), // 20
  subTitles(AppTextStyle.subTitles), // 18
  h16(AppTextStyle.h16), // 16
  medium16(AppTextStyle.medium16), // 16
  regular(AppTextStyle.regular), // 16
  medium(AppTextStyle.medium), // 15
  tips(AppTextStyle.tips), // 15
  tab(AppTextStyle.tab), // 13
  smallTips(AppTextStyle.smallTips); // 12

  const CustomTextType(this.textStyle);
  final TextStyle textStyle;
}

TextStyle textstyle(CustomTextType? type, Color? color,
    [TextStyle? textStyle]) {
  TextStyle rrText;
  if (type == null) {
    rrText = CustomTextType.regular.textStyle.copyWith(color: color);
  } else {
    rrText = type.textStyle.copyWith(color: color);
  }

  return (textStyle ?? const TextStyle()).copyWith(
    inherit: rrText.inherit,
    color: rrText.color,
    backgroundColor: rrText.backgroundColor,
    fontSize: rrText.fontSize,
    fontWeight: rrText.fontWeight,
    fontStyle: rrText.fontStyle,
    letterSpacing: rrText.letterSpacing,
    wordSpacing: rrText.wordSpacing,
    textBaseline: rrText.textBaseline,
    height: rrText.height,
    leadingDistribution: rrText.leadingDistribution,
    locale: rrText.locale,
    foreground: rrText.foreground,
    background: rrText.background,
    shadows: rrText.shadows,
    fontFeatures: rrText.fontFeatures,
    fontVariations: rrText.fontVariations,
    decoration: rrText.decoration,
    decorationColor: rrText.decorationColor,
    decorationStyle: rrText.decorationStyle,
    decorationThickness: rrText.decorationThickness,
    debugLabel: rrText.debugLabel,
    fontFamily: rrText.fontFamily,
    fontFamilyFallback: rrText.fontFamilyFallback,
    overflow: rrText.overflow,
  );
}

class CustomText extends StatelessWidget {
  final String data;
  final CustomTextType? type;
  final Color? color;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final Locale? locale;
  final bool? softWrap;
  final TextOverflow? overflow;
  final double? textScaleFactor;
  final int? maxLines;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final ui.TextHeightBehavior? textHeightBehavior;
  final TextStyle? textStyle;

  const CustomText(
    this.data, {
    this.type,
    this.color,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
    this.textStyle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      data,
      style: textstyle(type, color, textStyle),
      textAlign: textAlign,
      textDirection: textDirection,
      locale: locale,
      softWrap: softWrap,
      overflow: overflow,
      textScaleFactor: textScaleFactor,
      maxLines: maxLines,
      semanticsLabel: semanticsLabel,
      textWidthBasis: textWidthBasis,
      textHeightBehavior: textHeightBehavior,
    );
  }
}
