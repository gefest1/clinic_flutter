import 'dart:developer';
import 'dart:ui';

import 'package:clinic/utils/blink_detect.dart';
import 'package:flutter/material.dart';

class FaceMeshPainter extends CustomPainter {
  final List<Offset> points;
  final double ratio;

  FaceMeshPainter({
    required this.points,
    required this.ratio,
  });

  @override
  void paint(Canvas canvas, Size size) {
    if (points.isNotEmpty) {
      var paint1 = Paint()
        ..color = Colors.amber
        ..strokeWidth = 2;

      canvas.drawPoints(
        PointMode.points,
        points.map((point) => point * ratio).toList(),
        paint1,
      );

      canvas.drawPoints(
        PointMode.points,
        [points[160]].map((point) => point * ratio).toList(),
        paint1..color = Colors.red,
      );
      canvas.drawPoints(
        PointMode.points,
        [points[246]].map((point) => point * ratio).toList(),
        paint1..color = Colors.green,
      );
      canvas.drawPoints(
        PointMode.points,
        [points[7]].map((point) => point * ratio).toList(),
        paint1..color = Colors.green,
      );

      canvas.drawPoints(
        PointMode.points,
        [points[159]].map((point) => point * ratio).toList(),
        paint1..color = Colors.purple,
      );
      canvas.drawPoints(
        PointMode.points,
        [points[145]].map((point) => point * ratio).toList(),
        paint1..color = Colors.purple,
      );

      // log(rotateDetect(points).toString());
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
