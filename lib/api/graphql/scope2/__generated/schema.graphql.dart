class Input$DoctorAndTimeInfoInput {
  factory Input$DoctorAndTimeInfoInput({
    required String date,
    required String orgAndUserId,
    required String type,
  }) =>
      Input$DoctorAndTimeInfoInput._({
        r'date': date,
        r'orgAndUserId': orgAndUserId,
        r'type': type,
      });

  Input$DoctorAndTimeInfoInput._(this._$data);

  factory Input$DoctorAndTimeInfoInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$date = data['date'];
    result$data['date'] = (l$date as String);
    final l$orgAndUserId = data['orgAndUserId'];
    result$data['orgAndUserId'] = (l$orgAndUserId as String);
    final l$type = data['type'];
    result$data['type'] = (l$type as String);
    return Input$DoctorAndTimeInfoInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String get date => (_$data['date'] as String);
  String get orgAndUserId => (_$data['orgAndUserId'] as String);
  String get type => (_$data['type'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$date = date;
    result$data['date'] = l$date;
    final l$orgAndUserId = orgAndUserId;
    result$data['orgAndUserId'] = l$orgAndUserId;
    final l$type = type;
    result$data['type'] = l$type;
    return result$data;
  }

  CopyWith$Input$DoctorAndTimeInfoInput<Input$DoctorAndTimeInfoInput>
      get copyWith => CopyWith$Input$DoctorAndTimeInfoInput(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$DoctorAndTimeInfoInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$date = date;
    final lOther$date = other.date;
    if (l$date != lOther$date) {
      return false;
    }
    final l$orgAndUserId = orgAndUserId;
    final lOther$orgAndUserId = other.orgAndUserId;
    if (l$orgAndUserId != lOther$orgAndUserId) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (l$type != lOther$type) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$date = date;
    final l$orgAndUserId = orgAndUserId;
    final l$type = type;
    return Object.hashAll([
      l$date,
      l$orgAndUserId,
      l$type,
    ]);
  }
}

abstract class CopyWith$Input$DoctorAndTimeInfoInput<TRes> {
  factory CopyWith$Input$DoctorAndTimeInfoInput(
    Input$DoctorAndTimeInfoInput instance,
    TRes Function(Input$DoctorAndTimeInfoInput) then,
  ) = _CopyWithImpl$Input$DoctorAndTimeInfoInput;

  factory CopyWith$Input$DoctorAndTimeInfoInput.stub(TRes res) =
      _CopyWithStubImpl$Input$DoctorAndTimeInfoInput;

  TRes call({
    String? date,
    String? orgAndUserId,
    String? type,
  });
}

class _CopyWithImpl$Input$DoctorAndTimeInfoInput<TRes>
    implements CopyWith$Input$DoctorAndTimeInfoInput<TRes> {
  _CopyWithImpl$Input$DoctorAndTimeInfoInput(
    this._instance,
    this._then,
  );

  final Input$DoctorAndTimeInfoInput _instance;

  final TRes Function(Input$DoctorAndTimeInfoInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? date = _undefined,
    Object? orgAndUserId = _undefined,
    Object? type = _undefined,
  }) =>
      _then(Input$DoctorAndTimeInfoInput._({
        ..._instance._$data,
        if (date != _undefined && date != null) 'date': (date as String),
        if (orgAndUserId != _undefined && orgAndUserId != null)
          'orgAndUserId': (orgAndUserId as String),
        if (type != _undefined && type != null) 'type': (type as String),
      }));
}

class _CopyWithStubImpl$Input$DoctorAndTimeInfoInput<TRes>
    implements CopyWith$Input$DoctorAndTimeInfoInput<TRes> {
  _CopyWithStubImpl$Input$DoctorAndTimeInfoInput(this._res);

  TRes _res;

  call({
    String? date,
    String? orgAndUserId,
    String? type,
  }) =>
      _res;
}

class Input$GetDoctorListByOrgIdInput {
  factory Input$GetDoctorListByOrgIdInput({required String organisationId}) =>
      Input$GetDoctorListByOrgIdInput._({
        r'organisationId': organisationId,
      });

  Input$GetDoctorListByOrgIdInput._(this._$data);

  factory Input$GetDoctorListByOrgIdInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$organisationId = data['organisationId'];
    result$data['organisationId'] = (l$organisationId as String);
    return Input$GetDoctorListByOrgIdInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String get organisationId => (_$data['organisationId'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$organisationId = organisationId;
    result$data['organisationId'] = l$organisationId;
    return result$data;
  }

  CopyWith$Input$GetDoctorListByOrgIdInput<Input$GetDoctorListByOrgIdInput>
      get copyWith => CopyWith$Input$GetDoctorListByOrgIdInput(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$GetDoctorListByOrgIdInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$organisationId = organisationId;
    final lOther$organisationId = other.organisationId;
    if (l$organisationId != lOther$organisationId) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$organisationId = organisationId;
    return Object.hashAll([l$organisationId]);
  }
}

abstract class CopyWith$Input$GetDoctorListByOrgIdInput<TRes> {
  factory CopyWith$Input$GetDoctorListByOrgIdInput(
    Input$GetDoctorListByOrgIdInput instance,
    TRes Function(Input$GetDoctorListByOrgIdInput) then,
  ) = _CopyWithImpl$Input$GetDoctorListByOrgIdInput;

  factory CopyWith$Input$GetDoctorListByOrgIdInput.stub(TRes res) =
      _CopyWithStubImpl$Input$GetDoctorListByOrgIdInput;

  TRes call({String? organisationId});
}

class _CopyWithImpl$Input$GetDoctorListByOrgIdInput<TRes>
    implements CopyWith$Input$GetDoctorListByOrgIdInput<TRes> {
  _CopyWithImpl$Input$GetDoctorListByOrgIdInput(
    this._instance,
    this._then,
  );

  final Input$GetDoctorListByOrgIdInput _instance;

  final TRes Function(Input$GetDoctorListByOrgIdInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? organisationId = _undefined}) =>
      _then(Input$GetDoctorListByOrgIdInput._({
        ..._instance._$data,
        if (organisationId != _undefined && organisationId != null)
          'organisationId': (organisationId as String),
      }));
}

class _CopyWithStubImpl$Input$GetDoctorListByOrgIdInput<TRes>
    implements CopyWith$Input$GetDoctorListByOrgIdInput<TRes> {
  _CopyWithStubImpl$Input$GetDoctorListByOrgIdInput(this._res);

  TRes _res;

  call({String? organisationId}) => _res;
}

class Input$SearchPatientByINNInput {
  factory Input$SearchPatientByINNInput({required String inn}) =>
      Input$SearchPatientByINNInput._({
        r'inn': inn,
      });

  Input$SearchPatientByINNInput._(this._$data);

  factory Input$SearchPatientByINNInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$inn = data['inn'];
    result$data['inn'] = (l$inn as String);
    return Input$SearchPatientByINNInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String get inn => (_$data['inn'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$inn = inn;
    result$data['inn'] = l$inn;
    return result$data;
  }

  CopyWith$Input$SearchPatientByINNInput<Input$SearchPatientByINNInput>
      get copyWith => CopyWith$Input$SearchPatientByINNInput(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$SearchPatientByINNInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$inn = inn;
    final lOther$inn = other.inn;
    if (l$inn != lOther$inn) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$inn = inn;
    return Object.hashAll([l$inn]);
  }
}

abstract class CopyWith$Input$SearchPatientByINNInput<TRes> {
  factory CopyWith$Input$SearchPatientByINNInput(
    Input$SearchPatientByINNInput instance,
    TRes Function(Input$SearchPatientByINNInput) then,
  ) = _CopyWithImpl$Input$SearchPatientByINNInput;

  factory CopyWith$Input$SearchPatientByINNInput.stub(TRes res) =
      _CopyWithStubImpl$Input$SearchPatientByINNInput;

  TRes call({String? inn});
}

class _CopyWithImpl$Input$SearchPatientByINNInput<TRes>
    implements CopyWith$Input$SearchPatientByINNInput<TRes> {
  _CopyWithImpl$Input$SearchPatientByINNInput(
    this._instance,
    this._then,
  );

  final Input$SearchPatientByINNInput _instance;

  final TRes Function(Input$SearchPatientByINNInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? inn = _undefined}) =>
      _then(Input$SearchPatientByINNInput._({
        ..._instance._$data,
        if (inn != _undefined && inn != null) 'inn': (inn as String),
      }));
}

class _CopyWithStubImpl$Input$SearchPatientByINNInput<TRes>
    implements CopyWith$Input$SearchPatientByINNInput<TRes> {
  _CopyWithStubImpl$Input$SearchPatientByINNInput(this._res);

  TRes _res;

  call({String? inn}) => _res;
}

const possibleTypesMap = <String, Set<String>>{};
