import 'dart:async';
import 'package:gql/ast.dart';
import 'package:graphql/client.dart' as graphql;

class Fragment$SearchPatientByINNOutputFragment {
  Fragment$SearchPatientByINNOutputFragment({
    this.id,
    this.oms,
    this.organisationId,
    this.organisationNameCode,
    this.parentOrganisationPhone,
    this.parentOrganisationNameCode,
    this.organisationPhone,
    this.pin,
    this.openSearchOrganisationAndApplicationUserRequest,
    this.$__typename = 'SearchPatientByINNOutput',
  });

  factory Fragment$SearchPatientByINNOutputFragment.fromJson(
      Map<String, dynamic> json) {
    final l$id = json['id'];
    final l$oms = json['oms'];
    final l$organisationId = json['organisationId'];
    final l$organisationNameCode = json['organisationNameCode'];
    final l$parentOrganisationPhone = json['parentOrganisationPhone'];
    final l$parentOrganisationNameCode = json['parentOrganisationNameCode'];
    final l$organisationPhone = json['organisationPhone'];
    final l$pin = json['pin'];
    final l$openSearchOrganisationAndApplicationUserRequest =
        json['openSearchOrganisationAndApplicationUserRequest'];
    final l$$__typename = json['__typename'];
    return Fragment$SearchPatientByINNOutputFragment(
      id: (l$id as int?),
      oms: (l$oms as String?),
      organisationId: (l$organisationId as int?),
      organisationNameCode: (l$organisationNameCode as String?),
      parentOrganisationPhone: (l$parentOrganisationPhone as String?),
      parentOrganisationNameCode: (l$parentOrganisationNameCode as String?),
      organisationPhone: (l$organisationPhone as String?),
      pin: (l$pin as String?),
      openSearchOrganisationAndApplicationUserRequest:
          (l$openSearchOrganisationAndApplicationUserRequest as List<dynamic>?)
              ?.map((e) => e == null
                  ? null
                  : Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest
                      .fromJson((e as Map<String, dynamic>)))
              .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final int? id;

  final String? oms;

  final int? organisationId;

  final String? organisationNameCode;

  final String? parentOrganisationPhone;

  final String? parentOrganisationNameCode;

  final String? organisationPhone;

  final String? pin;

  final List<
          Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>?
      openSearchOrganisationAndApplicationUserRequest;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$id = id;
    _resultData['id'] = l$id;
    final l$oms = oms;
    _resultData['oms'] = l$oms;
    final l$organisationId = organisationId;
    _resultData['organisationId'] = l$organisationId;
    final l$organisationNameCode = organisationNameCode;
    _resultData['organisationNameCode'] = l$organisationNameCode;
    final l$parentOrganisationPhone = parentOrganisationPhone;
    _resultData['parentOrganisationPhone'] = l$parentOrganisationPhone;
    final l$parentOrganisationNameCode = parentOrganisationNameCode;
    _resultData['parentOrganisationNameCode'] = l$parentOrganisationNameCode;
    final l$organisationPhone = organisationPhone;
    _resultData['organisationPhone'] = l$organisationPhone;
    final l$pin = pin;
    _resultData['pin'] = l$pin;
    final l$openSearchOrganisationAndApplicationUserRequest =
        openSearchOrganisationAndApplicationUserRequest;
    _resultData['openSearchOrganisationAndApplicationUserRequest'] =
        l$openSearchOrganisationAndApplicationUserRequest
            ?.map((e) => e?.toJson())
            .toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$id = id;
    final l$oms = oms;
    final l$organisationId = organisationId;
    final l$organisationNameCode = organisationNameCode;
    final l$parentOrganisationPhone = parentOrganisationPhone;
    final l$parentOrganisationNameCode = parentOrganisationNameCode;
    final l$organisationPhone = organisationPhone;
    final l$pin = pin;
    final l$openSearchOrganisationAndApplicationUserRequest =
        openSearchOrganisationAndApplicationUserRequest;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$id,
      l$oms,
      l$organisationId,
      l$organisationNameCode,
      l$parentOrganisationPhone,
      l$parentOrganisationNameCode,
      l$organisationPhone,
      l$pin,
      l$openSearchOrganisationAndApplicationUserRequest == null
          ? null
          : Object.hashAll(
              l$openSearchOrganisationAndApplicationUserRequest.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$SearchPatientByINNOutputFragment) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$id = id;
    final lOther$id = other.id;
    if (l$id != lOther$id) {
      return false;
    }
    final l$oms = oms;
    final lOther$oms = other.oms;
    if (l$oms != lOther$oms) {
      return false;
    }
    final l$organisationId = organisationId;
    final lOther$organisationId = other.organisationId;
    if (l$organisationId != lOther$organisationId) {
      return false;
    }
    final l$organisationNameCode = organisationNameCode;
    final lOther$organisationNameCode = other.organisationNameCode;
    if (l$organisationNameCode != lOther$organisationNameCode) {
      return false;
    }
    final l$parentOrganisationPhone = parentOrganisationPhone;
    final lOther$parentOrganisationPhone = other.parentOrganisationPhone;
    if (l$parentOrganisationPhone != lOther$parentOrganisationPhone) {
      return false;
    }
    final l$parentOrganisationNameCode = parentOrganisationNameCode;
    final lOther$parentOrganisationNameCode = other.parentOrganisationNameCode;
    if (l$parentOrganisationNameCode != lOther$parentOrganisationNameCode) {
      return false;
    }
    final l$organisationPhone = organisationPhone;
    final lOther$organisationPhone = other.organisationPhone;
    if (l$organisationPhone != lOther$organisationPhone) {
      return false;
    }
    final l$pin = pin;
    final lOther$pin = other.pin;
    if (l$pin != lOther$pin) {
      return false;
    }
    final l$openSearchOrganisationAndApplicationUserRequest =
        openSearchOrganisationAndApplicationUserRequest;
    final lOther$openSearchOrganisationAndApplicationUserRequest =
        other.openSearchOrganisationAndApplicationUserRequest;
    if (l$openSearchOrganisationAndApplicationUserRequest != null &&
        lOther$openSearchOrganisationAndApplicationUserRequest != null) {
      if (l$openSearchOrganisationAndApplicationUserRequest.length !=
          lOther$openSearchOrganisationAndApplicationUserRequest.length) {
        return false;
      }
      for (int i = 0;
          i < l$openSearchOrganisationAndApplicationUserRequest.length;
          i++) {
        final l$openSearchOrganisationAndApplicationUserRequest$entry =
            l$openSearchOrganisationAndApplicationUserRequest[i];
        final lOther$openSearchOrganisationAndApplicationUserRequest$entry =
            lOther$openSearchOrganisationAndApplicationUserRequest[i];
        if (l$openSearchOrganisationAndApplicationUserRequest$entry !=
            lOther$openSearchOrganisationAndApplicationUserRequest$entry) {
          return false;
        }
      }
    } else if (l$openSearchOrganisationAndApplicationUserRequest !=
        lOther$openSearchOrganisationAndApplicationUserRequest) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$SearchPatientByINNOutputFragment
    on Fragment$SearchPatientByINNOutputFragment {
  CopyWith$Fragment$SearchPatientByINNOutputFragment<
          Fragment$SearchPatientByINNOutputFragment>
      get copyWith => CopyWith$Fragment$SearchPatientByINNOutputFragment(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes> {
  factory CopyWith$Fragment$SearchPatientByINNOutputFragment(
    Fragment$SearchPatientByINNOutputFragment instance,
    TRes Function(Fragment$SearchPatientByINNOutputFragment) then,
  ) = _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment;

  factory CopyWith$Fragment$SearchPatientByINNOutputFragment.stub(TRes res) =
      _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment;

  TRes call({
    int? id,
    String? oms,
    int? organisationId,
    String? organisationNameCode,
    String? parentOrganisationPhone,
    String? parentOrganisationNameCode,
    String? organisationPhone,
    String? pin,
    List<Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>?
        openSearchOrganisationAndApplicationUserRequest,
    String? $__typename,
  });
  TRes openSearchOrganisationAndApplicationUserRequest(
      Iterable<Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>? Function(
              Iterable<
                  CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
                      Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest>?>?)
          _fn);
}

class _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment<TRes>
    implements CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes> {
  _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment(
    this._instance,
    this._then,
  );

  final Fragment$SearchPatientByINNOutputFragment _instance;

  final TRes Function(Fragment$SearchPatientByINNOutputFragment) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? id = _undefined,
    Object? oms = _undefined,
    Object? organisationId = _undefined,
    Object? organisationNameCode = _undefined,
    Object? parentOrganisationPhone = _undefined,
    Object? parentOrganisationNameCode = _undefined,
    Object? organisationPhone = _undefined,
    Object? pin = _undefined,
    Object? openSearchOrganisationAndApplicationUserRequest = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$SearchPatientByINNOutputFragment(
        id: id == _undefined ? _instance.id : (id as int?),
        oms: oms == _undefined ? _instance.oms : (oms as String?),
        organisationId: organisationId == _undefined
            ? _instance.organisationId
            : (organisationId as int?),
        organisationNameCode: organisationNameCode == _undefined
            ? _instance.organisationNameCode
            : (organisationNameCode as String?),
        parentOrganisationPhone: parentOrganisationPhone == _undefined
            ? _instance.parentOrganisationPhone
            : (parentOrganisationPhone as String?),
        parentOrganisationNameCode: parentOrganisationNameCode == _undefined
            ? _instance.parentOrganisationNameCode
            : (parentOrganisationNameCode as String?),
        organisationPhone: organisationPhone == _undefined
            ? _instance.organisationPhone
            : (organisationPhone as String?),
        pin: pin == _undefined ? _instance.pin : (pin as String?),
        openSearchOrganisationAndApplicationUserRequest:
            openSearchOrganisationAndApplicationUserRequest == _undefined
                ? _instance.openSearchOrganisationAndApplicationUserRequest
                : (openSearchOrganisationAndApplicationUserRequest as List<
                    Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  TRes openSearchOrganisationAndApplicationUserRequest(
          Iterable<Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>? Function(
                  Iterable<
                      CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
                          Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest>?>?)
              _fn) =>
      call(
          openSearchOrganisationAndApplicationUserRequest: _fn(_instance
              .openSearchOrganisationAndApplicationUserRequest
              ?.map((e) => e == null
                  ? null
                  : CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
                      e,
                      (i) => i,
                    )))?.toList());
}

class _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment<TRes>
    implements CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes> {
  _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment(this._res);

  TRes _res;

  call({
    int? id,
    String? oms,
    int? organisationId,
    String? organisationNameCode,
    String? parentOrganisationPhone,
    String? parentOrganisationNameCode,
    String? organisationPhone,
    String? pin,
    List<Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest?>?
        openSearchOrganisationAndApplicationUserRequest,
    String? $__typename,
  }) =>
      _res;
  openSearchOrganisationAndApplicationUserRequest(_fn) => _res;
}

const fragmentDefinitionSearchPatientByINNOutputFragment =
    FragmentDefinitionNode(
  name: NameNode(value: 'SearchPatientByINNOutputFragment'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'SearchPatientByINNOutput'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'id'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'oms'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'organisationId'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'organisationNameCode'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'parentOrganisationPhone'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'parentOrganisationNameCode'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'organisationPhone'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'pin'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'openSearchOrganisationAndApplicationUserRequest'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
          name: NameNode(value: 'dictDoljnostName'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: 'applicationUserFIO'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: 'organisationNameCode'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
        FieldNode(
          name: NameNode(value: '__typename'),
          alias: null,
          arguments: [],
          directives: [],
          selectionSet: null,
        ),
      ]),
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentSearchPatientByINNOutputFragment =
    DocumentNode(definitions: [
  fragmentDefinitionSearchPatientByINNOutputFragment,
]);

extension ClientExtension$Fragment$SearchPatientByINNOutputFragment
    on graphql.GraphQLClient {
  void writeFragment$SearchPatientByINNOutputFragment({
    required Fragment$SearchPatientByINNOutputFragment data,
    required Map<String, dynamic> idFields,
    bool broadcast = true,
  }) =>
      this.writeFragment(
        graphql.FragmentRequest(
          idFields: idFields,
          fragment: const graphql.Fragment(
            fragmentName: 'SearchPatientByINNOutputFragment',
            document: documentNodeFragmentSearchPatientByINNOutputFragment,
          ),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Fragment$SearchPatientByINNOutputFragment?
      readFragment$SearchPatientByINNOutputFragment({
    required Map<String, dynamic> idFields,
    bool optimistic = true,
  }) {
    final result = this.readFragment(
      graphql.FragmentRequest(
        idFields: idFields,
        fragment: const graphql.Fragment(
          fragmentName: 'SearchPatientByINNOutputFragment',
          document: documentNodeFragmentSearchPatientByINNOutputFragment,
        ),
      ),
      optimistic: optimistic,
    );
    return result == null
        ? null
        : Fragment$SearchPatientByINNOutputFragment.fromJson(result);
  }
}

class Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest {
  Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest({
    this.dictDoljnostName,
    this.applicationUserFIO,
    this.organisationNameCode,
    this.$__typename = 'OpenSearchOrganisationAndApplicationUserRequest',
  });

  factory Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest.fromJson(
      Map<String, dynamic> json) {
    final l$dictDoljnostName = json['dictDoljnostName'];
    final l$applicationUserFIO = json['applicationUserFIO'];
    final l$organisationNameCode = json['organisationNameCode'];
    final l$$__typename = json['__typename'];
    return Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
      dictDoljnostName: (l$dictDoljnostName as String?),
      applicationUserFIO: (l$applicationUserFIO as String?),
      organisationNameCode: (l$organisationNameCode as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? dictDoljnostName;

  final String? applicationUserFIO;

  final String? organisationNameCode;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$dictDoljnostName = dictDoljnostName;
    _resultData['dictDoljnostName'] = l$dictDoljnostName;
    final l$applicationUserFIO = applicationUserFIO;
    _resultData['applicationUserFIO'] = l$applicationUserFIO;
    final l$organisationNameCode = organisationNameCode;
    _resultData['organisationNameCode'] = l$organisationNameCode;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$dictDoljnostName = dictDoljnostName;
    final l$applicationUserFIO = applicationUserFIO;
    final l$organisationNameCode = organisationNameCode;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$dictDoljnostName,
      l$applicationUserFIO,
      l$organisationNameCode,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other
            is Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$dictDoljnostName = dictDoljnostName;
    final lOther$dictDoljnostName = other.dictDoljnostName;
    if (l$dictDoljnostName != lOther$dictDoljnostName) {
      return false;
    }
    final l$applicationUserFIO = applicationUserFIO;
    final lOther$applicationUserFIO = other.applicationUserFIO;
    if (l$applicationUserFIO != lOther$applicationUserFIO) {
      return false;
    }
    final l$organisationNameCode = organisationNameCode;
    final lOther$organisationNameCode = other.organisationNameCode;
    if (l$organisationNameCode != lOther$organisationNameCode) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest
    on Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest {
  CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
          Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest>
      get copyWith =>
          CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
    TRes> {
  factory CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
    Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest
        instance,
    TRes Function(
            Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest)
        then,
  ) = _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest;

  factory CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest.stub(
          TRes res) =
      _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest;

  TRes call({
    String? dictDoljnostName,
    String? applicationUserFIO,
    String? organisationNameCode,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
        TRes>
    implements
        CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
            TRes> {
  _CopyWithImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
    this._instance,
    this._then,
  );

  final Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest
      _instance;

  final TRes Function(
          Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? dictDoljnostName = _undefined,
    Object? applicationUserFIO = _undefined,
    Object? organisationNameCode = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(
          Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
        dictDoljnostName: dictDoljnostName == _undefined
            ? _instance.dictDoljnostName
            : (dictDoljnostName as String?),
        applicationUserFIO: applicationUserFIO == _undefined
            ? _instance.applicationUserFIO
            : (applicationUserFIO as String?),
        organisationNameCode: organisationNameCode == _undefined
            ? _instance.organisationNameCode
            : (organisationNameCode as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
        TRes>
    implements
        CopyWith$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest<
            TRes> {
  _CopyWithStubImpl$Fragment$SearchPatientByINNOutputFragment$openSearchOrganisationAndApplicationUserRequest(
      this._res);

  TRes _res;

  call({
    String? dictDoljnostName,
    String? applicationUserFIO,
    String? organisationNameCode,
    String? $__typename,
  }) =>
      _res;
}

class Fragment$DoctorResult {
  Fragment$DoctorResult({
    this.applicationUserId,
    this.dictDoljnostId,
    this.dictDoljnostName,
    this.etaj,
    this.id,
    this.numberCabinet,
    this.organisationId,
    this.organisationName,
    this.typeDoctor,
    this.applicationUserFIO,
    this.$__typename = 'Result',
  });

  factory Fragment$DoctorResult.fromJson(Map<String, dynamic> json) {
    final l$applicationUserId = json['applicationUserId'];
    final l$dictDoljnostId = json['dictDoljnostId'];
    final l$dictDoljnostName = json['dictDoljnostName'];
    final l$etaj = json['etaj'];
    final l$id = json['id'];
    final l$numberCabinet = json['numberCabinet'];
    final l$organisationId = json['organisationId'];
    final l$organisationName = json['organisationName'];
    final l$typeDoctor = json['typeDoctor'];
    final l$applicationUserFIO = json['applicationUserFIO'];
    final l$$__typename = json['__typename'];
    return Fragment$DoctorResult(
      applicationUserId: (l$applicationUserId as String?),
      dictDoljnostId: (l$dictDoljnostId as int?),
      dictDoljnostName: (l$dictDoljnostName as String?),
      etaj: (l$etaj as String?),
      id: (l$id as int?),
      numberCabinet: (l$numberCabinet as String?),
      organisationId: (l$organisationId as int?),
      organisationName: (l$organisationName as String?),
      typeDoctor: (l$typeDoctor as int?),
      applicationUserFIO: (l$applicationUserFIO as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? applicationUserId;

  final int? dictDoljnostId;

  final String? dictDoljnostName;

  final String? etaj;

  final int? id;

  final String? numberCabinet;

  final int? organisationId;

  final String? organisationName;

  final int? typeDoctor;

  final String? applicationUserFIO;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$applicationUserId = applicationUserId;
    _resultData['applicationUserId'] = l$applicationUserId;
    final l$dictDoljnostId = dictDoljnostId;
    _resultData['dictDoljnostId'] = l$dictDoljnostId;
    final l$dictDoljnostName = dictDoljnostName;
    _resultData['dictDoljnostName'] = l$dictDoljnostName;
    final l$etaj = etaj;
    _resultData['etaj'] = l$etaj;
    final l$id = id;
    _resultData['id'] = l$id;
    final l$numberCabinet = numberCabinet;
    _resultData['numberCabinet'] = l$numberCabinet;
    final l$organisationId = organisationId;
    _resultData['organisationId'] = l$organisationId;
    final l$organisationName = organisationName;
    _resultData['organisationName'] = l$organisationName;
    final l$typeDoctor = typeDoctor;
    _resultData['typeDoctor'] = l$typeDoctor;
    final l$applicationUserFIO = applicationUserFIO;
    _resultData['applicationUserFIO'] = l$applicationUserFIO;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$applicationUserId = applicationUserId;
    final l$dictDoljnostId = dictDoljnostId;
    final l$dictDoljnostName = dictDoljnostName;
    final l$etaj = etaj;
    final l$id = id;
    final l$numberCabinet = numberCabinet;
    final l$organisationId = organisationId;
    final l$organisationName = organisationName;
    final l$typeDoctor = typeDoctor;
    final l$applicationUserFIO = applicationUserFIO;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$applicationUserId,
      l$dictDoljnostId,
      l$dictDoljnostName,
      l$etaj,
      l$id,
      l$numberCabinet,
      l$organisationId,
      l$organisationName,
      l$typeDoctor,
      l$applicationUserFIO,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$DoctorResult) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$applicationUserId = applicationUserId;
    final lOther$applicationUserId = other.applicationUserId;
    if (l$applicationUserId != lOther$applicationUserId) {
      return false;
    }
    final l$dictDoljnostId = dictDoljnostId;
    final lOther$dictDoljnostId = other.dictDoljnostId;
    if (l$dictDoljnostId != lOther$dictDoljnostId) {
      return false;
    }
    final l$dictDoljnostName = dictDoljnostName;
    final lOther$dictDoljnostName = other.dictDoljnostName;
    if (l$dictDoljnostName != lOther$dictDoljnostName) {
      return false;
    }
    final l$etaj = etaj;
    final lOther$etaj = other.etaj;
    if (l$etaj != lOther$etaj) {
      return false;
    }
    final l$id = id;
    final lOther$id = other.id;
    if (l$id != lOther$id) {
      return false;
    }
    final l$numberCabinet = numberCabinet;
    final lOther$numberCabinet = other.numberCabinet;
    if (l$numberCabinet != lOther$numberCabinet) {
      return false;
    }
    final l$organisationId = organisationId;
    final lOther$organisationId = other.organisationId;
    if (l$organisationId != lOther$organisationId) {
      return false;
    }
    final l$organisationName = organisationName;
    final lOther$organisationName = other.organisationName;
    if (l$organisationName != lOther$organisationName) {
      return false;
    }
    final l$typeDoctor = typeDoctor;
    final lOther$typeDoctor = other.typeDoctor;
    if (l$typeDoctor != lOther$typeDoctor) {
      return false;
    }
    final l$applicationUserFIO = applicationUserFIO;
    final lOther$applicationUserFIO = other.applicationUserFIO;
    if (l$applicationUserFIO != lOther$applicationUserFIO) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$DoctorResult on Fragment$DoctorResult {
  CopyWith$Fragment$DoctorResult<Fragment$DoctorResult> get copyWith =>
      CopyWith$Fragment$DoctorResult(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$DoctorResult<TRes> {
  factory CopyWith$Fragment$DoctorResult(
    Fragment$DoctorResult instance,
    TRes Function(Fragment$DoctorResult) then,
  ) = _CopyWithImpl$Fragment$DoctorResult;

  factory CopyWith$Fragment$DoctorResult.stub(TRes res) =
      _CopyWithStubImpl$Fragment$DoctorResult;

  TRes call({
    String? applicationUserId,
    int? dictDoljnostId,
    String? dictDoljnostName,
    String? etaj,
    int? id,
    String? numberCabinet,
    int? organisationId,
    String? organisationName,
    int? typeDoctor,
    String? applicationUserFIO,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$DoctorResult<TRes>
    implements CopyWith$Fragment$DoctorResult<TRes> {
  _CopyWithImpl$Fragment$DoctorResult(
    this._instance,
    this._then,
  );

  final Fragment$DoctorResult _instance;

  final TRes Function(Fragment$DoctorResult) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? applicationUserId = _undefined,
    Object? dictDoljnostId = _undefined,
    Object? dictDoljnostName = _undefined,
    Object? etaj = _undefined,
    Object? id = _undefined,
    Object? numberCabinet = _undefined,
    Object? organisationId = _undefined,
    Object? organisationName = _undefined,
    Object? typeDoctor = _undefined,
    Object? applicationUserFIO = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$DoctorResult(
        applicationUserId: applicationUserId == _undefined
            ? _instance.applicationUserId
            : (applicationUserId as String?),
        dictDoljnostId: dictDoljnostId == _undefined
            ? _instance.dictDoljnostId
            : (dictDoljnostId as int?),
        dictDoljnostName: dictDoljnostName == _undefined
            ? _instance.dictDoljnostName
            : (dictDoljnostName as String?),
        etaj: etaj == _undefined ? _instance.etaj : (etaj as String?),
        id: id == _undefined ? _instance.id : (id as int?),
        numberCabinet: numberCabinet == _undefined
            ? _instance.numberCabinet
            : (numberCabinet as String?),
        organisationId: organisationId == _undefined
            ? _instance.organisationId
            : (organisationId as int?),
        organisationName: organisationName == _undefined
            ? _instance.organisationName
            : (organisationName as String?),
        typeDoctor: typeDoctor == _undefined
            ? _instance.typeDoctor
            : (typeDoctor as int?),
        applicationUserFIO: applicationUserFIO == _undefined
            ? _instance.applicationUserFIO
            : (applicationUserFIO as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$DoctorResult<TRes>
    implements CopyWith$Fragment$DoctorResult<TRes> {
  _CopyWithStubImpl$Fragment$DoctorResult(this._res);

  TRes _res;

  call({
    String? applicationUserId,
    int? dictDoljnostId,
    String? dictDoljnostName,
    String? etaj,
    int? id,
    String? numberCabinet,
    int? organisationId,
    String? organisationName,
    int? typeDoctor,
    String? applicationUserFIO,
    String? $__typename,
  }) =>
      _res;
}

const fragmentDefinitionDoctorResult = FragmentDefinitionNode(
  name: NameNode(value: 'DoctorResult'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'Result'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'applicationUserId'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'dictDoljnostId'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'dictDoljnostName'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'etaj'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'id'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'numberCabinet'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'organisationId'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'organisationName'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'typeDoctor'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'applicationUserFIO'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentDoctorResult = DocumentNode(definitions: [
  fragmentDefinitionDoctorResult,
]);

extension ClientExtension$Fragment$DoctorResult on graphql.GraphQLClient {
  void writeFragment$DoctorResult({
    required Fragment$DoctorResult data,
    required Map<String, dynamic> idFields,
    bool broadcast = true,
  }) =>
      this.writeFragment(
        graphql.FragmentRequest(
          idFields: idFields,
          fragment: const graphql.Fragment(
            fragmentName: 'DoctorResult',
            document: documentNodeFragmentDoctorResult,
          ),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Fragment$DoctorResult? readFragment$DoctorResult({
    required Map<String, dynamic> idFields,
    bool optimistic = true,
  }) {
    final result = this.readFragment(
      graphql.FragmentRequest(
        idFields: idFields,
        fragment: const graphql.Fragment(
          fragmentName: 'DoctorResult',
          document: documentNodeFragmentDoctorResult,
        ),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Fragment$DoctorResult.fromJson(result);
  }
}

class Variables$Query$searchPatientByINN {
  factory Variables$Query$searchPatientByINN({required String pin}) =>
      Variables$Query$searchPatientByINN._({
        r'pin': pin,
      });

  Variables$Query$searchPatientByINN._(this._$data);

  factory Variables$Query$searchPatientByINN.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$pin = data['pin'];
    result$data['pin'] = (l$pin as String);
    return Variables$Query$searchPatientByINN._(result$data);
  }

  Map<String, dynamic> _$data;

  String get pin => (_$data['pin'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$pin = pin;
    result$data['pin'] = l$pin;
    return result$data;
  }

  CopyWith$Variables$Query$searchPatientByINN<
          Variables$Query$searchPatientByINN>
      get copyWith => CopyWith$Variables$Query$searchPatientByINN(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$searchPatientByINN) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$pin = pin;
    final lOther$pin = other.pin;
    if (l$pin != lOther$pin) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$pin = pin;
    return Object.hashAll([l$pin]);
  }
}

abstract class CopyWith$Variables$Query$searchPatientByINN<TRes> {
  factory CopyWith$Variables$Query$searchPatientByINN(
    Variables$Query$searchPatientByINN instance,
    TRes Function(Variables$Query$searchPatientByINN) then,
  ) = _CopyWithImpl$Variables$Query$searchPatientByINN;

  factory CopyWith$Variables$Query$searchPatientByINN.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$searchPatientByINN;

  TRes call({String? pin});
}

class _CopyWithImpl$Variables$Query$searchPatientByINN<TRes>
    implements CopyWith$Variables$Query$searchPatientByINN<TRes> {
  _CopyWithImpl$Variables$Query$searchPatientByINN(
    this._instance,
    this._then,
  );

  final Variables$Query$searchPatientByINN _instance;

  final TRes Function(Variables$Query$searchPatientByINN) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? pin = _undefined}) =>
      _then(Variables$Query$searchPatientByINN._({
        ..._instance._$data,
        if (pin != _undefined && pin != null) 'pin': (pin as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$searchPatientByINN<TRes>
    implements CopyWith$Variables$Query$searchPatientByINN<TRes> {
  _CopyWithStubImpl$Variables$Query$searchPatientByINN(this._res);

  TRes _res;

  call({String? pin}) => _res;
}

class Query$searchPatientByINN {
  Query$searchPatientByINN({
    this.searchPatientByINN,
    this.$__typename = 'query_root',
  });

  factory Query$searchPatientByINN.fromJson(Map<String, dynamic> json) {
    final l$searchPatientByINN = json['searchPatientByINN'];
    final l$$__typename = json['__typename'];
    return Query$searchPatientByINN(
      searchPatientByINN: l$searchPatientByINN == null
          ? null
          : Fragment$SearchPatientByINNOutputFragment.fromJson(
              (l$searchPatientByINN as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$SearchPatientByINNOutputFragment? searchPatientByINN;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$searchPatientByINN = searchPatientByINN;
    _resultData['searchPatientByINN'] = l$searchPatientByINN?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$searchPatientByINN = searchPatientByINN;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$searchPatientByINN,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$searchPatientByINN) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$searchPatientByINN = searchPatientByINN;
    final lOther$searchPatientByINN = other.searchPatientByINN;
    if (l$searchPatientByINN != lOther$searchPatientByINN) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$searchPatientByINN
    on Query$searchPatientByINN {
  CopyWith$Query$searchPatientByINN<Query$searchPatientByINN> get copyWith =>
      CopyWith$Query$searchPatientByINN(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$searchPatientByINN<TRes> {
  factory CopyWith$Query$searchPatientByINN(
    Query$searchPatientByINN instance,
    TRes Function(Query$searchPatientByINN) then,
  ) = _CopyWithImpl$Query$searchPatientByINN;

  factory CopyWith$Query$searchPatientByINN.stub(TRes res) =
      _CopyWithStubImpl$Query$searchPatientByINN;

  TRes call({
    Fragment$SearchPatientByINNOutputFragment? searchPatientByINN,
    String? $__typename,
  });
  CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes>
      get searchPatientByINN;
}

class _CopyWithImpl$Query$searchPatientByINN<TRes>
    implements CopyWith$Query$searchPatientByINN<TRes> {
  _CopyWithImpl$Query$searchPatientByINN(
    this._instance,
    this._then,
  );

  final Query$searchPatientByINN _instance;

  final TRes Function(Query$searchPatientByINN) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? searchPatientByINN = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$searchPatientByINN(
        searchPatientByINN: searchPatientByINN == _undefined
            ? _instance.searchPatientByINN
            : (searchPatientByINN
                as Fragment$SearchPatientByINNOutputFragment?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes>
      get searchPatientByINN {
    final local$searchPatientByINN = _instance.searchPatientByINN;
    return local$searchPatientByINN == null
        ? CopyWith$Fragment$SearchPatientByINNOutputFragment.stub(
            _then(_instance))
        : CopyWith$Fragment$SearchPatientByINNOutputFragment(
            local$searchPatientByINN, (e) => call(searchPatientByINN: e));
  }
}

class _CopyWithStubImpl$Query$searchPatientByINN<TRes>
    implements CopyWith$Query$searchPatientByINN<TRes> {
  _CopyWithStubImpl$Query$searchPatientByINN(this._res);

  TRes _res;

  call({
    Fragment$SearchPatientByINNOutputFragment? searchPatientByINN,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Fragment$SearchPatientByINNOutputFragment<TRes>
      get searchPatientByINN =>
          CopyWith$Fragment$SearchPatientByINNOutputFragment.stub(_res);
}

const documentNodeQuerysearchPatientByINN = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'searchPatientByINN'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'pin')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'searchPatientByINN'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'PatientInfo'),
            value: ObjectValueNode(fields: [
              ObjectFieldNode(
                name: NameNode(value: 'inn'),
                value: VariableNode(name: NameNode(value: 'pin')),
              )
            ]),
          )
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FragmentSpreadNode(
            name: NameNode(value: 'SearchPatientByINNOutputFragment'),
            directives: [],
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionSearchPatientByINNOutputFragment,
]);
Query$searchPatientByINN _parserFn$Query$searchPatientByINN(
        Map<String, dynamic> data) =>
    Query$searchPatientByINN.fromJson(data);
typedef OnQueryComplete$Query$searchPatientByINN = FutureOr<void> Function(
  Map<String, dynamic>?,
  Query$searchPatientByINN?,
);

class Options$Query$searchPatientByINN
    extends graphql.QueryOptions<Query$searchPatientByINN> {
  Options$Query$searchPatientByINN({
    String? operationName,
    required Variables$Query$searchPatientByINN variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$searchPatientByINN? typedOptimisticResult,
    Duration? pollInterval,
    graphql.Context? context,
    OnQueryComplete$Query$searchPatientByINN? onComplete,
    graphql.OnQueryError? onError,
  })  : onCompleteWithParsed = onComplete,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          pollInterval: pollInterval,
          context: context,
          onComplete: onComplete == null
              ? null
              : (data) => onComplete(
                    data,
                    data == null
                        ? null
                        : _parserFn$Query$searchPatientByINN(data),
                  ),
          onError: onError,
          document: documentNodeQuerysearchPatientByINN,
          parserFn: _parserFn$Query$searchPatientByINN,
        );

  final OnQueryComplete$Query$searchPatientByINN? onCompleteWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onComplete == null
            ? super.properties
            : super.properties.where((property) => property != onComplete),
        onCompleteWithParsed,
      ];
}

class WatchOptions$Query$searchPatientByINN
    extends graphql.WatchQueryOptions<Query$searchPatientByINN> {
  WatchOptions$Query$searchPatientByINN({
    String? operationName,
    required Variables$Query$searchPatientByINN variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$searchPatientByINN? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeQuerysearchPatientByINN,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Query$searchPatientByINN,
        );
}

class FetchMoreOptions$Query$searchPatientByINN
    extends graphql.FetchMoreOptions {
  FetchMoreOptions$Query$searchPatientByINN({
    required graphql.UpdateQuery updateQuery,
    required Variables$Query$searchPatientByINN variables,
  }) : super(
          updateQuery: updateQuery,
          variables: variables.toJson(),
          document: documentNodeQuerysearchPatientByINN,
        );
}

extension ClientExtension$Query$searchPatientByINN on graphql.GraphQLClient {
  Future<graphql.QueryResult<Query$searchPatientByINN>>
      query$searchPatientByINN(
              Options$Query$searchPatientByINN options) async =>
          await this.query(options);
  graphql.ObservableQuery<Query$searchPatientByINN>
      watchQuery$searchPatientByINN(
              WatchOptions$Query$searchPatientByINN options) =>
          this.watchQuery(options);
  void writeQuery$searchPatientByINN({
    required Query$searchPatientByINN data,
    required Variables$Query$searchPatientByINN variables,
    bool broadcast = true,
  }) =>
      this.writeQuery(
        graphql.Request(
          operation:
              graphql.Operation(document: documentNodeQuerysearchPatientByINN),
          variables: variables.toJson(),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Query$searchPatientByINN? readQuery$searchPatientByINN({
    required Variables$Query$searchPatientByINN variables,
    bool optimistic = true,
  }) {
    final result = this.readQuery(
      graphql.Request(
        operation:
            graphql.Operation(document: documentNodeQuerysearchPatientByINN),
        variables: variables.toJson(),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Query$searchPatientByINN.fromJson(result);
  }
}

class Variables$Query$getDoctorListByOrgId {
  factory Variables$Query$getDoctorListByOrgId(
          {required String organisationId}) =>
      Variables$Query$getDoctorListByOrgId._({
        r'organisationId': organisationId,
      });

  Variables$Query$getDoctorListByOrgId._(this._$data);

  factory Variables$Query$getDoctorListByOrgId.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$organisationId = data['organisationId'];
    result$data['organisationId'] = (l$organisationId as String);
    return Variables$Query$getDoctorListByOrgId._(result$data);
  }

  Map<String, dynamic> _$data;

  String get organisationId => (_$data['organisationId'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$organisationId = organisationId;
    result$data['organisationId'] = l$organisationId;
    return result$data;
  }

  CopyWith$Variables$Query$getDoctorListByOrgId<
          Variables$Query$getDoctorListByOrgId>
      get copyWith => CopyWith$Variables$Query$getDoctorListByOrgId(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$getDoctorListByOrgId) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$organisationId = organisationId;
    final lOther$organisationId = other.organisationId;
    if (l$organisationId != lOther$organisationId) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$organisationId = organisationId;
    return Object.hashAll([l$organisationId]);
  }
}

abstract class CopyWith$Variables$Query$getDoctorListByOrgId<TRes> {
  factory CopyWith$Variables$Query$getDoctorListByOrgId(
    Variables$Query$getDoctorListByOrgId instance,
    TRes Function(Variables$Query$getDoctorListByOrgId) then,
  ) = _CopyWithImpl$Variables$Query$getDoctorListByOrgId;

  factory CopyWith$Variables$Query$getDoctorListByOrgId.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$getDoctorListByOrgId;

  TRes call({String? organisationId});
}

class _CopyWithImpl$Variables$Query$getDoctorListByOrgId<TRes>
    implements CopyWith$Variables$Query$getDoctorListByOrgId<TRes> {
  _CopyWithImpl$Variables$Query$getDoctorListByOrgId(
    this._instance,
    this._then,
  );

  final Variables$Query$getDoctorListByOrgId _instance;

  final TRes Function(Variables$Query$getDoctorListByOrgId) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? organisationId = _undefined}) =>
      _then(Variables$Query$getDoctorListByOrgId._({
        ..._instance._$data,
        if (organisationId != _undefined && organisationId != null)
          'organisationId': (organisationId as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$getDoctorListByOrgId<TRes>
    implements CopyWith$Variables$Query$getDoctorListByOrgId<TRes> {
  _CopyWithStubImpl$Variables$Query$getDoctorListByOrgId(this._res);

  TRes _res;

  call({String? organisationId}) => _res;
}

class Query$getDoctorListByOrgId {
  Query$getDoctorListByOrgId({
    this.getDoctorListByOrgId,
    this.$__typename = 'query_root',
  });

  factory Query$getDoctorListByOrgId.fromJson(Map<String, dynamic> json) {
    final l$getDoctorListByOrgId = json['getDoctorListByOrgId'];
    final l$$__typename = json['__typename'];
    return Query$getDoctorListByOrgId(
      getDoctorListByOrgId: l$getDoctorListByOrgId == null
          ? null
          : Query$getDoctorListByOrgId$getDoctorListByOrgId.fromJson(
              (l$getDoctorListByOrgId as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Query$getDoctorListByOrgId$getDoctorListByOrgId? getDoctorListByOrgId;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$getDoctorListByOrgId = getDoctorListByOrgId;
    _resultData['getDoctorListByOrgId'] = l$getDoctorListByOrgId?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$getDoctorListByOrgId = getDoctorListByOrgId;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$getDoctorListByOrgId,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$getDoctorListByOrgId) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$getDoctorListByOrgId = getDoctorListByOrgId;
    final lOther$getDoctorListByOrgId = other.getDoctorListByOrgId;
    if (l$getDoctorListByOrgId != lOther$getDoctorListByOrgId) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$getDoctorListByOrgId
    on Query$getDoctorListByOrgId {
  CopyWith$Query$getDoctorListByOrgId<Query$getDoctorListByOrgId>
      get copyWith => CopyWith$Query$getDoctorListByOrgId(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$getDoctorListByOrgId<TRes> {
  factory CopyWith$Query$getDoctorListByOrgId(
    Query$getDoctorListByOrgId instance,
    TRes Function(Query$getDoctorListByOrgId) then,
  ) = _CopyWithImpl$Query$getDoctorListByOrgId;

  factory CopyWith$Query$getDoctorListByOrgId.stub(TRes res) =
      _CopyWithStubImpl$Query$getDoctorListByOrgId;

  TRes call({
    Query$getDoctorListByOrgId$getDoctorListByOrgId? getDoctorListByOrgId,
    String? $__typename,
  });
  CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes>
      get getDoctorListByOrgId;
}

class _CopyWithImpl$Query$getDoctorListByOrgId<TRes>
    implements CopyWith$Query$getDoctorListByOrgId<TRes> {
  _CopyWithImpl$Query$getDoctorListByOrgId(
    this._instance,
    this._then,
  );

  final Query$getDoctorListByOrgId _instance;

  final TRes Function(Query$getDoctorListByOrgId) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? getDoctorListByOrgId = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$getDoctorListByOrgId(
        getDoctorListByOrgId: getDoctorListByOrgId == _undefined
            ? _instance.getDoctorListByOrgId
            : (getDoctorListByOrgId
                as Query$getDoctorListByOrgId$getDoctorListByOrgId?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes>
      get getDoctorListByOrgId {
    final local$getDoctorListByOrgId = _instance.getDoctorListByOrgId;
    return local$getDoctorListByOrgId == null
        ? CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId.stub(
            _then(_instance))
        : CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId(
            local$getDoctorListByOrgId, (e) => call(getDoctorListByOrgId: e));
  }
}

class _CopyWithStubImpl$Query$getDoctorListByOrgId<TRes>
    implements CopyWith$Query$getDoctorListByOrgId<TRes> {
  _CopyWithStubImpl$Query$getDoctorListByOrgId(this._res);

  TRes _res;

  call({
    Query$getDoctorListByOrgId$getDoctorListByOrgId? getDoctorListByOrgId,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes>
      get getDoctorListByOrgId =>
          CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId.stub(_res);
}

const documentNodeQuerygetDoctorListByOrgId = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'getDoctorListByOrgId'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'organisationId')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'getDoctorListByOrgId'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'OrgInfo'),
            value: ObjectValueNode(fields: [
              ObjectFieldNode(
                name: NameNode(value: 'organisationId'),
                value: VariableNode(name: NameNode(value: 'organisationId')),
              )
            ]),
          )
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'currentPage'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'pageSize'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'result'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                name: NameNode(value: 'DoctorResult'),
                directives: [],
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: 'totalItems'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'totalPages'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionDoctorResult,
]);
Query$getDoctorListByOrgId _parserFn$Query$getDoctorListByOrgId(
        Map<String, dynamic> data) =>
    Query$getDoctorListByOrgId.fromJson(data);
typedef OnQueryComplete$Query$getDoctorListByOrgId = FutureOr<void> Function(
  Map<String, dynamic>?,
  Query$getDoctorListByOrgId?,
);

class Options$Query$getDoctorListByOrgId
    extends graphql.QueryOptions<Query$getDoctorListByOrgId> {
  Options$Query$getDoctorListByOrgId({
    String? operationName,
    required Variables$Query$getDoctorListByOrgId variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$getDoctorListByOrgId? typedOptimisticResult,
    Duration? pollInterval,
    graphql.Context? context,
    OnQueryComplete$Query$getDoctorListByOrgId? onComplete,
    graphql.OnQueryError? onError,
  })  : onCompleteWithParsed = onComplete,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          pollInterval: pollInterval,
          context: context,
          onComplete: onComplete == null
              ? null
              : (data) => onComplete(
                    data,
                    data == null
                        ? null
                        : _parserFn$Query$getDoctorListByOrgId(data),
                  ),
          onError: onError,
          document: documentNodeQuerygetDoctorListByOrgId,
          parserFn: _parserFn$Query$getDoctorListByOrgId,
        );

  final OnQueryComplete$Query$getDoctorListByOrgId? onCompleteWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onComplete == null
            ? super.properties
            : super.properties.where((property) => property != onComplete),
        onCompleteWithParsed,
      ];
}

class WatchOptions$Query$getDoctorListByOrgId
    extends graphql.WatchQueryOptions<Query$getDoctorListByOrgId> {
  WatchOptions$Query$getDoctorListByOrgId({
    String? operationName,
    required Variables$Query$getDoctorListByOrgId variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$getDoctorListByOrgId? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeQuerygetDoctorListByOrgId,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Query$getDoctorListByOrgId,
        );
}

class FetchMoreOptions$Query$getDoctorListByOrgId
    extends graphql.FetchMoreOptions {
  FetchMoreOptions$Query$getDoctorListByOrgId({
    required graphql.UpdateQuery updateQuery,
    required Variables$Query$getDoctorListByOrgId variables,
  }) : super(
          updateQuery: updateQuery,
          variables: variables.toJson(),
          document: documentNodeQuerygetDoctorListByOrgId,
        );
}

extension ClientExtension$Query$getDoctorListByOrgId on graphql.GraphQLClient {
  Future<graphql.QueryResult<Query$getDoctorListByOrgId>>
      query$getDoctorListByOrgId(
              Options$Query$getDoctorListByOrgId options) async =>
          await this.query(options);
  graphql.ObservableQuery<Query$getDoctorListByOrgId>
      watchQuery$getDoctorListByOrgId(
              WatchOptions$Query$getDoctorListByOrgId options) =>
          this.watchQuery(options);
  void writeQuery$getDoctorListByOrgId({
    required Query$getDoctorListByOrgId data,
    required Variables$Query$getDoctorListByOrgId variables,
    bool broadcast = true,
  }) =>
      this.writeQuery(
        graphql.Request(
          operation: graphql.Operation(
              document: documentNodeQuerygetDoctorListByOrgId),
          variables: variables.toJson(),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Query$getDoctorListByOrgId? readQuery$getDoctorListByOrgId({
    required Variables$Query$getDoctorListByOrgId variables,
    bool optimistic = true,
  }) {
    final result = this.readQuery(
      graphql.Request(
        operation:
            graphql.Operation(document: documentNodeQuerygetDoctorListByOrgId),
        variables: variables.toJson(),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Query$getDoctorListByOrgId.fromJson(result);
  }
}

class Query$getDoctorListByOrgId$getDoctorListByOrgId {
  Query$getDoctorListByOrgId$getDoctorListByOrgId({
    this.currentPage,
    this.pageSize,
    this.result,
    this.totalItems,
    this.totalPages,
    this.$__typename = 'GetDoctorListByOrgIdOutput',
  });

  factory Query$getDoctorListByOrgId$getDoctorListByOrgId.fromJson(
      Map<String, dynamic> json) {
    final l$currentPage = json['currentPage'];
    final l$pageSize = json['pageSize'];
    final l$result = json['result'];
    final l$totalItems = json['totalItems'];
    final l$totalPages = json['totalPages'];
    final l$$__typename = json['__typename'];
    return Query$getDoctorListByOrgId$getDoctorListByOrgId(
      currentPage: (l$currentPage as int?),
      pageSize: (l$pageSize as int?),
      result: (l$result as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Fragment$DoctorResult.fromJson((e as Map<String, dynamic>)))
          .toList(),
      totalItems: (l$totalItems as int?),
      totalPages: (l$totalPages as int?),
      $__typename: (l$$__typename as String),
    );
  }

  final int? currentPage;

  final int? pageSize;

  final List<Fragment$DoctorResult?>? result;

  final int? totalItems;

  final int? totalPages;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$currentPage = currentPage;
    _resultData['currentPage'] = l$currentPage;
    final l$pageSize = pageSize;
    _resultData['pageSize'] = l$pageSize;
    final l$result = result;
    _resultData['result'] = l$result?.map((e) => e?.toJson()).toList();
    final l$totalItems = totalItems;
    _resultData['totalItems'] = l$totalItems;
    final l$totalPages = totalPages;
    _resultData['totalPages'] = l$totalPages;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$currentPage = currentPage;
    final l$pageSize = pageSize;
    final l$result = result;
    final l$totalItems = totalItems;
    final l$totalPages = totalPages;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$currentPage,
      l$pageSize,
      l$result == null ? null : Object.hashAll(l$result.map((v) => v)),
      l$totalItems,
      l$totalPages,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$getDoctorListByOrgId$getDoctorListByOrgId) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$currentPage = currentPage;
    final lOther$currentPage = other.currentPage;
    if (l$currentPage != lOther$currentPage) {
      return false;
    }
    final l$pageSize = pageSize;
    final lOther$pageSize = other.pageSize;
    if (l$pageSize != lOther$pageSize) {
      return false;
    }
    final l$result = result;
    final lOther$result = other.result;
    if (l$result != null && lOther$result != null) {
      if (l$result.length != lOther$result.length) {
        return false;
      }
      for (int i = 0; i < l$result.length; i++) {
        final l$result$entry = l$result[i];
        final lOther$result$entry = lOther$result[i];
        if (l$result$entry != lOther$result$entry) {
          return false;
        }
      }
    } else if (l$result != lOther$result) {
      return false;
    }
    final l$totalItems = totalItems;
    final lOther$totalItems = other.totalItems;
    if (l$totalItems != lOther$totalItems) {
      return false;
    }
    final l$totalPages = totalPages;
    final lOther$totalPages = other.totalPages;
    if (l$totalPages != lOther$totalPages) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$getDoctorListByOrgId$getDoctorListByOrgId
    on Query$getDoctorListByOrgId$getDoctorListByOrgId {
  CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<
          Query$getDoctorListByOrgId$getDoctorListByOrgId>
      get copyWith => CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes> {
  factory CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId(
    Query$getDoctorListByOrgId$getDoctorListByOrgId instance,
    TRes Function(Query$getDoctorListByOrgId$getDoctorListByOrgId) then,
  ) = _CopyWithImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId;

  factory CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId.stub(
          TRes res) =
      _CopyWithStubImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId;

  TRes call({
    int? currentPage,
    int? pageSize,
    List<Fragment$DoctorResult?>? result,
    int? totalItems,
    int? totalPages,
    String? $__typename,
  });
  TRes result(
      Iterable<Fragment$DoctorResult?>? Function(
              Iterable<CopyWith$Fragment$DoctorResult<Fragment$DoctorResult>?>?)
          _fn);
}

class _CopyWithImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes>
    implements CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes> {
  _CopyWithImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId(
    this._instance,
    this._then,
  );

  final Query$getDoctorListByOrgId$getDoctorListByOrgId _instance;

  final TRes Function(Query$getDoctorListByOrgId$getDoctorListByOrgId) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? currentPage = _undefined,
    Object? pageSize = _undefined,
    Object? result = _undefined,
    Object? totalItems = _undefined,
    Object? totalPages = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$getDoctorListByOrgId$getDoctorListByOrgId(
        currentPage: currentPage == _undefined
            ? _instance.currentPage
            : (currentPage as int?),
        pageSize:
            pageSize == _undefined ? _instance.pageSize : (pageSize as int?),
        result: result == _undefined
            ? _instance.result
            : (result as List<Fragment$DoctorResult?>?),
        totalItems: totalItems == _undefined
            ? _instance.totalItems
            : (totalItems as int?),
        totalPages: totalPages == _undefined
            ? _instance.totalPages
            : (totalPages as int?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  TRes result(
          Iterable<Fragment$DoctorResult?>? Function(
                  Iterable<
                      CopyWith$Fragment$DoctorResult<Fragment$DoctorResult>?>?)
              _fn) =>
      call(
          result: _fn(_instance.result?.map((e) => e == null
              ? null
              : CopyWith$Fragment$DoctorResult(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes>
    implements CopyWith$Query$getDoctorListByOrgId$getDoctorListByOrgId<TRes> {
  _CopyWithStubImpl$Query$getDoctorListByOrgId$getDoctorListByOrgId(this._res);

  TRes _res;

  call({
    int? currentPage,
    int? pageSize,
    List<Fragment$DoctorResult?>? result,
    int? totalItems,
    int? totalPages,
    String? $__typename,
  }) =>
      _res;
  result(_fn) => _res;
}
