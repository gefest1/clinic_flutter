import 'package:clinic/utils/date_time_parse.dart';

class Input$PreUserCreateInput {
  factory Input$PreUserCreateInput({
    DateTime? dateBirth,
    String? firstName,
    String? id,
    String? lastName,
    String? patronymic,
    required String photoUrl,
    required String uniqueNumber,
  }) =>
      Input$PreUserCreateInput._({
        if (dateBirth != null) r'dateBirth': dateBirth,
        if (firstName != null) r'firstName': firstName,
        if (id != null) r'id': id,
        if (lastName != null) r'lastName': lastName,
        if (patronymic != null) r'patronymic': patronymic,
        r'photoUrl': photoUrl,
        r'uniqueNumber': uniqueNumber,
      });

  Input$PreUserCreateInput._(this._$data);

  factory Input$PreUserCreateInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('dateBirth')) {
      final l$dateBirth = data['dateBirth'];
      result$data['dateBirth'] =
          l$dateBirth == null ? null : dateTimeParseG(l$dateBirth);
    }
    if (data.containsKey('firstName')) {
      final l$firstName = data['firstName'];
      result$data['firstName'] = (l$firstName as String?);
    }
    if (data.containsKey('id')) {
      final l$id = data['id'];
      result$data['id'] = (l$id as String?);
    }
    if (data.containsKey('lastName')) {
      final l$lastName = data['lastName'];
      result$data['lastName'] = (l$lastName as String?);
    }
    if (data.containsKey('patronymic')) {
      final l$patronymic = data['patronymic'];
      result$data['patronymic'] = (l$patronymic as String?);
    }
    final l$photoUrl = data['photoUrl'];
    result$data['photoUrl'] = (l$photoUrl as String);
    final l$uniqueNumber = data['uniqueNumber'];
    result$data['uniqueNumber'] = (l$uniqueNumber as String);
    return Input$PreUserCreateInput._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime? get dateBirth => (_$data['dateBirth'] as DateTime?);
  String? get firstName => (_$data['firstName'] as String?);
  String? get id => (_$data['id'] as String?);
  String? get lastName => (_$data['lastName'] as String?);
  String? get patronymic => (_$data['patronymic'] as String?);
  String get photoUrl => (_$data['photoUrl'] as String);
  String get uniqueNumber => (_$data['uniqueNumber'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('dateBirth')) {
      final l$dateBirth = dateBirth;
      result$data['dateBirth'] =
          l$dateBirth == null ? null : toMapDateTimeParseG(l$dateBirth);
    }
    if (_$data.containsKey('firstName')) {
      final l$firstName = firstName;
      result$data['firstName'] = l$firstName;
    }
    if (_$data.containsKey('id')) {
      final l$id = id;
      result$data['id'] = l$id;
    }
    if (_$data.containsKey('lastName')) {
      final l$lastName = lastName;
      result$data['lastName'] = l$lastName;
    }
    if (_$data.containsKey('patronymic')) {
      final l$patronymic = patronymic;
      result$data['patronymic'] = l$patronymic;
    }
    final l$photoUrl = photoUrl;
    result$data['photoUrl'] = l$photoUrl;
    final l$uniqueNumber = uniqueNumber;
    result$data['uniqueNumber'] = l$uniqueNumber;
    return result$data;
  }

  CopyWith$Input$PreUserCreateInput<Input$PreUserCreateInput> get copyWith =>
      CopyWith$Input$PreUserCreateInput(
        this,
        (i) => i,
      );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$PreUserCreateInput) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$dateBirth = dateBirth;
    final lOther$dateBirth = other.dateBirth;
    if (_$data.containsKey('dateBirth') !=
        other._$data.containsKey('dateBirth')) {
      return false;
    }
    if (l$dateBirth != lOther$dateBirth) {
      return false;
    }
    final l$firstName = firstName;
    final lOther$firstName = other.firstName;
    if (_$data.containsKey('firstName') !=
        other._$data.containsKey('firstName')) {
      return false;
    }
    if (l$firstName != lOther$firstName) {
      return false;
    }
    final l$id = id;
    final lOther$id = other.id;
    if (_$data.containsKey('id') != other._$data.containsKey('id')) {
      return false;
    }
    if (l$id != lOther$id) {
      return false;
    }
    final l$lastName = lastName;
    final lOther$lastName = other.lastName;
    if (_$data.containsKey('lastName') !=
        other._$data.containsKey('lastName')) {
      return false;
    }
    if (l$lastName != lOther$lastName) {
      return false;
    }
    final l$patronymic = patronymic;
    final lOther$patronymic = other.patronymic;
    if (_$data.containsKey('patronymic') !=
        other._$data.containsKey('patronymic')) {
      return false;
    }
    if (l$patronymic != lOther$patronymic) {
      return false;
    }
    final l$photoUrl = photoUrl;
    final lOther$photoUrl = other.photoUrl;
    if (l$photoUrl != lOther$photoUrl) {
      return false;
    }
    final l$uniqueNumber = uniqueNumber;
    final lOther$uniqueNumber = other.uniqueNumber;
    if (l$uniqueNumber != lOther$uniqueNumber) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$dateBirth = dateBirth;
    final l$firstName = firstName;
    final l$id = id;
    final l$lastName = lastName;
    final l$patronymic = patronymic;
    final l$photoUrl = photoUrl;
    final l$uniqueNumber = uniqueNumber;
    return Object.hashAll([
      _$data.containsKey('dateBirth') ? l$dateBirth : const {},
      _$data.containsKey('firstName') ? l$firstName : const {},
      _$data.containsKey('id') ? l$id : const {},
      _$data.containsKey('lastName') ? l$lastName : const {},
      _$data.containsKey('patronymic') ? l$patronymic : const {},
      l$photoUrl,
      l$uniqueNumber,
    ]);
  }
}

abstract class CopyWith$Input$PreUserCreateInput<TRes> {
  factory CopyWith$Input$PreUserCreateInput(
    Input$PreUserCreateInput instance,
    TRes Function(Input$PreUserCreateInput) then,
  ) = _CopyWithImpl$Input$PreUserCreateInput;

  factory CopyWith$Input$PreUserCreateInput.stub(TRes res) =
      _CopyWithStubImpl$Input$PreUserCreateInput;

  TRes call({
    DateTime? dateBirth,
    String? firstName,
    String? id,
    String? lastName,
    String? patronymic,
    String? photoUrl,
    String? uniqueNumber,
  });
}

class _CopyWithImpl$Input$PreUserCreateInput<TRes>
    implements CopyWith$Input$PreUserCreateInput<TRes> {
  _CopyWithImpl$Input$PreUserCreateInput(
    this._instance,
    this._then,
  );

  final Input$PreUserCreateInput _instance;

  final TRes Function(Input$PreUserCreateInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? dateBirth = _undefined,
    Object? firstName = _undefined,
    Object? id = _undefined,
    Object? lastName = _undefined,
    Object? patronymic = _undefined,
    Object? photoUrl = _undefined,
    Object? uniqueNumber = _undefined,
  }) =>
      _then(Input$PreUserCreateInput._({
        ..._instance._$data,
        if (dateBirth != _undefined) 'dateBirth': (dateBirth as DateTime?),
        if (firstName != _undefined) 'firstName': (firstName as String?),
        if (id != _undefined) 'id': (id as String?),
        if (lastName != _undefined) 'lastName': (lastName as String?),
        if (patronymic != _undefined) 'patronymic': (patronymic as String?),
        if (photoUrl != _undefined && photoUrl != null)
          'photoUrl': (photoUrl as String),
        if (uniqueNumber != _undefined && uniqueNumber != null)
          'uniqueNumber': (uniqueNumber as String),
      }));
}

class _CopyWithStubImpl$Input$PreUserCreateInput<TRes>
    implements CopyWith$Input$PreUserCreateInput<TRes> {
  _CopyWithStubImpl$Input$PreUserCreateInput(this._res);

  TRes _res;

  call({
    DateTime? dateBirth,
    String? firstName,
    String? id,
    String? lastName,
    String? patronymic,
    String? photoUrl,
    String? uniqueNumber,
  }) =>
      _res;
}

class Input$UserCreateInput {
  factory Input$UserCreateInput({
    required DateTime dateBirth,
    required String firstName,
    String? hash,
    String? id,
    required String lastName,
    required String patronymic,
    required String uniqueNumber,
  }) =>
      Input$UserCreateInput._({
        r'dateBirth': dateBirth,
        r'firstName': firstName,
        if (hash != null) r'hash': hash,
        if (id != null) r'id': id,
        r'lastName': lastName,
        r'patronymic': patronymic,
        r'uniqueNumber': uniqueNumber,
      });

  Input$UserCreateInput._(this._$data);

  factory Input$UserCreateInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$dateBirth = data['dateBirth'];
    result$data['dateBirth'] = dateTimeParseG(l$dateBirth);
    final l$firstName = data['firstName'];
    result$data['firstName'] = (l$firstName as String);
    if (data.containsKey('hash')) {
      final l$hash = data['hash'];
      result$data['hash'] = (l$hash as String?);
    }
    if (data.containsKey('id')) {
      final l$id = data['id'];
      result$data['id'] = (l$id as String?);
    }
    final l$lastName = data['lastName'];
    result$data['lastName'] = (l$lastName as String);
    final l$patronymic = data['patronymic'];
    result$data['patronymic'] = (l$patronymic as String);
    final l$uniqueNumber = data['uniqueNumber'];
    result$data['uniqueNumber'] = (l$uniqueNumber as String);
    return Input$UserCreateInput._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime get dateBirth => (_$data['dateBirth'] as DateTime);
  String get firstName => (_$data['firstName'] as String);
  String? get hash => (_$data['hash'] as String?);
  String? get id => (_$data['id'] as String?);
  String get lastName => (_$data['lastName'] as String);
  String get patronymic => (_$data['patronymic'] as String);
  String get uniqueNumber => (_$data['uniqueNumber'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$dateBirth = dateBirth;
    result$data['dateBirth'] = toMapDateTimeParseG(l$dateBirth);
    final l$firstName = firstName;
    result$data['firstName'] = l$firstName;
    if (_$data.containsKey('hash')) {
      final l$hash = hash;
      result$data['hash'] = l$hash;
    }
    if (_$data.containsKey('id')) {
      final l$id = id;
      result$data['id'] = l$id;
    }
    final l$lastName = lastName;
    result$data['lastName'] = l$lastName;
    final l$patronymic = patronymic;
    result$data['patronymic'] = l$patronymic;
    final l$uniqueNumber = uniqueNumber;
    result$data['uniqueNumber'] = l$uniqueNumber;
    return result$data;
  }

  CopyWith$Input$UserCreateInput<Input$UserCreateInput> get copyWith =>
      CopyWith$Input$UserCreateInput(
        this,
        (i) => i,
      );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$UserCreateInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$dateBirth = dateBirth;
    final lOther$dateBirth = other.dateBirth;
    if (l$dateBirth != lOther$dateBirth) {
      return false;
    }
    final l$firstName = firstName;
    final lOther$firstName = other.firstName;
    if (l$firstName != lOther$firstName) {
      return false;
    }
    final l$hash = hash;
    final lOther$hash = other.hash;
    if (_$data.containsKey('hash') != other._$data.containsKey('hash')) {
      return false;
    }
    if (l$hash != lOther$hash) {
      return false;
    }
    final l$id = id;
    final lOther$id = other.id;
    if (_$data.containsKey('id') != other._$data.containsKey('id')) {
      return false;
    }
    if (l$id != lOther$id) {
      return false;
    }
    final l$lastName = lastName;
    final lOther$lastName = other.lastName;
    if (l$lastName != lOther$lastName) {
      return false;
    }
    final l$patronymic = patronymic;
    final lOther$patronymic = other.patronymic;
    if (l$patronymic != lOther$patronymic) {
      return false;
    }
    final l$uniqueNumber = uniqueNumber;
    final lOther$uniqueNumber = other.uniqueNumber;
    if (l$uniqueNumber != lOther$uniqueNumber) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$dateBirth = dateBirth;
    final l$firstName = firstName;
    final l$hash = hash;
    final l$id = id;
    final l$lastName = lastName;
    final l$patronymic = patronymic;
    final l$uniqueNumber = uniqueNumber;
    return Object.hashAll([
      l$dateBirth,
      l$firstName,
      _$data.containsKey('hash') ? l$hash : const {},
      _$data.containsKey('id') ? l$id : const {},
      l$lastName,
      l$patronymic,
      l$uniqueNumber,
    ]);
  }
}

abstract class CopyWith$Input$UserCreateInput<TRes> {
  factory CopyWith$Input$UserCreateInput(
    Input$UserCreateInput instance,
    TRes Function(Input$UserCreateInput) then,
  ) = _CopyWithImpl$Input$UserCreateInput;

  factory CopyWith$Input$UserCreateInput.stub(TRes res) =
      _CopyWithStubImpl$Input$UserCreateInput;

  TRes call({
    DateTime? dateBirth,
    String? firstName,
    String? hash,
    String? id,
    String? lastName,
    String? patronymic,
    String? uniqueNumber,
  });
}

class _CopyWithImpl$Input$UserCreateInput<TRes>
    implements CopyWith$Input$UserCreateInput<TRes> {
  _CopyWithImpl$Input$UserCreateInput(
    this._instance,
    this._then,
  );

  final Input$UserCreateInput _instance;

  final TRes Function(Input$UserCreateInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? dateBirth = _undefined,
    Object? firstName = _undefined,
    Object? hash = _undefined,
    Object? id = _undefined,
    Object? lastName = _undefined,
    Object? patronymic = _undefined,
    Object? uniqueNumber = _undefined,
  }) =>
      _then(Input$UserCreateInput._({
        ..._instance._$data,
        if (dateBirth != _undefined && dateBirth != null)
          'dateBirth': (dateBirth as DateTime),
        if (firstName != _undefined && firstName != null)
          'firstName': (firstName as String),
        if (hash != _undefined) 'hash': (hash as String?),
        if (id != _undefined) 'id': (id as String?),
        if (lastName != _undefined && lastName != null)
          'lastName': (lastName as String),
        if (patronymic != _undefined && patronymic != null)
          'patronymic': (patronymic as String),
        if (uniqueNumber != _undefined && uniqueNumber != null)
          'uniqueNumber': (uniqueNumber as String),
      }));
}

class _CopyWithStubImpl$Input$UserCreateInput<TRes>
    implements CopyWith$Input$UserCreateInput<TRes> {
  _CopyWithStubImpl$Input$UserCreateInput(this._res);

  TRes _res;

  call({
    DateTime? dateBirth,
    String? firstName,
    String? hash,
    String? id,
    String? lastName,
    String? patronymic,
    String? uniqueNumber,
  }) =>
      _res;
}

const possibleTypesMap = <String, Set<String>>{};
