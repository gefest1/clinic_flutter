import 'dart:async';
import 'package:clinic/utils/date_time_parse.dart';
import 'package:gql/ast.dart';
import 'package:graphql/client.dart' as graphql;
import 'package:http/http.dart';
import 'schema.graphql.dart';

class Fragment$AuthTokenFragment {
  Fragment$AuthTokenFragment({
    required this.accessToken,
    required this.refreshToken,
    this.$__typename = 'AuthToken',
  });

  factory Fragment$AuthTokenFragment.fromJson(Map<String, dynamic> json) {
    final l$accessToken = json['accessToken'];
    final l$refreshToken = json['refreshToken'];
    final l$$__typename = json['__typename'];
    return Fragment$AuthTokenFragment(
      accessToken: (l$accessToken as String),
      refreshToken: (l$refreshToken as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String accessToken;

  final String refreshToken;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$accessToken = accessToken;
    _resultData['accessToken'] = l$accessToken;
    final l$refreshToken = refreshToken;
    _resultData['refreshToken'] = l$refreshToken;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$accessToken = accessToken;
    final l$refreshToken = refreshToken;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$accessToken,
      l$refreshToken,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$AuthTokenFragment) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$accessToken = accessToken;
    final lOther$accessToken = other.accessToken;
    if (l$accessToken != lOther$accessToken) {
      return false;
    }
    final l$refreshToken = refreshToken;
    final lOther$refreshToken = other.refreshToken;
    if (l$refreshToken != lOther$refreshToken) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$AuthTokenFragment
    on Fragment$AuthTokenFragment {
  CopyWith$Fragment$AuthTokenFragment<Fragment$AuthTokenFragment>
      get copyWith => CopyWith$Fragment$AuthTokenFragment(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Fragment$AuthTokenFragment<TRes> {
  factory CopyWith$Fragment$AuthTokenFragment(
    Fragment$AuthTokenFragment instance,
    TRes Function(Fragment$AuthTokenFragment) then,
  ) = _CopyWithImpl$Fragment$AuthTokenFragment;

  factory CopyWith$Fragment$AuthTokenFragment.stub(TRes res) =
      _CopyWithStubImpl$Fragment$AuthTokenFragment;

  TRes call({
    String? accessToken,
    String? refreshToken,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$AuthTokenFragment<TRes>
    implements CopyWith$Fragment$AuthTokenFragment<TRes> {
  _CopyWithImpl$Fragment$AuthTokenFragment(
    this._instance,
    this._then,
  );

  final Fragment$AuthTokenFragment _instance;

  final TRes Function(Fragment$AuthTokenFragment) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? accessToken = _undefined,
    Object? refreshToken = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$AuthTokenFragment(
        accessToken: accessToken == _undefined || accessToken == null
            ? _instance.accessToken
            : (accessToken as String),
        refreshToken: refreshToken == _undefined || refreshToken == null
            ? _instance.refreshToken
            : (refreshToken as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$AuthTokenFragment<TRes>
    implements CopyWith$Fragment$AuthTokenFragment<TRes> {
  _CopyWithStubImpl$Fragment$AuthTokenFragment(this._res);

  TRes _res;

  call({
    String? accessToken,
    String? refreshToken,
    String? $__typename,
  }) =>
      _res;
}

const fragmentDefinitionAuthTokenFragment = FragmentDefinitionNode(
  name: NameNode(value: 'AuthTokenFragment'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'AuthToken'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'accessToken'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'refreshToken'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentAuthTokenFragment = DocumentNode(definitions: [
  fragmentDefinitionAuthTokenFragment,
]);

extension ClientExtension$Fragment$AuthTokenFragment on graphql.GraphQLClient {
  void writeFragment$AuthTokenFragment({
    required Fragment$AuthTokenFragment data,
    required Map<String, dynamic> idFields,
    bool broadcast = true,
  }) =>
      this.writeFragment(
        graphql.FragmentRequest(
          idFields: idFields,
          fragment: const graphql.Fragment(
            fragmentName: 'AuthTokenFragment',
            document: documentNodeFragmentAuthTokenFragment,
          ),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Fragment$AuthTokenFragment? readFragment$AuthTokenFragment({
    required Map<String, dynamic> idFields,
    bool optimistic = true,
  }) {
    final result = this.readFragment(
      graphql.FragmentRequest(
        idFields: idFields,
        fragment: const graphql.Fragment(
          fragmentName: 'AuthTokenFragment',
          document: documentNodeFragmentAuthTokenFragment,
        ),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Fragment$AuthTokenFragment.fromJson(result);
  }
}

class Fragment$UserFragment {
  Fragment$UserFragment({
    required this.id,
    required this.lastName,
    required this.dateBirth,
    required this.firstName,
    required this.patronymic,
    required this.uniqueNumber,
    this.$__typename = 'User',
  });

  factory Fragment$UserFragment.fromJson(Map<String, dynamic> json) {
    final l$id = json['id'];
    final l$lastName = json['lastName'];
    final l$dateBirth = json['dateBirth'];
    final l$firstName = json['firstName'];
    final l$patronymic = json['patronymic'];
    final l$uniqueNumber = json['uniqueNumber'];
    final l$$__typename = json['__typename'];
    return Fragment$UserFragment(
      id: (l$id as String),
      lastName: (l$lastName as String),
      dateBirth: dateTimeParseG(l$dateBirth),
      firstName: (l$firstName as String),
      patronymic: (l$patronymic as String),
      uniqueNumber: (l$uniqueNumber as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String id;

  final String lastName;

  final DateTime dateBirth;

  final String firstName;

  final String patronymic;

  final String uniqueNumber;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$id = id;
    _resultData['id'] = l$id;
    final l$lastName = lastName;
    _resultData['lastName'] = l$lastName;
    final l$dateBirth = dateBirth;
    _resultData['dateBirth'] = toMapDateTimeParseG(l$dateBirth);
    final l$firstName = firstName;
    _resultData['firstName'] = l$firstName;
    final l$patronymic = patronymic;
    _resultData['patronymic'] = l$patronymic;
    final l$uniqueNumber = uniqueNumber;
    _resultData['uniqueNumber'] = l$uniqueNumber;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$id = id;
    final l$lastName = lastName;
    final l$dateBirth = dateBirth;
    final l$firstName = firstName;
    final l$patronymic = patronymic;
    final l$uniqueNumber = uniqueNumber;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$id,
      l$lastName,
      l$dateBirth,
      l$firstName,
      l$patronymic,
      l$uniqueNumber,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Fragment$UserFragment) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$id = id;
    final lOther$id = other.id;
    if (l$id != lOther$id) {
      return false;
    }
    final l$lastName = lastName;
    final lOther$lastName = other.lastName;
    if (l$lastName != lOther$lastName) {
      return false;
    }
    final l$dateBirth = dateBirth;
    final lOther$dateBirth = other.dateBirth;
    if (l$dateBirth != lOther$dateBirth) {
      return false;
    }
    final l$firstName = firstName;
    final lOther$firstName = other.firstName;
    if (l$firstName != lOther$firstName) {
      return false;
    }
    final l$patronymic = patronymic;
    final lOther$patronymic = other.patronymic;
    if (l$patronymic != lOther$patronymic) {
      return false;
    }
    final l$uniqueNumber = uniqueNumber;
    final lOther$uniqueNumber = other.uniqueNumber;
    if (l$uniqueNumber != lOther$uniqueNumber) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Fragment$UserFragment on Fragment$UserFragment {
  CopyWith$Fragment$UserFragment<Fragment$UserFragment> get copyWith =>
      CopyWith$Fragment$UserFragment(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Fragment$UserFragment<TRes> {
  factory CopyWith$Fragment$UserFragment(
    Fragment$UserFragment instance,
    TRes Function(Fragment$UserFragment) then,
  ) = _CopyWithImpl$Fragment$UserFragment;

  factory CopyWith$Fragment$UserFragment.stub(TRes res) =
      _CopyWithStubImpl$Fragment$UserFragment;

  TRes call({
    String? id,
    String? lastName,
    DateTime? dateBirth,
    String? firstName,
    String? patronymic,
    String? uniqueNumber,
    String? $__typename,
  });
}

class _CopyWithImpl$Fragment$UserFragment<TRes>
    implements CopyWith$Fragment$UserFragment<TRes> {
  _CopyWithImpl$Fragment$UserFragment(
    this._instance,
    this._then,
  );

  final Fragment$UserFragment _instance;

  final TRes Function(Fragment$UserFragment) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? id = _undefined,
    Object? lastName = _undefined,
    Object? dateBirth = _undefined,
    Object? firstName = _undefined,
    Object? patronymic = _undefined,
    Object? uniqueNumber = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Fragment$UserFragment(
        id: id == _undefined || id == null ? _instance.id : (id as String),
        lastName: lastName == _undefined || lastName == null
            ? _instance.lastName
            : (lastName as String),
        dateBirth: dateBirth == _undefined || dateBirth == null
            ? _instance.dateBirth
            : (dateBirth as DateTime),
        firstName: firstName == _undefined || firstName == null
            ? _instance.firstName
            : (firstName as String),
        patronymic: patronymic == _undefined || patronymic == null
            ? _instance.patronymic
            : (patronymic as String),
        uniqueNumber: uniqueNumber == _undefined || uniqueNumber == null
            ? _instance.uniqueNumber
            : (uniqueNumber as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Fragment$UserFragment<TRes>
    implements CopyWith$Fragment$UserFragment<TRes> {
  _CopyWithStubImpl$Fragment$UserFragment(this._res);

  TRes _res;

  call({
    String? id,
    String? lastName,
    DateTime? dateBirth,
    String? firstName,
    String? patronymic,
    String? uniqueNumber,
    String? $__typename,
  }) =>
      _res;
}

const fragmentDefinitionUserFragment = FragmentDefinitionNode(
  name: NameNode(value: 'UserFragment'),
  typeCondition: TypeConditionNode(
      on: NamedTypeNode(
    name: NameNode(value: 'User'),
    isNonNull: false,
  )),
  directives: [],
  selectionSet: SelectionSetNode(selections: [
    FieldNode(
      name: NameNode(value: 'id'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'lastName'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'dateBirth'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'firstName'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'patronymic'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: 'uniqueNumber'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
    FieldNode(
      name: NameNode(value: '__typename'),
      alias: null,
      arguments: [],
      directives: [],
      selectionSet: null,
    ),
  ]),
);
const documentNodeFragmentUserFragment = DocumentNode(definitions: [
  fragmentDefinitionUserFragment,
]);

extension ClientExtension$Fragment$UserFragment on graphql.GraphQLClient {
  void writeFragment$UserFragment({
    required Fragment$UserFragment data,
    required Map<String, dynamic> idFields,
    bool broadcast = true,
  }) =>
      this.writeFragment(
        graphql.FragmentRequest(
          idFields: idFields,
          fragment: const graphql.Fragment(
            fragmentName: 'UserFragment',
            document: documentNodeFragmentUserFragment,
          ),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Fragment$UserFragment? readFragment$UserFragment({
    required Map<String, dynamic> idFields,
    bool optimistic = true,
  }) {
    final result = this.readFragment(
      graphql.FragmentRequest(
        idFields: idFields,
        fragment: const graphql.Fragment(
          fragmentName: 'UserFragment',
          document: documentNodeFragmentUserFragment,
        ),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Fragment$UserFragment.fromJson(result);
  }
}

class Variables$Query$isRegistered {
  factory Variables$Query$isRegistered({required String isRegistered}) =>
      Variables$Query$isRegistered._({
        r'isRegistered': isRegistered,
      });

  Variables$Query$isRegistered._(this._$data);

  factory Variables$Query$isRegistered.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$isRegistered = data['isRegistered'];
    result$data['isRegistered'] = (l$isRegistered as String);
    return Variables$Query$isRegistered._(result$data);
  }

  Map<String, dynamic> _$data;

  String get isRegistered => (_$data['isRegistered'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$isRegistered = isRegistered;
    result$data['isRegistered'] = l$isRegistered;
    return result$data;
  }

  CopyWith$Variables$Query$isRegistered<Variables$Query$isRegistered>
      get copyWith => CopyWith$Variables$Query$isRegistered(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$isRegistered) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$isRegistered = isRegistered;
    final lOther$isRegistered = other.isRegistered;
    if (l$isRegistered != lOther$isRegistered) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$isRegistered = isRegistered;
    return Object.hashAll([l$isRegistered]);
  }
}

abstract class CopyWith$Variables$Query$isRegistered<TRes> {
  factory CopyWith$Variables$Query$isRegistered(
    Variables$Query$isRegistered instance,
    TRes Function(Variables$Query$isRegistered) then,
  ) = _CopyWithImpl$Variables$Query$isRegistered;

  factory CopyWith$Variables$Query$isRegistered.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$isRegistered;

  TRes call({String? isRegistered});
}

class _CopyWithImpl$Variables$Query$isRegistered<TRes>
    implements CopyWith$Variables$Query$isRegistered<TRes> {
  _CopyWithImpl$Variables$Query$isRegistered(
    this._instance,
    this._then,
  );

  final Variables$Query$isRegistered _instance;

  final TRes Function(Variables$Query$isRegistered) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? isRegistered = _undefined}) =>
      _then(Variables$Query$isRegistered._({
        ..._instance._$data,
        if (isRegistered != _undefined && isRegistered != null)
          'isRegistered': (isRegistered as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$isRegistered<TRes>
    implements CopyWith$Variables$Query$isRegistered<TRes> {
  _CopyWithStubImpl$Variables$Query$isRegistered(this._res);

  TRes _res;

  call({String? isRegistered}) => _res;
}

class Query$isRegistered {
  Query$isRegistered({
    required this.isRegistered,
    this.$__typename = 'Query',
  });

  factory Query$isRegistered.fromJson(Map<String, dynamic> json) {
    final l$isRegistered = json['isRegistered'];
    final l$$__typename = json['__typename'];
    return Query$isRegistered(
      isRegistered: (l$isRegistered as bool),
      $__typename: (l$$__typename as String),
    );
  }

  final bool isRegistered;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$isRegistered = isRegistered;
    _resultData['isRegistered'] = l$isRegistered;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$isRegistered = isRegistered;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$isRegistered,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$isRegistered) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$isRegistered = isRegistered;
    final lOther$isRegistered = other.isRegistered;
    if (l$isRegistered != lOther$isRegistered) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$isRegistered on Query$isRegistered {
  CopyWith$Query$isRegistered<Query$isRegistered> get copyWith =>
      CopyWith$Query$isRegistered(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$isRegistered<TRes> {
  factory CopyWith$Query$isRegistered(
    Query$isRegistered instance,
    TRes Function(Query$isRegistered) then,
  ) = _CopyWithImpl$Query$isRegistered;

  factory CopyWith$Query$isRegistered.stub(TRes res) =
      _CopyWithStubImpl$Query$isRegistered;

  TRes call({
    bool? isRegistered,
    String? $__typename,
  });
}

class _CopyWithImpl$Query$isRegistered<TRes>
    implements CopyWith$Query$isRegistered<TRes> {
  _CopyWithImpl$Query$isRegistered(
    this._instance,
    this._then,
  );

  final Query$isRegistered _instance;

  final TRes Function(Query$isRegistered) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? isRegistered = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$isRegistered(
        isRegistered: isRegistered == _undefined || isRegistered == null
            ? _instance.isRegistered
            : (isRegistered as bool),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Query$isRegistered<TRes>
    implements CopyWith$Query$isRegistered<TRes> {
  _CopyWithStubImpl$Query$isRegistered(this._res);

  TRes _res;

  call({
    bool? isRegistered,
    String? $__typename,
  }) =>
      _res;
}

const documentNodeQueryisRegistered = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'isRegistered'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'isRegistered')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'isRegistered'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'uniqueNumber'),
            value: VariableNode(name: NameNode(value: 'isRegistered')),
          )
        ],
        directives: [],
        selectionSet: null,
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Query$isRegistered _parserFn$Query$isRegistered(Map<String, dynamic> data) =>
    Query$isRegistered.fromJson(data);
typedef OnQueryComplete$Query$isRegistered = FutureOr<void> Function(
  Map<String, dynamic>?,
  Query$isRegistered?,
);

class Options$Query$isRegistered
    extends graphql.QueryOptions<Query$isRegistered> {
  Options$Query$isRegistered({
    String? operationName,
    required Variables$Query$isRegistered variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$isRegistered? typedOptimisticResult,
    Duration? pollInterval,
    graphql.Context? context,
    OnQueryComplete$Query$isRegistered? onComplete,
    graphql.OnQueryError? onError,
  })  : onCompleteWithParsed = onComplete,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          pollInterval: pollInterval,
          context: context,
          onComplete: onComplete == null
              ? null
              : (data) => onComplete(
                    data,
                    data == null ? null : _parserFn$Query$isRegistered(data),
                  ),
          onError: onError,
          document: documentNodeQueryisRegistered,
          parserFn: _parserFn$Query$isRegistered,
        );

  final OnQueryComplete$Query$isRegistered? onCompleteWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onComplete == null
            ? super.properties
            : super.properties.where((property) => property != onComplete),
        onCompleteWithParsed,
      ];
}

class WatchOptions$Query$isRegistered
    extends graphql.WatchQueryOptions<Query$isRegistered> {
  WatchOptions$Query$isRegistered({
    String? operationName,
    required Variables$Query$isRegistered variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$isRegistered? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeQueryisRegistered,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Query$isRegistered,
        );
}

class FetchMoreOptions$Query$isRegistered extends graphql.FetchMoreOptions {
  FetchMoreOptions$Query$isRegistered({
    required graphql.UpdateQuery updateQuery,
    required Variables$Query$isRegistered variables,
  }) : super(
          updateQuery: updateQuery,
          variables: variables.toJson(),
          document: documentNodeQueryisRegistered,
        );
}

extension ClientExtension$Query$isRegistered on graphql.GraphQLClient {
  Future<graphql.QueryResult<Query$isRegistered>> query$isRegistered(
          Options$Query$isRegistered options) async =>
      await this.query(options);
  graphql.ObservableQuery<Query$isRegistered> watchQuery$isRegistered(
          WatchOptions$Query$isRegistered options) =>
      this.watchQuery(options);
  void writeQuery$isRegistered({
    required Query$isRegistered data,
    required Variables$Query$isRegistered variables,
    bool broadcast = true,
  }) =>
      this.writeQuery(
        graphql.Request(
          operation: graphql.Operation(document: documentNodeQueryisRegistered),
          variables: variables.toJson(),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Query$isRegistered? readQuery$isRegistered({
    required Variables$Query$isRegistered variables,
    bool optimistic = true,
  }) {
    final result = this.readQuery(
      graphql.Request(
        operation: graphql.Operation(document: documentNodeQueryisRegistered),
        variables: variables.toJson(),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Query$isRegistered.fromJson(result);
  }
}

class Variables$Mutation$CheckBio {
  factory Variables$Mutation$CheckBio({
    required List<MultipartFile> pipelines,
    required String uniqueNumber,
  }) =>
      Variables$Mutation$CheckBio._({
        r'pipelines': pipelines,
        r'uniqueNumber': uniqueNumber,
      });

  Variables$Mutation$CheckBio._(this._$data);

  factory Variables$Mutation$CheckBio.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$pipelines = data['pipelines'];
    result$data['pipelines'] = (l$pipelines as List<dynamic>)
        .map((e) => (e as MultipartFile))
        .toList();
    final l$uniqueNumber = data['uniqueNumber'];
    result$data['uniqueNumber'] = (l$uniqueNumber as String);
    return Variables$Mutation$CheckBio._(result$data);
  }

  Map<String, dynamic> _$data;

  List<MultipartFile> get pipelines =>
      (_$data['pipelines'] as List<MultipartFile>);
  String get uniqueNumber => (_$data['uniqueNumber'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$pipelines = pipelines;
    result$data['pipelines'] = l$pipelines.map((e) => e).toList();
    final l$uniqueNumber = uniqueNumber;
    result$data['uniqueNumber'] = l$uniqueNumber;
    return result$data;
  }

  CopyWith$Variables$Mutation$CheckBio<Variables$Mutation$CheckBio>
      get copyWith => CopyWith$Variables$Mutation$CheckBio(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$CheckBio) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$pipelines = pipelines;
    final lOther$pipelines = other.pipelines;
    if (l$pipelines.length != lOther$pipelines.length) {
      return false;
    }
    for (int i = 0; i < l$pipelines.length; i++) {
      final l$pipelines$entry = l$pipelines[i];
      final lOther$pipelines$entry = lOther$pipelines[i];
      if (l$pipelines$entry != lOther$pipelines$entry) {
        return false;
      }
    }
    final l$uniqueNumber = uniqueNumber;
    final lOther$uniqueNumber = other.uniqueNumber;
    if (l$uniqueNumber != lOther$uniqueNumber) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$pipelines = pipelines;
    final l$uniqueNumber = uniqueNumber;
    return Object.hashAll([
      Object.hashAll(l$pipelines.map((v) => v)),
      l$uniqueNumber,
    ]);
  }
}

abstract class CopyWith$Variables$Mutation$CheckBio<TRes> {
  factory CopyWith$Variables$Mutation$CheckBio(
    Variables$Mutation$CheckBio instance,
    TRes Function(Variables$Mutation$CheckBio) then,
  ) = _CopyWithImpl$Variables$Mutation$CheckBio;

  factory CopyWith$Variables$Mutation$CheckBio.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$CheckBio;

  TRes call({
    List<MultipartFile>? pipelines,
    String? uniqueNumber,
  });
}

class _CopyWithImpl$Variables$Mutation$CheckBio<TRes>
    implements CopyWith$Variables$Mutation$CheckBio<TRes> {
  _CopyWithImpl$Variables$Mutation$CheckBio(
    this._instance,
    this._then,
  );

  final Variables$Mutation$CheckBio _instance;

  final TRes Function(Variables$Mutation$CheckBio) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? pipelines = _undefined,
    Object? uniqueNumber = _undefined,
  }) =>
      _then(Variables$Mutation$CheckBio._({
        ..._instance._$data,
        if (pipelines != _undefined && pipelines != null)
          'pipelines': (pipelines as List<MultipartFile>),
        if (uniqueNumber != _undefined && uniqueNumber != null)
          'uniqueNumber': (uniqueNumber as String),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$CheckBio<TRes>
    implements CopyWith$Variables$Mutation$CheckBio<TRes> {
  _CopyWithStubImpl$Variables$Mutation$CheckBio(this._res);

  TRes _res;

  call({
    List<MultipartFile>? pipelines,
    String? uniqueNumber,
  }) =>
      _res;
}

class Mutation$CheckBio {
  Mutation$CheckBio({
    required this.checkBio,
    this.$__typename = 'Mutation',
  });

  factory Mutation$CheckBio.fromJson(Map<String, dynamic> json) {
    final l$checkBio = json['checkBio'];
    final l$$__typename = json['__typename'];
    return Mutation$CheckBio(
      checkBio: Mutation$CheckBio$checkBio.fromJson(
          (l$checkBio as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Mutation$CheckBio$checkBio checkBio;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$checkBio = checkBio;
    _resultData['checkBio'] = l$checkBio.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$checkBio = checkBio;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$checkBio,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$CheckBio) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$checkBio = checkBio;
    final lOther$checkBio = other.checkBio;
    if (l$checkBio != lOther$checkBio) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$CheckBio on Mutation$CheckBio {
  CopyWith$Mutation$CheckBio<Mutation$CheckBio> get copyWith =>
      CopyWith$Mutation$CheckBio(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$CheckBio<TRes> {
  factory CopyWith$Mutation$CheckBio(
    Mutation$CheckBio instance,
    TRes Function(Mutation$CheckBio) then,
  ) = _CopyWithImpl$Mutation$CheckBio;

  factory CopyWith$Mutation$CheckBio.stub(TRes res) =
      _CopyWithStubImpl$Mutation$CheckBio;

  TRes call({
    Mutation$CheckBio$checkBio? checkBio,
    String? $__typename,
  });
  CopyWith$Mutation$CheckBio$checkBio<TRes> get checkBio;
}

class _CopyWithImpl$Mutation$CheckBio<TRes>
    implements CopyWith$Mutation$CheckBio<TRes> {
  _CopyWithImpl$Mutation$CheckBio(
    this._instance,
    this._then,
  );

  final Mutation$CheckBio _instance;

  final TRes Function(Mutation$CheckBio) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? checkBio = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$CheckBio(
        checkBio: checkBio == _undefined || checkBio == null
            ? _instance.checkBio
            : (checkBio as Mutation$CheckBio$checkBio),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Mutation$CheckBio$checkBio<TRes> get checkBio {
    final local$checkBio = _instance.checkBio;
    return CopyWith$Mutation$CheckBio$checkBio(
        local$checkBio, (e) => call(checkBio: e));
  }
}

class _CopyWithStubImpl$Mutation$CheckBio<TRes>
    implements CopyWith$Mutation$CheckBio<TRes> {
  _CopyWithStubImpl$Mutation$CheckBio(this._res);

  TRes _res;

  call({
    Mutation$CheckBio$checkBio? checkBio,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Mutation$CheckBio$checkBio<TRes> get checkBio =>
      CopyWith$Mutation$CheckBio$checkBio.stub(_res);
}

const documentNodeMutationCheckBio = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'CheckBio'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'pipelines')),
        type: ListTypeNode(
          type: NamedTypeNode(
            name: NameNode(value: 'Upload'),
            isNonNull: true,
          ),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'uniqueNumber')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'checkBio'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'pipelines'),
            value: VariableNode(name: NameNode(value: 'pipelines')),
          ),
          ArgumentNode(
            name: NameNode(value: 'uniqueNumber'),
            value: VariableNode(name: NameNode(value: 'uniqueNumber')),
          ),
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'lastName'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'dateBirth'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'firstName'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'patronymic'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Mutation$CheckBio _parserFn$Mutation$CheckBio(Map<String, dynamic> data) =>
    Mutation$CheckBio.fromJson(data);
typedef OnMutationCompleted$Mutation$CheckBio = FutureOr<void> Function(
  Map<String, dynamic>?,
  Mutation$CheckBio?,
);

class Options$Mutation$CheckBio
    extends graphql.MutationOptions<Mutation$CheckBio> {
  Options$Mutation$CheckBio({
    String? operationName,
    required Variables$Mutation$CheckBio variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$CheckBio? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$CheckBio? onCompleted,
    graphql.OnMutationUpdate<Mutation$CheckBio>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null ? null : _parserFn$Mutation$CheckBio(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationCheckBio,
          parserFn: _parserFn$Mutation$CheckBio,
        );

  final OnMutationCompleted$Mutation$CheckBio? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$CheckBio
    extends graphql.WatchQueryOptions<Mutation$CheckBio> {
  WatchOptions$Mutation$CheckBio({
    String? operationName,
    required Variables$Mutation$CheckBio variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$CheckBio? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationCheckBio,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$CheckBio,
        );
}

extension ClientExtension$Mutation$CheckBio on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$CheckBio>> mutate$CheckBio(
          Options$Mutation$CheckBio options) async =>
      await this.mutate(options);
  graphql.ObservableQuery<Mutation$CheckBio> watchMutation$CheckBio(
          WatchOptions$Mutation$CheckBio options) =>
      this.watchMutation(options);
}

class Mutation$CheckBio$checkBio {
  Mutation$CheckBio$checkBio({
    required this.lastName,
    required this.dateBirth,
    required this.firstName,
    required this.patronymic,
    this.$__typename = 'UserBioData',
  });

  factory Mutation$CheckBio$checkBio.fromJson(Map<String, dynamic> json) {
    final l$lastName = json['lastName'];
    final l$dateBirth = json['dateBirth'];
    final l$firstName = json['firstName'];
    final l$patronymic = json['patronymic'];
    final l$$__typename = json['__typename'];
    return Mutation$CheckBio$checkBio(
      lastName: (l$lastName as String),
      dateBirth: dateTimeParseG(l$dateBirth),
      firstName: (l$firstName as String),
      patronymic: (l$patronymic as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String lastName;

  final DateTime dateBirth;

  final String firstName;

  final String patronymic;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$lastName = lastName;
    _resultData['lastName'] = l$lastName;
    final l$dateBirth = dateBirth;
    _resultData['dateBirth'] = toMapDateTimeParseG(l$dateBirth);
    final l$firstName = firstName;
    _resultData['firstName'] = l$firstName;
    final l$patronymic = patronymic;
    _resultData['patronymic'] = l$patronymic;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$lastName = lastName;
    final l$dateBirth = dateBirth;
    final l$firstName = firstName;
    final l$patronymic = patronymic;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$lastName,
      l$dateBirth,
      l$firstName,
      l$patronymic,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$CheckBio$checkBio) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$lastName = lastName;
    final lOther$lastName = other.lastName;
    if (l$lastName != lOther$lastName) {
      return false;
    }
    final l$dateBirth = dateBirth;
    final lOther$dateBirth = other.dateBirth;
    if (l$dateBirth != lOther$dateBirth) {
      return false;
    }
    final l$firstName = firstName;
    final lOther$firstName = other.firstName;
    if (l$firstName != lOther$firstName) {
      return false;
    }
    final l$patronymic = patronymic;
    final lOther$patronymic = other.patronymic;
    if (l$patronymic != lOther$patronymic) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$CheckBio$checkBio
    on Mutation$CheckBio$checkBio {
  CopyWith$Mutation$CheckBio$checkBio<Mutation$CheckBio$checkBio>
      get copyWith => CopyWith$Mutation$CheckBio$checkBio(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Mutation$CheckBio$checkBio<TRes> {
  factory CopyWith$Mutation$CheckBio$checkBio(
    Mutation$CheckBio$checkBio instance,
    TRes Function(Mutation$CheckBio$checkBio) then,
  ) = _CopyWithImpl$Mutation$CheckBio$checkBio;

  factory CopyWith$Mutation$CheckBio$checkBio.stub(TRes res) =
      _CopyWithStubImpl$Mutation$CheckBio$checkBio;

  TRes call({
    String? lastName,
    DateTime? dateBirth,
    String? firstName,
    String? patronymic,
    String? $__typename,
  });
}

class _CopyWithImpl$Mutation$CheckBio$checkBio<TRes>
    implements CopyWith$Mutation$CheckBio$checkBio<TRes> {
  _CopyWithImpl$Mutation$CheckBio$checkBio(
    this._instance,
    this._then,
  );

  final Mutation$CheckBio$checkBio _instance;

  final TRes Function(Mutation$CheckBio$checkBio) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? lastName = _undefined,
    Object? dateBirth = _undefined,
    Object? firstName = _undefined,
    Object? patronymic = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$CheckBio$checkBio(
        lastName: lastName == _undefined || lastName == null
            ? _instance.lastName
            : (lastName as String),
        dateBirth: dateBirth == _undefined || dateBirth == null
            ? _instance.dateBirth
            : (dateBirth as DateTime),
        firstName: firstName == _undefined || firstName == null
            ? _instance.firstName
            : (firstName as String),
        patronymic: patronymic == _undefined || patronymic == null
            ? _instance.patronymic
            : (patronymic as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Mutation$CheckBio$checkBio<TRes>
    implements CopyWith$Mutation$CheckBio$checkBio<TRes> {
  _CopyWithStubImpl$Mutation$CheckBio$checkBio(this._res);

  TRes _res;

  call({
    String? lastName,
    DateTime? dateBirth,
    String? firstName,
    String? patronymic,
    String? $__typename,
  }) =>
      _res;
}

class Variables$Mutation$SendCode {
  factory Variables$Mutation$SendCode({required String phoneNumber}) =>
      Variables$Mutation$SendCode._({
        r'phoneNumber': phoneNumber,
      });

  Variables$Mutation$SendCode._(this._$data);

  factory Variables$Mutation$SendCode.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$phoneNumber = data['phoneNumber'];
    result$data['phoneNumber'] = (l$phoneNumber as String);
    return Variables$Mutation$SendCode._(result$data);
  }

  Map<String, dynamic> _$data;

  String get phoneNumber => (_$data['phoneNumber'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$phoneNumber = phoneNumber;
    result$data['phoneNumber'] = l$phoneNumber;
    return result$data;
  }

  CopyWith$Variables$Mutation$SendCode<Variables$Mutation$SendCode>
      get copyWith => CopyWith$Variables$Mutation$SendCode(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$SendCode) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$phoneNumber = phoneNumber;
    final lOther$phoneNumber = other.phoneNumber;
    if (l$phoneNumber != lOther$phoneNumber) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$phoneNumber = phoneNumber;
    return Object.hashAll([l$phoneNumber]);
  }
}

abstract class CopyWith$Variables$Mutation$SendCode<TRes> {
  factory CopyWith$Variables$Mutation$SendCode(
    Variables$Mutation$SendCode instance,
    TRes Function(Variables$Mutation$SendCode) then,
  ) = _CopyWithImpl$Variables$Mutation$SendCode;

  factory CopyWith$Variables$Mutation$SendCode.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$SendCode;

  TRes call({String? phoneNumber});
}

class _CopyWithImpl$Variables$Mutation$SendCode<TRes>
    implements CopyWith$Variables$Mutation$SendCode<TRes> {
  _CopyWithImpl$Variables$Mutation$SendCode(
    this._instance,
    this._then,
  );

  final Variables$Mutation$SendCode _instance;

  final TRes Function(Variables$Mutation$SendCode) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? phoneNumber = _undefined}) =>
      _then(Variables$Mutation$SendCode._({
        ..._instance._$data,
        if (phoneNumber != _undefined && phoneNumber != null)
          'phoneNumber': (phoneNumber as String),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$SendCode<TRes>
    implements CopyWith$Variables$Mutation$SendCode<TRes> {
  _CopyWithStubImpl$Variables$Mutation$SendCode(this._res);

  TRes _res;

  call({String? phoneNumber}) => _res;
}

class Mutation$SendCode {
  Mutation$SendCode({
    required this.sendWbVerify,
    this.$__typename = 'Mutation',
  });

  factory Mutation$SendCode.fromJson(Map<String, dynamic> json) {
    final l$sendWbVerify = json['sendWbVerify'];
    final l$$__typename = json['__typename'];
    return Mutation$SendCode(
      sendWbVerify: (l$sendWbVerify as bool),
      $__typename: (l$$__typename as String),
    );
  }

  final bool sendWbVerify;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$sendWbVerify = sendWbVerify;
    _resultData['sendWbVerify'] = l$sendWbVerify;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$sendWbVerify = sendWbVerify;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$sendWbVerify,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$SendCode) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$sendWbVerify = sendWbVerify;
    final lOther$sendWbVerify = other.sendWbVerify;
    if (l$sendWbVerify != lOther$sendWbVerify) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$SendCode on Mutation$SendCode {
  CopyWith$Mutation$SendCode<Mutation$SendCode> get copyWith =>
      CopyWith$Mutation$SendCode(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$SendCode<TRes> {
  factory CopyWith$Mutation$SendCode(
    Mutation$SendCode instance,
    TRes Function(Mutation$SendCode) then,
  ) = _CopyWithImpl$Mutation$SendCode;

  factory CopyWith$Mutation$SendCode.stub(TRes res) =
      _CopyWithStubImpl$Mutation$SendCode;

  TRes call({
    bool? sendWbVerify,
    String? $__typename,
  });
}

class _CopyWithImpl$Mutation$SendCode<TRes>
    implements CopyWith$Mutation$SendCode<TRes> {
  _CopyWithImpl$Mutation$SendCode(
    this._instance,
    this._then,
  );

  final Mutation$SendCode _instance;

  final TRes Function(Mutation$SendCode) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? sendWbVerify = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$SendCode(
        sendWbVerify: sendWbVerify == _undefined || sendWbVerify == null
            ? _instance.sendWbVerify
            : (sendWbVerify as bool),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Mutation$SendCode<TRes>
    implements CopyWith$Mutation$SendCode<TRes> {
  _CopyWithStubImpl$Mutation$SendCode(this._res);

  TRes _res;

  call({
    bool? sendWbVerify,
    String? $__typename,
  }) =>
      _res;
}

const documentNodeMutationSendCode = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'SendCode'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'phoneNumber')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'sendWbVerify'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'phoneNumber'),
            value: VariableNode(name: NameNode(value: 'phoneNumber')),
          )
        ],
        directives: [],
        selectionSet: null,
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Mutation$SendCode _parserFn$Mutation$SendCode(Map<String, dynamic> data) =>
    Mutation$SendCode.fromJson(data);
typedef OnMutationCompleted$Mutation$SendCode = FutureOr<void> Function(
  Map<String, dynamic>?,
  Mutation$SendCode?,
);

class Options$Mutation$SendCode
    extends graphql.MutationOptions<Mutation$SendCode> {
  Options$Mutation$SendCode({
    String? operationName,
    required Variables$Mutation$SendCode variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$SendCode? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$SendCode? onCompleted,
    graphql.OnMutationUpdate<Mutation$SendCode>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null ? null : _parserFn$Mutation$SendCode(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationSendCode,
          parserFn: _parserFn$Mutation$SendCode,
        );

  final OnMutationCompleted$Mutation$SendCode? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$SendCode
    extends graphql.WatchQueryOptions<Mutation$SendCode> {
  WatchOptions$Mutation$SendCode({
    String? operationName,
    required Variables$Mutation$SendCode variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$SendCode? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationSendCode,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$SendCode,
        );
}

extension ClientExtension$Mutation$SendCode on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$SendCode>> mutate$SendCode(
          Options$Mutation$SendCode options) async =>
      await this.mutate(options);
  graphql.ObservableQuery<Mutation$SendCode> watchMutation$SendCode(
          WatchOptions$Mutation$SendCode options) =>
      this.watchMutation(options);
}

class Variables$Mutation$CheckCode {
  factory Variables$Mutation$CheckCode({
    required String phoneNumber,
    required String code,
  }) =>
      Variables$Mutation$CheckCode._({
        r'phoneNumber': phoneNumber,
        r'code': code,
      });

  Variables$Mutation$CheckCode._(this._$data);

  factory Variables$Mutation$CheckCode.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$phoneNumber = data['phoneNumber'];
    result$data['phoneNumber'] = (l$phoneNumber as String);
    final l$code = data['code'];
    result$data['code'] = (l$code as String);
    return Variables$Mutation$CheckCode._(result$data);
  }

  Map<String, dynamic> _$data;

  String get phoneNumber => (_$data['phoneNumber'] as String);
  String get code => (_$data['code'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$phoneNumber = phoneNumber;
    result$data['phoneNumber'] = l$phoneNumber;
    final l$code = code;
    result$data['code'] = l$code;
    return result$data;
  }

  CopyWith$Variables$Mutation$CheckCode<Variables$Mutation$CheckCode>
      get copyWith => CopyWith$Variables$Mutation$CheckCode(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$CheckCode) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$phoneNumber = phoneNumber;
    final lOther$phoneNumber = other.phoneNumber;
    if (l$phoneNumber != lOther$phoneNumber) {
      return false;
    }
    final l$code = code;
    final lOther$code = other.code;
    if (l$code != lOther$code) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$phoneNumber = phoneNumber;
    final l$code = code;
    return Object.hashAll([
      l$phoneNumber,
      l$code,
    ]);
  }
}

abstract class CopyWith$Variables$Mutation$CheckCode<TRes> {
  factory CopyWith$Variables$Mutation$CheckCode(
    Variables$Mutation$CheckCode instance,
    TRes Function(Variables$Mutation$CheckCode) then,
  ) = _CopyWithImpl$Variables$Mutation$CheckCode;

  factory CopyWith$Variables$Mutation$CheckCode.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$CheckCode;

  TRes call({
    String? phoneNumber,
    String? code,
  });
}

class _CopyWithImpl$Variables$Mutation$CheckCode<TRes>
    implements CopyWith$Variables$Mutation$CheckCode<TRes> {
  _CopyWithImpl$Variables$Mutation$CheckCode(
    this._instance,
    this._then,
  );

  final Variables$Mutation$CheckCode _instance;

  final TRes Function(Variables$Mutation$CheckCode) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? phoneNumber = _undefined,
    Object? code = _undefined,
  }) =>
      _then(Variables$Mutation$CheckCode._({
        ..._instance._$data,
        if (phoneNumber != _undefined && phoneNumber != null)
          'phoneNumber': (phoneNumber as String),
        if (code != _undefined && code != null) 'code': (code as String),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$CheckCode<TRes>
    implements CopyWith$Variables$Mutation$CheckCode<TRes> {
  _CopyWithStubImpl$Variables$Mutation$CheckCode(this._res);

  TRes _res;

  call({
    String? phoneNumber,
    String? code,
  }) =>
      _res;
}

class Mutation$CheckCode {
  Mutation$CheckCode({
    required this.checkWbVerify,
    this.$__typename = 'Mutation',
  });

  factory Mutation$CheckCode.fromJson(Map<String, dynamic> json) {
    final l$checkWbVerify = json['checkWbVerify'];
    final l$$__typename = json['__typename'];
    return Mutation$CheckCode(
      checkWbVerify: (l$checkWbVerify as bool),
      $__typename: (l$$__typename as String),
    );
  }

  final bool checkWbVerify;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$checkWbVerify = checkWbVerify;
    _resultData['checkWbVerify'] = l$checkWbVerify;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$checkWbVerify = checkWbVerify;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$checkWbVerify,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$CheckCode) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$checkWbVerify = checkWbVerify;
    final lOther$checkWbVerify = other.checkWbVerify;
    if (l$checkWbVerify != lOther$checkWbVerify) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$CheckCode on Mutation$CheckCode {
  CopyWith$Mutation$CheckCode<Mutation$CheckCode> get copyWith =>
      CopyWith$Mutation$CheckCode(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$CheckCode<TRes> {
  factory CopyWith$Mutation$CheckCode(
    Mutation$CheckCode instance,
    TRes Function(Mutation$CheckCode) then,
  ) = _CopyWithImpl$Mutation$CheckCode;

  factory CopyWith$Mutation$CheckCode.stub(TRes res) =
      _CopyWithStubImpl$Mutation$CheckCode;

  TRes call({
    bool? checkWbVerify,
    String? $__typename,
  });
}

class _CopyWithImpl$Mutation$CheckCode<TRes>
    implements CopyWith$Mutation$CheckCode<TRes> {
  _CopyWithImpl$Mutation$CheckCode(
    this._instance,
    this._then,
  );

  final Mutation$CheckCode _instance;

  final TRes Function(Mutation$CheckCode) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? checkWbVerify = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$CheckCode(
        checkWbVerify: checkWbVerify == _undefined || checkWbVerify == null
            ? _instance.checkWbVerify
            : (checkWbVerify as bool),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Mutation$CheckCode<TRes>
    implements CopyWith$Mutation$CheckCode<TRes> {
  _CopyWithStubImpl$Mutation$CheckCode(this._res);

  TRes _res;

  call({
    bool? checkWbVerify,
    String? $__typename,
  }) =>
      _res;
}

const documentNodeMutationCheckCode = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'CheckCode'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'phoneNumber')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'code')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'checkWbVerify'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'phoneNumber'),
            value: VariableNode(name: NameNode(value: 'phoneNumber')),
          ),
          ArgumentNode(
            name: NameNode(value: 'code'),
            value: VariableNode(name: NameNode(value: 'code')),
          ),
        ],
        directives: [],
        selectionSet: null,
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Mutation$CheckCode _parserFn$Mutation$CheckCode(Map<String, dynamic> data) =>
    Mutation$CheckCode.fromJson(data);
typedef OnMutationCompleted$Mutation$CheckCode = FutureOr<void> Function(
  Map<String, dynamic>?,
  Mutation$CheckCode?,
);

class Options$Mutation$CheckCode
    extends graphql.MutationOptions<Mutation$CheckCode> {
  Options$Mutation$CheckCode({
    String? operationName,
    required Variables$Mutation$CheckCode variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$CheckCode? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$CheckCode? onCompleted,
    graphql.OnMutationUpdate<Mutation$CheckCode>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null ? null : _parserFn$Mutation$CheckCode(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationCheckCode,
          parserFn: _parserFn$Mutation$CheckCode,
        );

  final OnMutationCompleted$Mutation$CheckCode? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$CheckCode
    extends graphql.WatchQueryOptions<Mutation$CheckCode> {
  WatchOptions$Mutation$CheckCode({
    String? operationName,
    required Variables$Mutation$CheckCode variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$CheckCode? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationCheckCode,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$CheckCode,
        );
}

extension ClientExtension$Mutation$CheckCode on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$CheckCode>> mutate$CheckCode(
          Options$Mutation$CheckCode options) async =>
      await this.mutate(options);
  graphql.ObservableQuery<Mutation$CheckCode> watchMutation$CheckCode(
          WatchOptions$Mutation$CheckCode options) =>
      this.watchMutation(options);
}

class Variables$Mutation$finishRegistration {
  factory Variables$Mutation$finishRegistration({
    required Input$UserCreateInput data,
    required String password,
  }) =>
      Variables$Mutation$finishRegistration._({
        r'data': data,
        r'password': password,
      });

  Variables$Mutation$finishRegistration._(this._$data);

  factory Variables$Mutation$finishRegistration.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$data = data['data'];
    result$data['data'] =
        Input$UserCreateInput.fromJson((l$data as Map<String, dynamic>));
    final l$password = data['password'];
    result$data['password'] = (l$password as String);
    return Variables$Mutation$finishRegistration._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$UserCreateInput get data => (_$data['data'] as Input$UserCreateInput);
  String get password => (_$data['password'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$data = data;
    result$data['data'] = l$data.toJson();
    final l$password = password;
    result$data['password'] = l$password;
    return result$data;
  }

  CopyWith$Variables$Mutation$finishRegistration<
          Variables$Mutation$finishRegistration>
      get copyWith => CopyWith$Variables$Mutation$finishRegistration(
            this,
            (i) => i,
          );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$finishRegistration) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$data = data;
    final lOther$data = other.data;
    if (l$data != lOther$data) {
      return false;
    }
    final l$password = password;
    final lOther$password = other.password;
    if (l$password != lOther$password) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$data = data;
    final l$password = password;
    return Object.hashAll([
      l$data,
      l$password,
    ]);
  }
}

abstract class CopyWith$Variables$Mutation$finishRegistration<TRes> {
  factory CopyWith$Variables$Mutation$finishRegistration(
    Variables$Mutation$finishRegistration instance,
    TRes Function(Variables$Mutation$finishRegistration) then,
  ) = _CopyWithImpl$Variables$Mutation$finishRegistration;

  factory CopyWith$Variables$Mutation$finishRegistration.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$finishRegistration;

  TRes call({
    Input$UserCreateInput? data,
    String? password,
  });
}

class _CopyWithImpl$Variables$Mutation$finishRegistration<TRes>
    implements CopyWith$Variables$Mutation$finishRegistration<TRes> {
  _CopyWithImpl$Variables$Mutation$finishRegistration(
    this._instance,
    this._then,
  );

  final Variables$Mutation$finishRegistration _instance;

  final TRes Function(Variables$Mutation$finishRegistration) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? data = _undefined,
    Object? password = _undefined,
  }) =>
      _then(Variables$Mutation$finishRegistration._({
        ..._instance._$data,
        if (data != _undefined && data != null)
          'data': (data as Input$UserCreateInput),
        if (password != _undefined && password != null)
          'password': (password as String),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$finishRegistration<TRes>
    implements CopyWith$Variables$Mutation$finishRegistration<TRes> {
  _CopyWithStubImpl$Variables$Mutation$finishRegistration(this._res);

  TRes _res;

  call({
    Input$UserCreateInput? data,
    String? password,
  }) =>
      _res;
}

class Mutation$finishRegistration {
  Mutation$finishRegistration({
    required this.fullForm,
    this.$__typename = 'Mutation',
  });

  factory Mutation$finishRegistration.fromJson(Map<String, dynamic> json) {
    final l$fullForm = json['fullForm'];
    final l$$__typename = json['__typename'];
    return Mutation$finishRegistration(
      fullForm: Mutation$finishRegistration$fullForm.fromJson(
          (l$fullForm as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Mutation$finishRegistration$fullForm fullForm;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$fullForm = fullForm;
    _resultData['fullForm'] = l$fullForm.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$fullForm = fullForm;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$fullForm,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$finishRegistration) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$fullForm = fullForm;
    final lOther$fullForm = other.fullForm;
    if (l$fullForm != lOther$fullForm) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$finishRegistration
    on Mutation$finishRegistration {
  CopyWith$Mutation$finishRegistration<Mutation$finishRegistration>
      get copyWith => CopyWith$Mutation$finishRegistration(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Mutation$finishRegistration<TRes> {
  factory CopyWith$Mutation$finishRegistration(
    Mutation$finishRegistration instance,
    TRes Function(Mutation$finishRegistration) then,
  ) = _CopyWithImpl$Mutation$finishRegistration;

  factory CopyWith$Mutation$finishRegistration.stub(TRes res) =
      _CopyWithStubImpl$Mutation$finishRegistration;

  TRes call({
    Mutation$finishRegistration$fullForm? fullForm,
    String? $__typename,
  });
  CopyWith$Mutation$finishRegistration$fullForm<TRes> get fullForm;
}

class _CopyWithImpl$Mutation$finishRegistration<TRes>
    implements CopyWith$Mutation$finishRegistration<TRes> {
  _CopyWithImpl$Mutation$finishRegistration(
    this._instance,
    this._then,
  );

  final Mutation$finishRegistration _instance;

  final TRes Function(Mutation$finishRegistration) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? fullForm = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$finishRegistration(
        fullForm: fullForm == _undefined || fullForm == null
            ? _instance.fullForm
            : (fullForm as Mutation$finishRegistration$fullForm),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Mutation$finishRegistration$fullForm<TRes> get fullForm {
    final local$fullForm = _instance.fullForm;
    return CopyWith$Mutation$finishRegistration$fullForm(
        local$fullForm, (e) => call(fullForm: e));
  }
}

class _CopyWithStubImpl$Mutation$finishRegistration<TRes>
    implements CopyWith$Mutation$finishRegistration<TRes> {
  _CopyWithStubImpl$Mutation$finishRegistration(this._res);

  TRes _res;

  call({
    Mutation$finishRegistration$fullForm? fullForm,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Mutation$finishRegistration$fullForm<TRes> get fullForm =>
      CopyWith$Mutation$finishRegistration$fullForm.stub(_res);
}

const documentNodeMutationfinishRegistration = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'finishRegistration'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'data')),
        type: NamedTypeNode(
          name: NameNode(value: 'UserCreateInput'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'password')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'fullForm'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'data'),
            value: VariableNode(name: NameNode(value: 'data')),
          ),
          ArgumentNode(
            name: NameNode(value: 'password'),
            value: VariableNode(name: NameNode(value: 'password')),
          ),
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'token'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                name: NameNode(value: 'AuthTokenFragment'),
                directives: [],
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: 'user'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                name: NameNode(value: 'UserFragment'),
                directives: [],
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionAuthTokenFragment,
  fragmentDefinitionUserFragment,
]);
Mutation$finishRegistration _parserFn$Mutation$finishRegistration(
        Map<String, dynamic> data) =>
    Mutation$finishRegistration.fromJson(data);
typedef OnMutationCompleted$Mutation$finishRegistration = FutureOr<void>
    Function(
  Map<String, dynamic>?,
  Mutation$finishRegistration?,
);

class Options$Mutation$finishRegistration
    extends graphql.MutationOptions<Mutation$finishRegistration> {
  Options$Mutation$finishRegistration({
    String? operationName,
    required Variables$Mutation$finishRegistration variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$finishRegistration? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$finishRegistration? onCompleted,
    graphql.OnMutationUpdate<Mutation$finishRegistration>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null
                        ? null
                        : _parserFn$Mutation$finishRegistration(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationfinishRegistration,
          parserFn: _parserFn$Mutation$finishRegistration,
        );

  final OnMutationCompleted$Mutation$finishRegistration? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$finishRegistration
    extends graphql.WatchQueryOptions<Mutation$finishRegistration> {
  WatchOptions$Mutation$finishRegistration({
    String? operationName,
    required Variables$Mutation$finishRegistration variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$finishRegistration? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationfinishRegistration,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$finishRegistration,
        );
}

extension ClientExtension$Mutation$finishRegistration on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$finishRegistration>>
      mutate$finishRegistration(
              Options$Mutation$finishRegistration options) async =>
          await this.mutate(options);
  graphql.ObservableQuery<Mutation$finishRegistration>
      watchMutation$finishRegistration(
              WatchOptions$Mutation$finishRegistration options) =>
          this.watchMutation(options);
}

class Mutation$finishRegistration$fullForm {
  Mutation$finishRegistration$fullForm({
    required this.token,
    required this.user,
    this.$__typename = 'UserAuthModel',
  });

  factory Mutation$finishRegistration$fullForm.fromJson(
      Map<String, dynamic> json) {
    final l$token = json['token'];
    final l$user = json['user'];
    final l$$__typename = json['__typename'];
    return Mutation$finishRegistration$fullForm(
      token: Fragment$AuthTokenFragment.fromJson(
          (l$token as Map<String, dynamic>)),
      user: Fragment$UserFragment.fromJson((l$user as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$AuthTokenFragment token;

  final Fragment$UserFragment user;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$token = token;
    _resultData['token'] = l$token.toJson();
    final l$user = user;
    _resultData['user'] = l$user.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$token = token;
    final l$user = user;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$token,
      l$user,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$finishRegistration$fullForm) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$token = token;
    final lOther$token = other.token;
    if (l$token != lOther$token) {
      return false;
    }
    final l$user = user;
    final lOther$user = other.user;
    if (l$user != lOther$user) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$finishRegistration$fullForm
    on Mutation$finishRegistration$fullForm {
  CopyWith$Mutation$finishRegistration$fullForm<
          Mutation$finishRegistration$fullForm>
      get copyWith => CopyWith$Mutation$finishRegistration$fullForm(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Mutation$finishRegistration$fullForm<TRes> {
  factory CopyWith$Mutation$finishRegistration$fullForm(
    Mutation$finishRegistration$fullForm instance,
    TRes Function(Mutation$finishRegistration$fullForm) then,
  ) = _CopyWithImpl$Mutation$finishRegistration$fullForm;

  factory CopyWith$Mutation$finishRegistration$fullForm.stub(TRes res) =
      _CopyWithStubImpl$Mutation$finishRegistration$fullForm;

  TRes call({
    Fragment$AuthTokenFragment? token,
    Fragment$UserFragment? user,
    String? $__typename,
  });
  CopyWith$Fragment$AuthTokenFragment<TRes> get token;
  CopyWith$Fragment$UserFragment<TRes> get user;
}

class _CopyWithImpl$Mutation$finishRegistration$fullForm<TRes>
    implements CopyWith$Mutation$finishRegistration$fullForm<TRes> {
  _CopyWithImpl$Mutation$finishRegistration$fullForm(
    this._instance,
    this._then,
  );

  final Mutation$finishRegistration$fullForm _instance;

  final TRes Function(Mutation$finishRegistration$fullForm) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? token = _undefined,
    Object? user = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$finishRegistration$fullForm(
        token: token == _undefined || token == null
            ? _instance.token
            : (token as Fragment$AuthTokenFragment),
        user: user == _undefined || user == null
            ? _instance.user
            : (user as Fragment$UserFragment),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Fragment$AuthTokenFragment<TRes> get token {
    final local$token = _instance.token;
    return CopyWith$Fragment$AuthTokenFragment(
        local$token, (e) => call(token: e));
  }

  CopyWith$Fragment$UserFragment<TRes> get user {
    final local$user = _instance.user;
    return CopyWith$Fragment$UserFragment(local$user, (e) => call(user: e));
  }
}

class _CopyWithStubImpl$Mutation$finishRegistration$fullForm<TRes>
    implements CopyWith$Mutation$finishRegistration$fullForm<TRes> {
  _CopyWithStubImpl$Mutation$finishRegistration$fullForm(this._res);

  TRes _res;

  call({
    Fragment$AuthTokenFragment? token,
    Fragment$UserFragment? user,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Fragment$AuthTokenFragment<TRes> get token =>
      CopyWith$Fragment$AuthTokenFragment.stub(_res);
  CopyWith$Fragment$UserFragment<TRes> get user =>
      CopyWith$Fragment$UserFragment.stub(_res);
}

class Variables$Mutation$signIn {
  factory Variables$Mutation$signIn({
    required String uniqueNumber,
    required String password,
  }) =>
      Variables$Mutation$signIn._({
        r'uniqueNumber': uniqueNumber,
        r'password': password,
      });

  Variables$Mutation$signIn._(this._$data);

  factory Variables$Mutation$signIn.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$uniqueNumber = data['uniqueNumber'];
    result$data['uniqueNumber'] = (l$uniqueNumber as String);
    final l$password = data['password'];
    result$data['password'] = (l$password as String);
    return Variables$Mutation$signIn._(result$data);
  }

  Map<String, dynamic> _$data;

  String get uniqueNumber => (_$data['uniqueNumber'] as String);
  String get password => (_$data['password'] as String);
  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$uniqueNumber = uniqueNumber;
    result$data['uniqueNumber'] = l$uniqueNumber;
    final l$password = password;
    result$data['password'] = l$password;
    return result$data;
  }

  CopyWith$Variables$Mutation$signIn<Variables$Mutation$signIn> get copyWith =>
      CopyWith$Variables$Mutation$signIn(
        this,
        (i) => i,
      );
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$signIn) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$uniqueNumber = uniqueNumber;
    final lOther$uniqueNumber = other.uniqueNumber;
    if (l$uniqueNumber != lOther$uniqueNumber) {
      return false;
    }
    final l$password = password;
    final lOther$password = other.password;
    if (l$password != lOther$password) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$uniqueNumber = uniqueNumber;
    final l$password = password;
    return Object.hashAll([
      l$uniqueNumber,
      l$password,
    ]);
  }
}

abstract class CopyWith$Variables$Mutation$signIn<TRes> {
  factory CopyWith$Variables$Mutation$signIn(
    Variables$Mutation$signIn instance,
    TRes Function(Variables$Mutation$signIn) then,
  ) = _CopyWithImpl$Variables$Mutation$signIn;

  factory CopyWith$Variables$Mutation$signIn.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$signIn;

  TRes call({
    String? uniqueNumber,
    String? password,
  });
}

class _CopyWithImpl$Variables$Mutation$signIn<TRes>
    implements CopyWith$Variables$Mutation$signIn<TRes> {
  _CopyWithImpl$Variables$Mutation$signIn(
    this._instance,
    this._then,
  );

  final Variables$Mutation$signIn _instance;

  final TRes Function(Variables$Mutation$signIn) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? uniqueNumber = _undefined,
    Object? password = _undefined,
  }) =>
      _then(Variables$Mutation$signIn._({
        ..._instance._$data,
        if (uniqueNumber != _undefined && uniqueNumber != null)
          'uniqueNumber': (uniqueNumber as String),
        if (password != _undefined && password != null)
          'password': (password as String),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$signIn<TRes>
    implements CopyWith$Variables$Mutation$signIn<TRes> {
  _CopyWithStubImpl$Variables$Mutation$signIn(this._res);

  TRes _res;

  call({
    String? uniqueNumber,
    String? password,
  }) =>
      _res;
}

class Mutation$signIn {
  Mutation$signIn({
    required this.login,
    this.$__typename = 'Mutation',
  });

  factory Mutation$signIn.fromJson(Map<String, dynamic> json) {
    final l$login = json['login'];
    final l$$__typename = json['__typename'];
    return Mutation$signIn(
      login: Mutation$signIn$login.fromJson((l$login as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Mutation$signIn$login login;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$login = login;
    _resultData['login'] = l$login.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$login = login;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$login,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$signIn) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$login = login;
    final lOther$login = other.login;
    if (l$login != lOther$login) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$signIn on Mutation$signIn {
  CopyWith$Mutation$signIn<Mutation$signIn> get copyWith =>
      CopyWith$Mutation$signIn(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$signIn<TRes> {
  factory CopyWith$Mutation$signIn(
    Mutation$signIn instance,
    TRes Function(Mutation$signIn) then,
  ) = _CopyWithImpl$Mutation$signIn;

  factory CopyWith$Mutation$signIn.stub(TRes res) =
      _CopyWithStubImpl$Mutation$signIn;

  TRes call({
    Mutation$signIn$login? login,
    String? $__typename,
  });
  CopyWith$Mutation$signIn$login<TRes> get login;
}

class _CopyWithImpl$Mutation$signIn<TRes>
    implements CopyWith$Mutation$signIn<TRes> {
  _CopyWithImpl$Mutation$signIn(
    this._instance,
    this._then,
  );

  final Mutation$signIn _instance;

  final TRes Function(Mutation$signIn) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? login = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$signIn(
        login: login == _undefined || login == null
            ? _instance.login
            : (login as Mutation$signIn$login),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Mutation$signIn$login<TRes> get login {
    final local$login = _instance.login;
    return CopyWith$Mutation$signIn$login(local$login, (e) => call(login: e));
  }
}

class _CopyWithStubImpl$Mutation$signIn<TRes>
    implements CopyWith$Mutation$signIn<TRes> {
  _CopyWithStubImpl$Mutation$signIn(this._res);

  TRes _res;

  call({
    Mutation$signIn$login? login,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Mutation$signIn$login<TRes> get login =>
      CopyWith$Mutation$signIn$login.stub(_res);
}

const documentNodeMutationsignIn = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'signIn'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'uniqueNumber')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'password')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'login'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'uniqueNumber'),
            value: VariableNode(name: NameNode(value: 'uniqueNumber')),
          ),
          ArgumentNode(
            name: NameNode(value: 'password'),
            value: VariableNode(name: NameNode(value: 'password')),
          ),
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'token'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                name: NameNode(value: 'AuthTokenFragment'),
                directives: [],
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: 'user'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                name: NameNode(value: 'UserFragment'),
                directives: [],
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionAuthTokenFragment,
  fragmentDefinitionUserFragment,
]);
Mutation$signIn _parserFn$Mutation$signIn(Map<String, dynamic> data) =>
    Mutation$signIn.fromJson(data);
typedef OnMutationCompleted$Mutation$signIn = FutureOr<void> Function(
  Map<String, dynamic>?,
  Mutation$signIn?,
);

class Options$Mutation$signIn extends graphql.MutationOptions<Mutation$signIn> {
  Options$Mutation$signIn({
    String? operationName,
    required Variables$Mutation$signIn variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$signIn? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$signIn? onCompleted,
    graphql.OnMutationUpdate<Mutation$signIn>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null ? null : _parserFn$Mutation$signIn(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationsignIn,
          parserFn: _parserFn$Mutation$signIn,
        );

  final OnMutationCompleted$Mutation$signIn? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$signIn
    extends graphql.WatchQueryOptions<Mutation$signIn> {
  WatchOptions$Mutation$signIn({
    String? operationName,
    required Variables$Mutation$signIn variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$signIn? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationsignIn,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$signIn,
        );
}

extension ClientExtension$Mutation$signIn on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$signIn>> mutate$signIn(
          Options$Mutation$signIn options) async =>
      await this.mutate(options);
  graphql.ObservableQuery<Mutation$signIn> watchMutation$signIn(
          WatchOptions$Mutation$signIn options) =>
      this.watchMutation(options);
}

class Mutation$signIn$login {
  Mutation$signIn$login({
    required this.token,
    required this.user,
    this.$__typename = 'UserAuthModel',
  });

  factory Mutation$signIn$login.fromJson(Map<String, dynamic> json) {
    final l$token = json['token'];
    final l$user = json['user'];
    final l$$__typename = json['__typename'];
    return Mutation$signIn$login(
      token: Fragment$AuthTokenFragment.fromJson(
          (l$token as Map<String, dynamic>)),
      user: Fragment$UserFragment.fromJson((l$user as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$AuthTokenFragment token;

  final Fragment$UserFragment user;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$token = token;
    _resultData['token'] = l$token.toJson();
    final l$user = user;
    _resultData['user'] = l$user.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$token = token;
    final l$user = user;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$token,
      l$user,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$signIn$login) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$token = token;
    final lOther$token = other.token;
    if (l$token != lOther$token) {
      return false;
    }
    final l$user = user;
    final lOther$user = other.user;
    if (l$user != lOther$user) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$signIn$login on Mutation$signIn$login {
  CopyWith$Mutation$signIn$login<Mutation$signIn$login> get copyWith =>
      CopyWith$Mutation$signIn$login(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$signIn$login<TRes> {
  factory CopyWith$Mutation$signIn$login(
    Mutation$signIn$login instance,
    TRes Function(Mutation$signIn$login) then,
  ) = _CopyWithImpl$Mutation$signIn$login;

  factory CopyWith$Mutation$signIn$login.stub(TRes res) =
      _CopyWithStubImpl$Mutation$signIn$login;

  TRes call({
    Fragment$AuthTokenFragment? token,
    Fragment$UserFragment? user,
    String? $__typename,
  });
  CopyWith$Fragment$AuthTokenFragment<TRes> get token;
  CopyWith$Fragment$UserFragment<TRes> get user;
}

class _CopyWithImpl$Mutation$signIn$login<TRes>
    implements CopyWith$Mutation$signIn$login<TRes> {
  _CopyWithImpl$Mutation$signIn$login(
    this._instance,
    this._then,
  );

  final Mutation$signIn$login _instance;

  final TRes Function(Mutation$signIn$login) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? token = _undefined,
    Object? user = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$signIn$login(
        token: token == _undefined || token == null
            ? _instance.token
            : (token as Fragment$AuthTokenFragment),
        user: user == _undefined || user == null
            ? _instance.user
            : (user as Fragment$UserFragment),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Fragment$AuthTokenFragment<TRes> get token {
    final local$token = _instance.token;
    return CopyWith$Fragment$AuthTokenFragment(
        local$token, (e) => call(token: e));
  }

  CopyWith$Fragment$UserFragment<TRes> get user {
    final local$user = _instance.user;
    return CopyWith$Fragment$UserFragment(local$user, (e) => call(user: e));
  }
}

class _CopyWithStubImpl$Mutation$signIn$login<TRes>
    implements CopyWith$Mutation$signIn$login<TRes> {
  _CopyWithStubImpl$Mutation$signIn$login(this._res);

  TRes _res;

  call({
    Fragment$AuthTokenFragment? token,
    Fragment$UserFragment? user,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Fragment$AuthTokenFragment<TRes> get token =>
      CopyWith$Fragment$AuthTokenFragment.stub(_res);
  CopyWith$Fragment$UserFragment<TRes> get user =>
      CopyWith$Fragment$UserFragment.stub(_res);
}

class Query$getUser {
  Query$getUser({
    required this.getUserData,
    this.$__typename = 'Query',
  });

  factory Query$getUser.fromJson(Map<String, dynamic> json) {
    final l$getUserData = json['getUserData'];
    final l$$__typename = json['__typename'];
    return Query$getUser(
      getUserData: Fragment$UserFragment.fromJson(
          (l$getUserData as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Fragment$UserFragment getUserData;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$getUserData = getUserData;
    _resultData['getUserData'] = l$getUserData.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$getUserData = getUserData;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$getUserData,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$getUser) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$getUserData = getUserData;
    final lOther$getUserData = other.getUserData;
    if (l$getUserData != lOther$getUserData) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$getUser on Query$getUser {
  CopyWith$Query$getUser<Query$getUser> get copyWith => CopyWith$Query$getUser(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$getUser<TRes> {
  factory CopyWith$Query$getUser(
    Query$getUser instance,
    TRes Function(Query$getUser) then,
  ) = _CopyWithImpl$Query$getUser;

  factory CopyWith$Query$getUser.stub(TRes res) =
      _CopyWithStubImpl$Query$getUser;

  TRes call({
    Fragment$UserFragment? getUserData,
    String? $__typename,
  });
  CopyWith$Fragment$UserFragment<TRes> get getUserData;
}

class _CopyWithImpl$Query$getUser<TRes>
    implements CopyWith$Query$getUser<TRes> {
  _CopyWithImpl$Query$getUser(
    this._instance,
    this._then,
  );

  final Query$getUser _instance;

  final TRes Function(Query$getUser) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? getUserData = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$getUser(
        getUserData: getUserData == _undefined || getUserData == null
            ? _instance.getUserData
            : (getUserData as Fragment$UserFragment),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Fragment$UserFragment<TRes> get getUserData {
    final local$getUserData = _instance.getUserData;
    return CopyWith$Fragment$UserFragment(
        local$getUserData, (e) => call(getUserData: e));
  }
}

class _CopyWithStubImpl$Query$getUser<TRes>
    implements CopyWith$Query$getUser<TRes> {
  _CopyWithStubImpl$Query$getUser(this._res);

  TRes _res;

  call({
    Fragment$UserFragment? getUserData,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Fragment$UserFragment<TRes> get getUserData =>
      CopyWith$Fragment$UserFragment.stub(_res);
}

const documentNodeQuerygetUser = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'getUser'),
    variableDefinitions: [],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'getUserData'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FragmentSpreadNode(
            name: NameNode(value: 'UserFragment'),
            directives: [],
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
  fragmentDefinitionUserFragment,
]);
Query$getUser _parserFn$Query$getUser(Map<String, dynamic> data) =>
    Query$getUser.fromJson(data);
typedef OnQueryComplete$Query$getUser = FutureOr<void> Function(
  Map<String, dynamic>?,
  Query$getUser?,
);

class Options$Query$getUser extends graphql.QueryOptions<Query$getUser> {
  Options$Query$getUser({
    String? operationName,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$getUser? typedOptimisticResult,
    Duration? pollInterval,
    graphql.Context? context,
    OnQueryComplete$Query$getUser? onComplete,
    graphql.OnQueryError? onError,
  })  : onCompleteWithParsed = onComplete,
        super(
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          pollInterval: pollInterval,
          context: context,
          onComplete: onComplete == null
              ? null
              : (data) => onComplete(
                    data,
                    data == null ? null : _parserFn$Query$getUser(data),
                  ),
          onError: onError,
          document: documentNodeQuerygetUser,
          parserFn: _parserFn$Query$getUser,
        );

  final OnQueryComplete$Query$getUser? onCompleteWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onComplete == null
            ? super.properties
            : super.properties.where((property) => property != onComplete),
        onCompleteWithParsed,
      ];
}

class WatchOptions$Query$getUser
    extends graphql.WatchQueryOptions<Query$getUser> {
  WatchOptions$Query$getUser({
    String? operationName,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$getUser? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeQuerygetUser,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Query$getUser,
        );
}

class FetchMoreOptions$Query$getUser extends graphql.FetchMoreOptions {
  FetchMoreOptions$Query$getUser({required graphql.UpdateQuery updateQuery})
      : super(
          updateQuery: updateQuery,
          document: documentNodeQuerygetUser,
        );
}

extension ClientExtension$Query$getUser on graphql.GraphQLClient {
  Future<graphql.QueryResult<Query$getUser>> query$getUser(
          [Options$Query$getUser? options]) async =>
      await this.query(options ?? Options$Query$getUser());
  graphql.ObservableQuery<Query$getUser> watchQuery$getUser(
          [WatchOptions$Query$getUser? options]) =>
      this.watchQuery(options ?? WatchOptions$Query$getUser());
  void writeQuery$getUser({
    required Query$getUser data,
    bool broadcast = true,
  }) =>
      this.writeQuery(
        graphql.Request(
            operation: graphql.Operation(document: documentNodeQuerygetUser)),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Query$getUser? readQuery$getUser({bool optimistic = true}) {
    final result = this.readQuery(
      graphql.Request(
          operation: graphql.Operation(document: documentNodeQuerygetUser)),
      optimistic: optimistic,
    );
    return result == null ? null : Query$getUser.fromJson(result);
  }
}
