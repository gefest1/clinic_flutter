import 'package:clinic/main.dart';
import 'package:graphql/client.dart';

// ignore: unused_import
import 'graphql/scope2/__generated/schema.graphql.dart';
// ignore: unused_import
import 'graphql/scope2/__generated/request.graphql.dart';

// ignore: unused_import
export 'graphql/scope2/__generated/schema.graphql.dart';
// ignore: unused_import
export 'graphql/scope2/__generated/request.graphql.dart';

GraphQLClient getGraphLink2() {
  final AuthLink authLink = AuthLink(
    getToken: () async {
      final token = sharedBox.get('accessToken');

      return token;
    },
  );
  final uri = Uri.parse(httpUrlPath2);

  final Link link = Link.split(
    (request) {
      return request.isSubscription;
    },
    WebSocketLink(
      () {
        final url = uri.replace(scheme: 'wss', port: null).toString();

        return url;
      }(),
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: const Duration(seconds: 30),
        initialPayload: () async {
          final token = sharedBox.get('accessToken');
          // sharedBox.get('token') ?? fastToken;
          return {
            'Authorization': token,
          };
        },
      ),
    ),
    authLink.concat(HttpLink(uri.toString())),
  );
  return GraphQLClient(
    link: link,
    cache: GraphQLCache(),
  );
}

GraphQLClient tundukGraphqlClient = getGraphLink2();

const httpUrlPath2 = "http://176.126.166.233:8080/v1/graphql";
