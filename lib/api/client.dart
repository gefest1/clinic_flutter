import 'package:clinic/main.dart';
import 'package:graphql/client.dart';

// ignore: unused_import
import 'graphql/scope1/__generated/schema.graphql.dart';
// ignore: unused_import
import 'graphql/scope1/__generated/request.graphql.dart';

export 'graphql/scope1/__generated/schema.graphql.dart';
export 'graphql/scope1/__generated/request.graphql.dart';

GraphQLClient getGraphLink() {
  final AuthLink authLink = AuthLink(
    getToken: () async {
      final token = sharedBox.get('accessToken');

      return token;
    },
  );
  final uri = Uri.parse('$httpUrlPath/graphql');

  final Link link = Link.split(
    (request) {
      return request.isSubscription;
    },
    WebSocketLink(
      () {
        final url = uri.replace(scheme: 'wss', port: null).toString();

        return url;
      }(),
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: const Duration(seconds: 30),
        initialPayload: () async {
          final token = sharedBox.get('accessToken');
          // sharedBox.get('token') ?? fastToken;
          return {
            'Authorization': token,
          };
        },
      ),
    ),
    authLink.concat(HttpLink(uri.toString())),
  );
  return GraphQLClient(
    link: link,
    cache: GraphQLCache(),
  );
}

GraphQLClient graphqlClient = getGraphLink();

const httpUrlPath = "http://104.248.25.76:3000";
