import 'dart:developer';

import 'package:clinic/api/client1.dart';
import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/logic/tunduk/doctor/doctor_bloc.dart';
import 'package:clinic/logic/tunduk/doctor/doctor_event.dart';
import 'package:clinic/logic/tunduk/doctor/doctor_state.dart';
import 'package:clinic/logic/tunduk/organization/organization_bloc.dart';
import 'package:clinic/logic/tunduk/organization/organization_event.dart';
import 'package:clinic/logic/tunduk/organization/organization_state.dart';
import 'package:clinic/pages/hospital/doctors_page.dart';
import 'package:clinic/utils/nested_logic_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HospitalPage extends StatefulWidget {
  const HospitalPage({super.key});

  @override
  State<HospitalPage> createState() => _HospitalPageState();
}

class _HospitalPageState extends State<HospitalPage> {
  @override
  void initState() {
    context.read<OrganizationBloc>().add(
          const GetOrganizationEvent(),
        );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrganizationBloc, OrganizationState>(
      builder: (context, state) {
        if (state is LoadingOrganizationState) {
          return Scaffold(
            floatingActionButton: FloatingActionButton(onPressed: () {
              context.read<OrganizationBloc>().add(
                    const GetOrganizationEvent(),
                  );
            }),
            body: const Center(
              child: CircularProgressIndicator.adaptive(),
            ),
          );
        }
        final dataState = state as DataOrganizationState;

        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              DoctorBloc.getDoctorBloc(
                dataState.data.organisationId.toString(),
              ).add(const GetDoctorEvent());

              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (__) => NestedLogicRoute(
              //       child: Scaffold(
              //         appBar: AppBar(
              //           leading: IconButton(
              //               onPressed: () => Navigator.pop(__),
              //               icon: const Icon(Icons.chevron_left)),
              //         ),
              //       ),
              //     ),
              //   ),
              // );
            },
          ),
          appBar: AppBar(
            title: Text(dataState.data.organisationNameCode ?? ' '),
          ),
          body: CustomScrollView(
            slivers: [
              BlocBuilder<DoctorBloc, DoctorState>(
                bloc: DoctorBloc.getDoctorBloc(
                  dataState.data.organisationId.toString(),
                ),
                builder: (context, state) {
                  if (state is LoadingDoctorState) {
                    return const SliverToBoxAdapter();
                  }
                  final doctors = <Fragment$DoctorResult>[];
                  if (state is DataDoctorState) {
                    doctors.addAll(state.data);
                  }
                  final Map<int, List<Fragment$DoctorResult>> doctorType = {};
                  final Map<int, String> doctorTypeNames = {};
                  for (final val in doctors) {
                    if (val.dictDoljnostId == null) continue;
                    (doctorType[val.dictDoljnostId!] ??= []).add(val);
                    doctorTypeNames[val.dictDoljnostId!] =
                        val.dictDoljnostName ?? '';
                  }
                  final entries = doctorTypeNames.entries.toList();
                  return SliverList.builder(
                    itemCount: entries.length,
                    itemBuilder: (context, index) {
                      return SizeTapAnimation(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DoctorsPage(
                                title: entries[index].value,
                                doctors: doctorType[entries[index].key] ?? [],
                              ),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 12),
                          child: Text(entries[index].value),
                        ),
                      );
                    },
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
