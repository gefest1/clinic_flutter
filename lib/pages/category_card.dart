// import 'package:clinic_mobile/common/atoms/svg_local_widget.dart';
// import 'package:clinic_mobile/common/atoms/text.dart';
// import 'package:clinic_mobile/common/molecules/size_tap_animation.dart';
// import 'package:clinic_mobile/common/tools/color_data.dart';
import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/pages/svg_local_widget.dart';
import 'package:flutter/material.dart';

class CategoryCard extends StatefulWidget {
  final String title;
  final String svgLocal;
  final VoidCallback? onTap;

  const CategoryCard({
    required this.title,
    required this.svgLocal,
    this.onTap,
    super.key,
  });

  @override
  State<CategoryCard> createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: widget.onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 48,
            width: 48,
            child: SvgLocalWidget(assetPath: widget.svgLocal),
          ),
          const SizedBox(height: 10),
          ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: 40,
              maxWidth: 124,
              maxHeight: 32,
              minHeight: 32,
            ),
            child: Center(
              child: Text(
                widget.title,
                textAlign: TextAlign.center,
              ),
              // CustomText(
              //   widget.title,
              //   color: ColorData.basicColorDarkBlue,
              //   type: CustomTextType.tips,
              //   textAlign: TextAlign.center,
              // ),
            ),
          ),
        ],
      ),
    );
  }
}
