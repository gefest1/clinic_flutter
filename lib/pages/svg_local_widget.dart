import 'package:clinic/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:jovial_svg/jovial_svg.dart';

class SvgLocalWidget extends StatelessWidget {
  final double? height;
  final double? width;

  final String assetPath;
  final Color? currentColor;
  final BoxFit fit;

  const SvgLocalWidget({
    this.height,
    this.width,
    required this.assetPath,
    this.currentColor,
    this.fit = BoxFit.cover,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: SizedBox(
        height: height,
        width: width,
        child: ScalableImageWidget.fromSISource(
          si: persistentCache.get(assetPath),
          fit: fit,
          reload: false,
        ),
      ),
    );
  }
}
