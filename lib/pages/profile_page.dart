import 'package:clinic/main.dart';
import 'package:clinic/pages/personal_data_page.dart';
import 'package:clinic/pages/profile_card.dart';
import 'package:clinic/pages/restart_widget.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/text.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: H4Text(
          AppLocalizations.of(context)?.profile ?? "Профиль",
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          top: 10,
          bottom: 100,
        ),
        children: [
          ProfileCard(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const PersonalDataPage(),
                ),
              );
            },
            icon: const Icon(
              Icons.assignment_ind,
              color: ColorData.colorElementsActive,
            ),
            label: AppLocalizations.of(context)?.personal_information ??
                'Личная информация',
          ),
          ProfileCard(
            onTap: () async {
              final loc = MyApp.of(context);
              if (loc == null) return;
              if (loc.locale.languageCode == 'en') {
                loc.setLocale(const Locale('ky'));
              } else if (loc.locale.languageCode == 'ky') {
                loc.setLocale(const Locale('ru'));
              } else {
                loc.setLocale(const Locale('en'));
              }
            },
            icon: const Icon(
              Icons.translate,
              color: ColorData.colorElementsActive,
            ),
            label: AppLocalizations.of(context)?.change_lang ?? 'Сменить язык',
          ),
          ProfileCard(
            onTap: () async {
              await Future.wait([
                sharedBox.delete('accessToken'),
                sharedBox.delete('refreshToken')
              ]);

              // await sharedBox.clear();
              if (!mounted) return;
              RestartWidget.restartApp(context);
            },
            icon: const Icon(
              Icons.meeting_room,
              color: ColorData.colorElementsActive,
            ),
            label: AppLocalizations.of(context)?.log_out ?? 'Выйти из профиля',
          ),
        ],
      ),
    );
  }
}
