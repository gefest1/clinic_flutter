import 'package:clinic/logic/user_bloc.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TextCardProfile extends StatelessWidget {
  final String title;
  final String label;
  const TextCardProfile({
    required this.title,
    required this.label,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        H4Text(title),
        const SizedBox(height: 10),
        P2Text(label),
      ],
    );
  }
}

class PersonalDataPage extends StatefulWidget {
  const PersonalDataPage({super.key});

  @override
  State<PersonalDataPage> createState() => _PersonalDataPageState();
}

class _PersonalDataPageState extends State<PersonalDataPage> {
  bool isInited = false;

  void _init() {
    if (isInited) return;
    isInited = true;
    Provider.of<UserBloc>(context).add(const GetUserEvent());
  }

  @override
  Widget build(BuildContext context) {
    _init();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorData.colorButtonMain,
        title: H4Text(
          AppLocalizations.of(context)?.personal_information ??
              'Персональые данные',
        ),
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          if (state is! DataUserState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          state.uniqueNumber;
          state.firstName;
          state.lastName;
          state.patronymic;
          state.dateBirth;

          return Align(
            alignment: Alignment.centerLeft,
            child: ListView(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, bottom: 100, top: 20),
              children: [
                TextCardProfile(
                  title: AppLocalizations.of(context)?.uniqueNumber ??
                      "uniqueNumber",
                  label: state.uniqueNumber,
                ),
                const SizedBox(height: 20),
                TextCardProfile(
                  title: AppLocalizations.of(context)?.firstName ?? "firstName",
                  label: state.firstName,
                ),
                const SizedBox(height: 20),
                TextCardProfile(
                  title: AppLocalizations.of(context)?.lastName ?? "lastName",
                  label: state.lastName,
                ),
                const SizedBox(height: 20),
                TextCardProfile(
                  title:
                      AppLocalizations.of(context)?.patronymic ?? "patronymic",
                  label: state.patronymic,
                ),
                const SizedBox(height: 20),
                TextCardProfile(
                  title: AppLocalizations.of(context)?.dateBirth ?? "dateBirth",
                  label:
                      DateFormat("dd/MM/yyyy").format(state.dateBirth.toUtc()),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
