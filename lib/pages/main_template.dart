import 'package:clinic/pages/faast_main_page.dart';
import 'package:clinic/pages/main_bottom.dart';
import 'package:clinic/pages/profile_page.dart';
import 'package:flutter/material.dart';

class MainTemplate extends StatefulWidget {
  const MainTemplate({super.key});

  @override
  State<MainTemplate> createState() => _MainTemplateState();
}

class _MainTemplateState extends State<MainTemplate>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController = TabController(
    vsync: this,
    length: 2,
  )..addListener(() {
      if (_tabController.indexIsChanging) {
        setState(() {
          _activeIndex = _tabController.index;
        });
      }
    });
  int _activeIndex = 0;
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: [
              FastMainPage(),
              ProfilePage(),
            ],
          ),
        ),
        MainBottom(
          tabController: _tabController,
          tabIndex: _activeIndex,
        ),
      ],
    );
  }
}
