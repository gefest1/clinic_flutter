import 'package:clinic/common/assets/assets.gen.dart';
import 'package:clinic/components/common/recent_watched.dart';
import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/pages/category_card.dart';
import 'package:clinic/components/common/category_wrap.dart';
import 'package:clinic/pages/svg_local_widget.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:clinic/components/common/text.dart';
import 'package:flutter/services.dart';

final List<String?> images = [
  null, //"https://pulse-backup-kyrg.s3.eu-central-1.amazonaws.com/hell/photo_2023-02-27_15-01-48.jpg",
  null, //'https://pulse-backup-kyrg.s3.eu-central-1.amazonaws.com/hell/photo_2023-02-27_15-02-01.jpg',
  null, //'https://pulse-backup-kyrg.s3.eu-central-1.amazonaws.com/hell/photo_2023-02-27_15-02-04.jpg',
  null, //'https://pulse-backup-kyrg.s3.eu-central-1.amazonaws.com/hell/photo_2023-02-27_15-02-08.jpg',
  null, //'https://pulse-backup-kyrg.s3.eu-central-1.amazonaws.com/hell/photo_2023-02-27_15-02-12.jpg'
];

class FastMainPage extends StatefulWidget {
  const FastMainPage({super.key});

  @override
  State<FastMainPage> createState() => _FastMainPageState();
}

class _FastMainPageState extends State<FastMainPage> {
  @override
  void initState() {
    // Future(() {
    //   context.read<DoctorBloc>().add(const GetDoctorEvent());
    // });
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Assets
    final media = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: const Color(0xffF6F8FA),
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              physics: const ClampingScrollPhysics(),
              slivers: [
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: media.padding.top,
                          bottom: 20,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: const Offset(0, 4),
                              color:
                                  (const Color(0xff2566F9)).withOpacity(0.05),
                              blurRadius: 10,
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                const SizedBox(width: 20),
                                SvgLocalWidget(
                                  assetPath: Assets.svgs.logo.pulseUgly,
                                  height: 40,
                                  width: 150,
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: SizeTapAnimation(
                                      onTap: () {},
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Flexible(
                                            child: CustomText(
                                              AppLocalizations.of(context)
                                                      ?.personal_information ??
                                                  'Личная карта',
                                              type: CustomTextType.h16,
                                              textAlign: TextAlign.end,
                                              color: ColorData.colorBlack,
                                            ),
                                          ),
                                          const SizedBox(width: 8),
                                          SvgLocalWidget(
                                            assetPath: Assets.svgs.profile,
                                            height: 24,
                                            width: 24,
                                          ),
                                          const SizedBox(width: 20),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 24),
                            SizedBox(
                              height: 120,
                              child: ListView.separated(
                                clipBehavior: Clip.none,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                itemCount: images.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) => Container(
                                  width: 120,
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(10),
                                    image: images[index] == null
                                        ? null
                                        : DecorationImage(
                                            image: NetworkImage(images[index]!),
                                            fit: BoxFit.cover,
                                          ),
                                    boxShadow: [
                                      BoxShadow(
                                        color:
                                            Color(0xff2566F9).withOpacity(0.1),
                                        blurRadius: 20,
                                        offset: const Offset(0, 4),
                                      ),
                                    ],
                                  ),
                                ),
                                separatorBuilder: (context, index) =>
                                    const SizedBox(width: 10),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20),
                      const CategoryWrap(),
                      const SizedBox(height: 30),
                      RecentWatchedCard(
                        title: AppLocalizations.of(context)?.recently_watched ??
                            "Recently Watched",
                        photos: [
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-21.jpg',
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-36.jpg',
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-40.jpg'
                        ],
                      ),
                      const SizedBox(height: 30),
                      const RecentWatchedCard(
                        title: "Массаж",
                        photos: [
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-18-19.jpg',
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-18-23.jpg',
                        ],
                      ),
                      const SizedBox(height: 30),
                      const RecentWatchedCard(
                        title: "Иглотерапия",
                        photos: [
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-36.jpg',
                          // 'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-36.jpg',
                          'https://can-space.fra1.cdn.digitaloceanspaces.com/delete_me/photo_2023-05-10_14-09-40.jpg'
                        ],
                      ),
                      const SizedBox(height: 200),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Wrap(
                        children: [],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
