import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/main.dart';
import 'package:clinic/pages/auth/login_page.dart';
import 'package:clinic/pages/auth/register_page.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/text.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/material.dart';

class InitAuthPage extends StatefulWidget {
  const InitAuthPage({super.key});

  @override
  State<InitAuthPage> createState() => _InitAuthPageState();
}

class _InitAuthPageState extends State<InitAuthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorData.colorMainElements,
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Expanded(
                    child: Align(
                      alignment: Alignment(0, -0.6),
                      child: Text(
                        'Pulse',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 72,
                          fontWeight: H1TextStyle.fontWeight,
                          height: H1TextStyle.height,
                        ),
                        // color: ColorData.colorWhiteBackground,
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 40, left: 20, right: 20),
                    child: Column(
                      children: [
                        CustomButton(
                          color: const Color(0xff5185fa),
                          title: AppLocalizations.of(context)?.enter ?? 'enter',
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const LoginPage(),
                              ),
                            );
                          },
                        ),
                        const SizedBox(height: 10),
                        CustomButton(
                          color: Colors.transparent,
                          titleColor: Colors.white,
                          boxShadow: [],
                          title: AppLocalizations.of(context)?.register ??
                              'register',
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const RegisterPage(),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(height: 1),
                ],
              ),
              Positioned(
                right: 16,
                top: 16,
                child: SizeTapAnimation(
                  onTap: () {
                    final loc = MyApp.of(context);
                    if (loc == null) return;
                    if (loc.locale.languageCode == 'en') {
                      loc.setLocale(const Locale('ky'));
                    } else if (loc.locale.languageCode == 'ky') {
                      loc.setLocale(const Locale('ru'));
                    } else {
                      loc.setLocale(const Locale('en'));
                    }
                  },
                  child: Column(
                    children: [
                      const Icon(
                        Icons.translate,
                        color: Colors.white,
                      ),
                      () {
                        return P0Text((MyApp.of(context)?.locale).toString());
                      }(),
                    ],
                  ),
                ),
                // ProfileCard(
                //   onTap: () async {
                //     final loc = MyApp.of(context);
                //     if (loc == null) return;
                //     if (loc.locale.languageCode == 'en') {
                //       loc.setLocale(const Locale('ky'));
                //     } else {
                //       loc.setLocale(const Locale('en'));
                //     }
                //   },
                //   icon: const Icon(
                //     Icons.translate,
                //     color: ColorData.colorElementsActive,
                //   ),
                //   label: AppLocalizations.of(context)?.change_lang ??
                //       'Сменить язык',
                // ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
