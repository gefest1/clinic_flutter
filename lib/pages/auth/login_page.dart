import 'dart:async';
import 'dart:developer';

import 'package:clinic/api/client.dart';
import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/custom_text_field.dart';
import 'package:clinic/main.dart';
import 'package:clinic/pages/restart_widget.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/load_pop_up_route.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late final uniqueController = TextEditingController()
    ..addListener(() {
      activeSub.add(null);
    });
  late final passwordController = TextEditingController()
    ..addListener(() {
      activeSub.add(null);
    });
  final activeSub = BehaviorSubject.seeded(null);

  @override
  void dispose() {
    uniqueController.dispose();
    passwordController.dispose();
    activeSub.close();
    super.dispose();
  }

  bool isValid() {
    return uniqueController.text.length == 14 &&
        passwordController.text.length > 6;
  }

  void login() async {
    final compl = Completer();
    Navigator.push(context, LoadPopupRoute(completer: compl));
    try {
      final responce = await graphqlClient.mutate$signIn(
        Options$Mutation$signIn(
          variables: Variables$Mutation$signIn(
            uniqueNumber: uniqueController.text,
            password: passwordController.text,
          ),
        ),
      );
      if (responce.hasException) throw responce.exception!;
      final data = responce.parsedData!;
      final token = data.login.token;

      await Future.wait([
        sharedBox.put('accessToken', token.accessToken),
        sharedBox.put('refreshToken', token.refreshToken),
      ]);
      if (!mounted) return;
      RestartWidget.restartApp(context);
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
    compl.complete();
  }

  @override
  Widget build(BuildContext context) {
    final med = MediaQuery.of(context);

    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: med.padding.top + 100),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)?.enter ?? 'Enter',
                    style: const TextStyle(
                      color: ColorData.colorMainElements,
                      fontSize: 72,
                      fontWeight: H1TextStyle.fontWeight,
                      height: H1TextStyle.height,
                    ),
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomTextField(
                        title:
                            AppLocalizations.of(context)?.your_unique_number ??
                                "your_unique_number",
                        hintText: '00000000000000',
                        controller: uniqueController,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(14),
                        ],
                        keyboardType: TextInputType.number,
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        title: AppLocalizations.of(context)?.your_password ??
                            "your_password",
                        hintText: '******',
                        controller: passwordController,
                      ),
                      const SizedBox(height: 40),
                      StreamBuilder(
                          stream: activeSub,
                          builder: (context, snapshot) {
                            return CustomButton(
                              active: isValid(),
                              title: AppLocalizations.of(context)?.enter ??
                                  "enter",
                              onTap: login,
                            );
                          }),
                      SizedBox(height: med.padding.bottom + 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
