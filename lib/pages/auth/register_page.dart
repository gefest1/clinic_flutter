import 'dart:async';
import 'dart:developer';
import 'dart:io' as io;

import 'package:camera/camera.dart';
import 'package:clinic/api/client.dart';
import 'package:clinic/face_full_camera/camera_page.dart';
import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/custom_text_field.dart';
import 'package:clinic/face_full_camera/face_camera.dart';
import 'package:clinic/logic/register_logic.dart';
import 'package:clinic/main.dart';
import 'package:clinic/pages/auth/face_validate.dart';
import 'package:clinic/pages/auth/preview_page.dart';
import 'package:clinic/pages/auth/wb_verification_page.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/error_overlay.dart';
import 'package:clinic/utils/load_pop_up_route.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphql/client.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'dart:math' as math;
import 'package:image/image.dart' as img;

// const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
// math.Random _rnd = math.Random();

// String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
//     length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

Future<MultipartFile> convertImageToMultiPartFile(img.Image image,
    {String? name}) async {
  final jpg = await compute<img.Image, List<int>>(img.encodeJpg, image);

  return MultipartFile.fromBytes(
    '',
    jpg,
    filename: name,
  );
}

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final registerController = TextEditingController();
  final phoneController = TextEditingController();
  final sbj = BehaviorSubject.seeded(null);

  bool isValid() {
    return registerController.text.length == 14;
  }

  @override
  void initState() {
    registerController.addListener(() {
      sbj.add(null);
    });
    super.initState();
  }

  @override
  void dispose() {
    registerController.dispose();
    sbj.close();
    super.dispose();
  }

  Future<void> onTap() async {
    log("ALl okay 0");
    final responce = await graphqlClient.query$isRegistered(
      Options$Query$isRegistered(
        variables: Variables$Query$isRegistered(
          isRegistered: registerController.text,
        ),
        fetchPolicy: FetchPolicy.networkOnly,
      ),
    );
    if (responce.hasException) throw responce.exception!;
    if (responce.parsedData?.isRegistered != false) {
      throw "Already registered";
    }

    if (!mounted) return;
    //
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const PreviewPage(),
      ),
    );

    //
    log("ALl okay 1");
    final files = await Navigator.push<List<img.Image>>(
      context,
      MaterialPageRoute(
        builder: (context) => const FaceValidate(),
      ),
    );
    if (files == null) return;
    log("START UPLOAD");
    final res = await graphqlClient.mutate$CheckBio(
      Options$Mutation$CheckBio(
        fetchPolicy: FetchPolicy.networkOnly,
        variables: Variables$Mutation$CheckBio(
          pipelines: await Future.wait(
            files
                .asMap()
                .entries
                .map(
                  (e) => convertImageToMultiPartFile(
                    e.value,
                    name: "${e.key}.jpeg",
                  ),
                )
                .toList(),
          ),
          uniqueNumber: registerController.text,
        ),
      ),
    );

    log('FINISH UPLOAD');
    if (res.hasException) throw res.exception!;
    final checkBio = res.parsedData?.checkBio;
    if (checkBio == null) return;

    if (!mounted) return;
    final rgLogic = context.read<RegisterLogic>();
    rgLogic.setUniqueNumber(registerController.text);
    rgLogic.setFirstName(checkBio.firstName);
    rgLogic.setLastName(checkBio.lastName);
    rgLogic.setPatronymic(checkBio.patronymic);
    rgLogic.setDateBirn(checkBio.dateBirth);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const WBVerificationPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final med = MediaQuery.of(context);

    // log(math.sin(30 * (math.pi / 180)).toString());
    // log((math.sqrt(math.pow(off.dx, 2) + math.pow(off.dy, 2))).toString());
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: med.padding.top + 100),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)?.register ?? 'register',
                    style: const TextStyle(
                      color: ColorData.colorMainElements,
                      fontSize: 42,
                      fontWeight: H1TextStyle.fontWeight,
                      height: H1TextStyle.height,
                    ),
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomTextField(
                        title:
                            AppLocalizations.of(context)?.your_unique_number ??
                                'your_unique_number',
                        // 'введите ваш ИНН',
                        hintText: '00000000000000',
                        controller: registerController,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(14),
                        ],
                      ),
                      const SizedBox(height: 40),
                      StreamBuilder(
                        stream: sbj,
                        builder: (context, snapshot) {
                          return CustomButton(
                            active: isValid(),
                            title: AppLocalizations.of(context)
                                    ?.check_unique_number ??
                                'Проверить ИНН',
                            onTap: () async {
                              final completer = Completer();
                              Navigator.push(
                                context,
                                LoadPopupRoute(completer: completer),
                              );
                              try {
                                await onTap();
                              } catch (e, s) {
                                if (e is OperationException) {
                                  if (e.graphqlErrors.isNotEmpty) {
                                    navgitorKey.currentState?.overlay?.insert(
                                      ErrorOverlay(
                                        title: e.graphqlErrors.first.message
                                            .toString(),
                                      ),
                                    );
                                  }
                                }
                                if (e is String) {
                                  navgitorKey.currentState?.overlay?.insert(
                                    ErrorOverlay(
                                      title: e.toString(),
                                    ),
                                  );
                                }

                                log(e.toString(), stackTrace: s);
                              }
                              completer.complete();
                            },
                          );
                        },
                      ),
                      SizedBox(height: med.padding.bottom + 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
