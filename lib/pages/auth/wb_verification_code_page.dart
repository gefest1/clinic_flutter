import 'dart:async';
import 'dart:developer';

import 'package:clinic/api/client.dart';
import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/custom_text_field.dart';
import 'package:clinic/logic/register_logic.dart';
import 'package:clinic/pages/auth/full_form_register.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/load_pop_up_route.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';

class WBVerificationCodePage extends StatefulWidget {
  const WBVerificationCodePage({super.key});

  @override
  State<WBVerificationCodePage> createState() => _WBVerificationCodePageState();
}

class _WBVerificationCodePageState extends State<WBVerificationCodePage> {
  final codeController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    codeController.dispose();
    super.dispose();
  }

  Future<void> _proccess() async {
    final rgLogic = context.read<RegisterLogic>();

    final resp = await graphqlClient.mutate$CheckCode(
      Options$Mutation$CheckCode(
        variables: Variables$Mutation$CheckCode(
          phoneNumber: rgLogic.phoneNumber!,
          code: codeController.text,
        ),
        fetchPolicy: FetchPolicy.networkOnly,
      ),
    );
    if (resp.parsedData?.checkWbVerify != true) {
      return;
    }
    if (!mounted) return;
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const FullFormRegister(),
      ),
    );
  }

  void checkCode() async {
    final com = Completer();
    Navigator.push(context, LoadPopupRoute(completer: com));
    try {
      await _proccess();
    } catch (e, s) {
      log(e.toString(), stackTrace: s);
    }
    com.complete();
  }

  @override
  Widget build(BuildContext context) {
    final med = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: med.padding.top + 100),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)?.register ?? 'Регистрация',
                    style: const TextStyle(
                      color: ColorData.colorMainElements,
                      fontSize: 42,
                      fontWeight: H1TextStyle.fontWeight,
                      height: H1TextStyle.height,
                    ),
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomTextField(
                        title: AppLocalizations.of(context)?.write_your_code ??
                            'Введите код который пришел к вам на WhatsApp',
                        hintText: '* * * * *',
                        controller: codeController,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(5),
                        ],
                      ),
                      const SizedBox(height: 40),
                      StreamBuilder(
                        stream: null,
                        builder: (context, snapshot) {
                          return CustomButton(
                            active: true,
                            title:
                                AppLocalizations.of(context)?.next ?? 'Дальше',
                            onTap: checkCode,
                          );
                        },
                      ),
                      SizedBox(height: med.padding.bottom + 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
