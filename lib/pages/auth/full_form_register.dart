import 'dart:async';
import 'dart:developer';

import 'package:clinic/api/client.dart';
import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/custom_text_field.dart';
import 'package:clinic/logic/register_logic.dart';
import 'package:clinic/main.dart';
import 'package:clinic/pages/restart_widget.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/load_pop_up_route.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class FullFormRegister extends StatefulWidget {
  const FullFormRegister({super.key});

  @override
  State<FullFormRegister> createState() => _FullFormRegisterState();
}

class _FullFormRegisterState extends State<FullFormRegister> {
  final uniqNumber = TextEditingController();
  final phoneNumber = TextEditingController();
  final firstName = TextEditingController();
  final lastName = TextEditingController();
  final patronymic = TextEditingController();
  final dateBirn = TextEditingController();
  final passwordController = TextEditingController();
  final passwordFocus = FocusNode();

  bool isInited = false;
  @override
  void dispose() {
    uniqNumber.dispose();
    passwordFocus.dispose();
    super.dispose();
  }

  void initControllers() {
    if (isInited) return;
    isInited = true;
    final rgLogic = context.read<RegisterLogic>();

    uniqNumber.text = rgLogic.uniqueNumber ?? "";
    phoneNumber.text = rgLogic.phoneNumber ?? "";

    firstName.text = rgLogic.firstName ?? "";
    lastName.text = rgLogic.lastName ?? "";
    patronymic.text = rgLogic.patronymic ?? "";

    dateBirn.text =
        DateFormat("dd.MM.yyyy").format(rgLogic.dateBirn ?? DateTime.now());
    final f = FocusScope.of(context);
    f.requestFocus(passwordFocus);
  }

  Future<void> _sendData() async {
    final rgLogic = context.read<RegisterLogic>();

    uniqNumber.text = rgLogic.uniqueNumber ?? "";
    phoneNumber.text = rgLogic.phoneNumber ?? "";

    firstName.text = rgLogic.firstName ?? "";
    lastName.text = rgLogic.lastName ?? "";
    patronymic.text = rgLogic.patronymic ?? "";

    dateBirn.text =
        DateFormat("dd.MM.yyyy").format(rgLogic.dateBirn ?? DateTime.now());
    final res = await graphqlClient.mutate$finishRegistration(
      Options$Mutation$finishRegistration(
        variables: Variables$Mutation$finishRegistration(
          data: Input$UserCreateInput(
            dateBirth: rgLogic.dateBirn!,
            firstName: rgLogic.firstName!,
            lastName: rgLogic.lastName!,
            patronymic: rgLogic.patronymic!,
            uniqueNumber: rgLogic.uniqueNumber!,
          ),
          password: passwordController.text,
        ),
      ),
    );
    final fullForm = res.parsedData?.fullForm;
    if (fullForm == null) return;
    log(fullForm.toString());

    await Future.wait([
      sharedBox.put('accessToken', fullForm.token.accessToken),
      sharedBox.put('refreshToken', fullForm.token.refreshToken)
    ]);
    if (!mounted) return;
    RestartWidget.restartApp(context);
  }

  @override
  Widget build(BuildContext context) {
    initControllers();
    final med = MediaQuery.of(context);

    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: med.padding.top + 10),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)?.register ?? 'Регистрация',
                    style: const TextStyle(
                      color: ColorData.colorMainElements,
                      fontSize: 42,
                      fontWeight: H1TextStyle.fontWeight,
                      height: H1TextStyle.height,
                    ),
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title:
                            AppLocalizations.of(context)?.your_unique_number ??
                                'Ваш уникальный код',
                        hintText: '',
                        controller: uniqNumber,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title:
                            AppLocalizations.of(context)?.your_phone_number ??
                                'Ваш номер телефона',
                        hintText: '',
                        controller: phoneNumber,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title: AppLocalizations.of(context)?.your_name ??
                            'Ваше Имя',
                        hintText: '',
                        controller: firstName,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title: AppLocalizations.of(context)?.your_surname ??
                            'Ваше Фамилия',
                        hintText: '',
                        controller: lastName,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title: AppLocalizations.of(context)?.your_patronymic ??
                            'Ваше Отчество',
                        hintText: '',
                        controller: patronymic,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        readOnly: true,
                        title: AppLocalizations.of(context)?.your_birth_date ??
                            'Ваше дата рождение',
                        hintText: '',
                        controller: dateBirn,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 20),
                      CustomTextField(
                        title: AppLocalizations.of(context)?.your_password ??
                            'Введите пароль',
                        hintText: '',
                        scrollPadding: const EdgeInsets.only(bottom: 300),
                        controller: passwordController,
                        focusNode: passwordFocus,
                      ),
                      const SizedBox(height: 40),
                      StreamBuilder(
                        stream: null,
                        builder: (context, snapshot) {
                          return CustomButton(
                            active: true,
                            title:
                                AppLocalizations.of(context)?.next ?? 'Дальше',
                            onTap: () async {
                              final completer = Completer();
                              Navigator.push(
                                context,
                                LoadPopupRoute(completer: completer),
                              );
                              try {
                                await _sendData();
                              } catch (e) {
                                log(e.toString());
                              }
                              completer.complete();
                            },
                          );
                        },
                      ),
                      SizedBox(height: med.padding.bottom + 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
