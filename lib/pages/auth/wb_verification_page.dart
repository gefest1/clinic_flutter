import 'dart:async';
import 'dart:developer';
import 'dart:io' as io;

import 'package:clinic/api/client.dart';
import 'package:clinic/components/common/custom_button.dart';
import 'package:clinic/components/common/custom_text_field.dart';
import 'package:clinic/logic/register_logic.dart';
import 'package:clinic/pages/auth/wb_verification_code_page.dart';
import 'package:clinic/utils/applocal.dart';

import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/load_pop_up_route.dart';
import 'package:clinic/utils/text_style.dart';
import 'package:flutter/material.dart';

import 'package:image/image.dart' as img;
import 'package:provider/provider.dart';

class WBVerificationPage extends StatefulWidget {
  const WBVerificationPage({super.key});

  @override
  State<WBVerificationPage> createState() => _WBVerificationPageState();
}

class _WBVerificationPageState extends State<WBVerificationPage> {
  final phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final med = MediaQuery.of(context);

    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);
        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.only(top: med.padding.top + 100),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)?.register ?? 'Регистрация',
                    // 'Регистрация',
                    style: const TextStyle(
                      color: ColorData.colorMainElements,
                      fontSize: 42,
                      fontWeight: H1TextStyle.fontWeight,
                      height: H1TextStyle.height,
                    ),
                  ),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomTextField(
                        icon: const Text("+"),
                        prefixIconConstraints:
                            const BoxConstraints(maxWidth: 11),
                        title: AppLocalizations.of(context)
                                ?.wrtie_your_whats_app ??
                            "Введите номер телефона для верификации через WhatsApp",
                        hintText: '7',
                        controller: phoneController,
                        keyboardType: TextInputType.phone,
                        inputFormatters: [],
                      ),
                      const SizedBox(height: 40),
                      StreamBuilder(
                        stream: null,
                        // stream: sbj,
                        builder: (context, snapshot) {
                          return CustomButton(
                            active: true,
                            title:
                                AppLocalizations.of(context)?.next ?? 'Дальше',
                            onTap: () async {
                              final comp = Completer();
                              Navigator.push(
                                context,
                                LoadPopupRoute(completer: comp),
                              );
                              try {
                                await graphqlClient.mutate$SendCode(
                                  Options$Mutation$SendCode(
                                    variables: Variables$Mutation$SendCode(
                                      phoneNumber: "+${phoneController.text}",
                                    ),
                                  ),
                                );
                              } catch (e) {}
                              comp.complete();
                              if (!mounted) return;
                              context
                                  .read<RegisterLogic>()
                                  .setPhoneNumber("+${phoneController.text}");
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const WBVerificationCodePage(),
                                ),
                              );
                            },
                          );
                        },
                      ),
                      SizedBox(height: med.padding.bottom + 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
