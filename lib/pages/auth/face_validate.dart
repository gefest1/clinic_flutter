import 'dart:developer';
import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:clinic/face_full_camera/face_camera.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/blink_detect.dart';
import 'package:clinic/utils/image_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'package:rxdart/subjects.dart';
import 'package:image/image.dart' as img;

enum PipelineEnum {
  frontRotate('frontRotate'),
  blink('blink'),
  rightRotate('rightRotate'),
  leftRotate('leftRotate'),
  openMouth('openMouth');

  final String str;
  const PipelineEnum(this.str);

  String getLocalText(BuildContext context) {
    if (this == PipelineEnum.frontRotate) {
      return AppLocalizations.of(context)?.frontRotate ?? "frontRotate";
    }
    if (this == PipelineEnum.blink) {
      return AppLocalizations.of(context)?.blink ?? "blink";
    }
    if (this == PipelineEnum.rightRotate) {
      return AppLocalizations.of(context)?.rightRotate ?? "rightRotate";
    }
    if (this == PipelineEnum.leftRotate) {
      return AppLocalizations.of(context)?.leftRotate ?? "leftRotate";
    }
    if (this == PipelineEnum.openMouth) {
      return AppLocalizations.of(context)?.openMouth ?? "openMouth";
    }
    throw 'No Implementation';
  }
}

class FaceValidate extends StatefulWidget {
  const FaceValidate({super.key});

  @override
  State<FaceValidate> createState() => _FaceValidateState();
}

class _FaceValidateState extends State<FaceValidate> {
  final controller = FaceCameraController();
  double scale = 1;
  var rng = math.Random();
  late List pipes;
  late final pipeSubj = BehaviorSubject<PipelineEnum?>.seeded(null);
  CameraImage? lastCameraImage;

  bool leftRotate = false;
  bool frontRotate = false;
  bool rightRotate = false;

  List<img.Image> files = [];

  @override
  void initState() {
    controller.cameraController.listen((value) {
      if (value == null) return;

      value.addListener(listenCamera);
    });
    pipes = [
      PipelineEnum.frontRotate,
      PipelineEnum.blink,
      PipelineEnum.openMouth,
      PipelineEnum.rightRotate,
      PipelineEnum.leftRotate,
      ...List.generate(
        2 + rng.nextInt(2),
        (index) => PipelineEnum.values[rng.nextInt(PipelineEnum.values.length)],
      ),
    ];
    pipeSubj.value = pipes.first;
    controller.pointsBehavior.listen((value) => listenPoints());
    super.initState();
  }

  void listenPoints() {
    if (!mounted) return;
    if (pipes.isEmpty) return;
    if (pipes.first == PipelineEnum.rightRotate) {
      final rotVal = controller.rotation.value;
      final rect = controller.rectBehavior.value;
      if (rotVal == null || rect == null) return;
      if (!rightRotate) {
        if (rotVal.last < -0.35) {
          rightRotate = true;
          controller.rightFace.value = true;
          return;
        }
      } else {
        if (rotVal.last < 0.1 && rotVal.last > -0.1) {
          if (rect.top > 0.17 &&
              rect.top < 0.40 &&
              rect.bottom > 0.64 &&
              rect.bottom < 0.84) {
            leftRotate = false;
            rightRotate = false;
            controller.leftFace.value = false;
            controller.rightFace.value = false;
            next();
            return;
          }
        }
      }
    } else if (pipes.first == PipelineEnum.leftRotate) {
      final rotVal = controller.rotation.value;
      final rect = controller.rectBehavior.value;
      if (rotVal == null || rect == null) return;
      if (!leftRotate) {
        if (rotVal.last > 0.35) {
          leftRotate = true;
          controller.leftFace.value = true;
          return;
        }
      } else {
        if (rotVal.last < 0.1 && rotVal.last > -0.1) {
          if (rect.top > 0.17 &&
              rect.top < 0.40 &&
              rect.bottom > 0.64 &&
              rect.bottom < 0.84) {
            leftRotate = false;
            rightRotate = false;
            controller.leftFace.value = false;
            controller.rightFace.value = false;
            next();
            return;
          }
        }
      }
    } else if (pipes.first == PipelineEnum.frontRotate) {
      final rotVal = controller.rotation.value;
      final rect = controller.rectBehavior.value;
      if (rotVal == null || rect == null) return;
      if (rotVal.last < 0.1 && rotVal.last > -0.1) {
        if (rect.top > 0.17 &&
            rect.top < 0.40 &&
            rect.bottom > 0.64 &&
            rect.bottom < 0.84) {
          next();
          return;
        }
      }
    } else if (pipes.first == PipelineEnum.blink) {
      final rotVal = controller.rotation.value;
      final pointsVal = controller.pointsBehavior.value;
      final rect = controller.rectBehavior.value;
      if (rotVal == null || rect == null || pointsVal == null) return;
      if (rotVal.last < 0.1 && rotVal.last > -0.1) {
        if (rect.top > 0.17 &&
            rect.top < 0.40 &&
            rect.bottom > 0.64 &&
            rect.bottom < 0.84 &&
            detectBlink(pointsVal)) {
          next();
          return;
        }
      }
    } else if (pipes.first == PipelineEnum.openMouth) {
      final pointsVal = controller.pointsBehavior.value;
      if (pointsVal == null) return;
      if (detectMouth(pointsVal)) {
        next();
        return;
      }
    }
  }

  void next() async {
    if (!mounted) return;
    if (pipes.isEmpty) {
      return;
    }
    pipes.removeAt(0);
    if (pipes.isEmpty) {
      pipeSubj.value = null;
      controller.cameraController.value?.stopImageStream();
      Navigator.pop(context, [...files]);

      return;
    } else {
      pipeSubj.value = pipes.first;
    }
    final cmCntr = controller.cameraController.value;
    if (cmCntr != null) {
      final rotVal = controller.rotation.value;
      final rect = controller.rectBehavior.value;
      final points = controller.pointsBehavior.value;
      if (rotVal == null || rect == null || points == null) return;
      if (detectBlink(points)) return;
      if (rotVal.last < 0.1 && rotVal.last > -0.1) {
        if (rect.top > 0.17 &&
            rect.top < 0.40 &&
            rect.bottom > 0.64 &&
            rect.bottom < 0.84) {
          if (controller.image.value == null) return;

          final img.Image? needToAdd = controller.image.value;
          if (needToAdd != null) {
            files.add(needToAdd.clone());
          }

          return;
        }
      }
    }
  }

  void listenCamera() {
    final cVal = controller.cameraController.value;
    if (cVal == null) return;

    if (cVal.value.isInitialized) {
      setState(() {
        scale = 1 /
            ((controller.cameraController.value?.value.aspectRatio ?? 1) *
                MediaQuery.of(context).size.aspectRatio);
      });

      cVal.removeListener(listenCamera);
    }
  }

  @override
  void dispose() {
    controller.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: Transform.scale(
              scale: scale,
              child: FaceCamera(
                controller: controller,
                draw: false,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: StreamBuilder(
              stream: pipeSubj,
              builder: (context, snapshot) {
                final txt = pipeSubj.value?.getLocalText(context);
                if (txt == null) return const SizedBox();
                return Padding(
                  padding: EdgeInsets.only(top: media.padding.top + 20),
                  child: Text(
                    txt,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 48,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
