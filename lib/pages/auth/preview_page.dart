import 'package:clinic/common/assets/assets.gen.dart';
import 'package:clinic/utils/applocal.dart';
import 'package:clinic/utils/colors.dart';
import 'package:clinic/utils/text.dart';
import 'package:flutter/material.dart';
import 'package:clinic/components/common/custom_button.dart';

class PreviewPage extends StatefulWidget {
  const PreviewPage({super.key});

  @override
  State<PreviewPage> createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 30, right: 30),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H4Text(
                AppLocalizations.of(context)?.follow_instructions ??
                    "follow_instructions",
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 24),
              P1Text(
                "· ${AppLocalizations.of(context)?.keep_camera_eye_level ?? "keep_camera_eye_level"}",
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 24),
              Assets.images.photo20230513155615.image(),
              const SizedBox(height: 8),
              P1Text(
                "· ${AppLocalizations.of(context)?.face_is_evenly_lit ?? "face_is_evenly_lit"}",
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 24),
              Assets.images.photo20230513155816.image(),
              // Assets.images.photo20230423162943.image(),
              const SizedBox(height: 40),
              CustomButton(
                // TODO
                title: 'Продолжить',
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              const SizedBox(height: 80),
            ],
          ),
        ),
      ),
    );
  }
}
