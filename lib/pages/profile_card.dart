import 'package:clinic/components/common/size_tap_animation.dart';
import 'package:clinic/utils/colors.dart';
import 'package:flutter/material.dart';

class ProfileCard extends StatelessWidget {
  final Icon icon;
  final String label;
  final VoidCallback? onTap;

  const ProfileCard({
    required this.icon,
    required this.label,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Column(
        children: [
          Row(
            children: [
              icon,
              const SizedBox(width: 10),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    label,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                ),
              ),
              const Icon(
                Icons.navigate_next,
                size: 24,
                color: ColorData.colortTextSecondary,
              ),
            ],
          ),
          const Divider(
            thickness: 0.5,
            height: 0,
            color: ColorData.color5Percent,
          )
        ],
      ),
    );
  }
}
